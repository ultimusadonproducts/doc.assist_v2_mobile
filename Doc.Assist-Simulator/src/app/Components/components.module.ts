import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { FloatingActionButtonComponent } from "./floating-action-button/floating-action-button.component";

@NgModule({
    declarations: [FloatingActionButtonComponent],
    exports: [FloatingActionButtonComponent],
    imports: [IonicModule.forRoot(), 
              CommonModule, 
              FormsModule]
  })
  
  export class CustomComponents {
  
  }