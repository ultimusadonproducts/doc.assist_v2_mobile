import { Component, OnInit } from '@angular/core';
import { VoiceCommandHandler } from 'src/app/Services/VoiceCommandHandler/voice-command-handler.service';
import { VoiceRecordingHandler } from 'src/app/Services/VoiceRecordingHandler/voice-recording-handler.service';

@Component({
  selector: 'app-floating-action-button',
  templateUrl: './floating-action-button.component.html',
  styleUrls: ['./floating-action-button.component.scss'],
})
export class FloatingActionButtonComponent implements OnInit {
  record
  constructor(private voiceRecordingHandler: VoiceRecordingHandler,
              private voiceCommandHandler: VoiceCommandHandler) { 
    this.voiceRecordingHandler.onRecordComplete.subscribe((message) => {
      alert(message);
      this.voiceCommandHandler.classify(message);
    })
  }

  ngOnInit() {}

  recordButtonTrigger() {
    this.voiceCommandHandler.classify('Patienten-Tab öffnen');
  }
}
