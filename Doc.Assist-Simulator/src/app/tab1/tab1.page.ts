import { Component } from '@angular/core';
import { VoiceCommandHandler } from '../Services/VoiceCommandHandler/voice-command-handler.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  constructor(private voiceCmdHandler: VoiceCommandHandler) {}

  triggerAction() {
    this.voiceCmdHandler.handleChooseAction({action: "Choose", value: "serial-aufGaben"});
  }

}
