import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export class Command {
  action: string = null;
  value: any = null;
}

@Injectable({
  providedIn: 'root'
})
export class VoiceCommandHandler {

  constructor(private http: HttpClient) { }

  classify(message) {
    alert('classify');
    this.http.post('https://badt-docassist-voice.azurewebsites.net/api/classify', {
        "message": message,
        "context": "patienten",
        "logged_user_id": 123456,
        "patients": [   {"name": "Petra Meier", "id": 9},
                        {"name": "Josefine Zimmermann", "id": 10},
                        {"name": "Claudia Pfaff", "id": 13},
                        {"name": "Fanette Brousseau", "id": 14},
                        {"name": "Maximilian Eberbacher", "id": 17}],
        "API_KEY": "430467213112199115930236015207364867"
      }
    ).subscribe((res: any) => {
      alert(res.commands[0].action);
    }, error => {alert('error')});
  }

  handleChooseAction(cmd: Command) {
    console.log(cmd.value.toString().toLowerCase());
    
    let element = document.getElementById('tab-button-' + cmd.value.toString().toLowerCase());
    //    let element: HTMLElement = document.getElementById('tab-button-aufgaben') as HTMLElement;
    if (element instanceof HTMLElement) {
      element.click();
    }

    //    element.click();
  }
}
