import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoadingHandlerService {
  private loading;

  constructor(private loadingController: LoadingController) { }

  async presentLoading() {
    //const loadingMessage = this.translate.instant('COMMON.PLEASE_WAIT');

    this.loading = await this.loadingController.create({
      cssClass: 'loadingContainer',
      message: 'Please wait...',
      mode: 'ios',
      spinner: 'circular'
    });
    await this.loading.present();
  }

  async dismissLoading() {
    this.loading.dismiss();
  }
}
