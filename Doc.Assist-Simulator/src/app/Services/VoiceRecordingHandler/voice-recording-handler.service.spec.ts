import { TestBed } from '@angular/core/testing';

import { VoiceRecordingHandlerService } from './voice-recording-handler.service';

describe('VoiceRecordingHandlerService', () => {
  let service: VoiceRecordingHandlerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VoiceRecordingHandlerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
