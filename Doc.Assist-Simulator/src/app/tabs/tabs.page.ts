import { Component } from '@angular/core';

export enum MainTabs {
  Patienten = 0,
  MeineAufgaben = 1,
  SerialAufgaben = 2,
  Settings = 3
}

export enum SecondaryTabs {
  my = 1,
  all = 2,
  free = 3,
  ungrouped = 4,
  room = 5,
  patient = 6
}

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  constructor() {}

  mainTabs = MainTabs;
  secondaryTabs = SecondaryTabs;

  activePatient = './assets/icon/Patienten/active.svg'
  inactivePatient = './assets/icon/Patienten/inactive.svg'

  activeAufgaben = './assets/icon/Aufgaben/active.svg'
  inactiveAufgaben = './assets/icon/Aufgaben/inactive.svg'

  activeProfile = './assets/icon/Profile/active.svg'
  inactiveProfile = './assets/icon/Profile/inactive.svg'

  activeSerienaufgabe = './assets/icon/Serienaufgabe/active.svg'
  inactiveSerienaufgabe = './assets/icon/Serienaufgabe/inactive.svg'

  activite = './assets/icon/activite.svg'

  selectedUpperPatientTab = 1;

  CurrentSelectedTab = MainTabs.Patienten;

  popover: any;
  searchQuery: string = '';

  ngOnInit() {
  }


  patientTabClicked(tabNumber) {
    this.selectedUpperPatientTab = tabNumber;
    
  }

  changeCurrentSelectedTab(tab) {

    this.CurrentSelectedTab = tab;

    switch(tab) {
      case this.mainTabs.Patienten:
        this.patientTabClicked(this.secondaryTabs.my);
        break;
      case this.mainTabs.MeineAufgaben:
        this.patientTabClicked(this.secondaryTabs.ungrouped);
        break;
      case this.mainTabs.SerialAufgaben:
        break;
      case this.mainTabs.Settings:
        break;
    }
  }

  changetoPatiented() {
    console.log(document.getElementById('tab-button-aufgaben'));
    let element: HTMLElement = document.getElementById('tab-button-aufgaben') as HTMLElement;
    console.log(element)
    element.click();
  }
}
