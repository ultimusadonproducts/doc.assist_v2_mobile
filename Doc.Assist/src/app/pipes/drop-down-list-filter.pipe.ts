import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dropDownListFilter'
})
export class DropDownListFilterPipe implements PipeTransform {

  transform(list: any, searchTerm: string): unknown {
    if(!list || !searchTerm) return list;
    return list.filter(item => item.text.toLowerCase().includes(searchTerm.toLowerCase()));
  }

}
