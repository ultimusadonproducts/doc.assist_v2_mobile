export class DomainConfig {
    EnterDomain: boolean;
    DefaultDomain: string;

    constructor(EnterDomain: boolean, DefaultDomain: string) {
        this.EnterDomain = EnterDomain;
        this.DefaultDomain = DefaultDomain;
    }
}
