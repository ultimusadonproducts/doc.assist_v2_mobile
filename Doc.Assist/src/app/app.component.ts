import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { HttpClient } from '@angular/common/http';
import { AppModule } from './app.module';
import { DomainConfig } from './Models/DomainConfig/domain-config';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private http: HttpClient,
    private translate: TranslateService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.translate.setDefaultLang('de');
      this.splashScreen.hide();
    });
  }

  getDomainConfig() {
    this.http.get('https://psdev05.ultimuscps.com/Doc-Assist/api/Services/checkdomain').subscribe((config: DomainConfig) => {
      this.splashScreen.hide();
      AppModule.domainConfig.DefaultDomain = config.DefaultDomain;
      AppModule.domainConfig.EnterDomain = config.EnterDomain;
    }, error => {
      this.splashScreen.hide();
    }) 
  }
}
