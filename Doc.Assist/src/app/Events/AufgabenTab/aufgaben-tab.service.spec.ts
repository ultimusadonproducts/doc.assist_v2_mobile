import { TestBed } from '@angular/core/testing';

import { AufgabenTabService } from './aufgaben-tab.service';

describe('AufgabenTabService', () => {
  let service: AufgabenTabService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AufgabenTabService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
