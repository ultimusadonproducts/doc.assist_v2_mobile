import { TestBed } from '@angular/core/testing';

import { SerialTabService } from './serial-tab.service';

describe('SerialTabService', () => {
  let service: SerialTabService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SerialTabService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
