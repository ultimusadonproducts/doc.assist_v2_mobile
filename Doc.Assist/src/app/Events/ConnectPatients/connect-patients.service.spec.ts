import { TestBed } from '@angular/core/testing';

import { ConnectPatientsService } from './connect-patients.service';

describe('ConnectPatientsService', () => {
  let service: ConnectPatientsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConnectPatientsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
