import { TestBed } from '@angular/core/testing';

import { AddPatientModuleService } from './add-patient-module.service';

describe('AddPatientModuleService', () => {
  let service: AddPatientModuleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AddPatientModuleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
