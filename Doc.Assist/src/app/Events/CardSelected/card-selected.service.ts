import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CardSelectedService {

  constructor() { }
  private cardIndex = new Subject<any>();

    publishSomeData(data: any) {
        this.cardIndex.next(data);
    }

    getObservable(): Subject<any> {
        return this.cardIndex;
    }
}
