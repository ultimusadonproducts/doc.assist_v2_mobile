import { TestBed } from '@angular/core/testing';

import { PatientTabService } from './patient-tab.service';

describe('PatientTabService', () => {
  let service: PatientTabService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PatientTabService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
