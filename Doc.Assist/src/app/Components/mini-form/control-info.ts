export class ControlInfo {
    
    constructor(label: string, type: string, parameters: string, value: string) {
        this.label = label;
        this.type = type.toLowerCase().replace(/^\s+|\s+$/g, '');
        this.parameters = parameters.split(",");
        this.value = value;
    }

    public label: String = '';
    public type: string = '';
    public parameters: any = [];
    public value: string = null;
}
