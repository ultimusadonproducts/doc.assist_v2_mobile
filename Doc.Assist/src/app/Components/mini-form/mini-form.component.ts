import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SpeechRecognition } from '@ionic-native/speech-recognition/ngx';
import { AppModule } from 'src/app/app.module';
import { ControlInfo } from './control-info';

@Component({
  selector: 'app-mini-form',
  templateUrl: './mini-form.component.html',
  styleUrls: ['./mini-form.component.scss'],
})
export class MiniFormComponent implements OnInit {
  voiceRecorderInactive = './assets/icon/VoiceRecorder/Voice_inactive.png';

  @Input() activityName: string = '';
  @Input() activityID: number = 0;
  @Input() disabled: boolean = false;
  @Input() value: string = '';
  @Output() onChange = new EventEmitter();

  controls: ControlInfo[] = [];
  controlValues: string[] = [];
  constructor(private speechRecognition: SpeechRecognition,
              private cd: ChangeDetectorRef,
              private http: HttpClient) { }

  ngOnInit() {
    this.getControlInfo();

    this.controlValues = new Array<string>(this.controls.length);

  }

  getControlInfo() {
    this.http.post(AppModule.baseURL + '/MiniForm/GetActivityMiniForm', this.activityID)
    .subscribe(async (info: any) => {
      console.log(info);
      info.forEach(element => {
        this.controls.push(new ControlInfo(element.ControlLabel, 
                                            element.ControlType, 
                                            element.Parameters, element.Value))
      });
      this.controlValues = new Array<string>(this.controls.length);
      console.log(this.controls)
    }, error => {
      console.log('alert should be shown here.')
    })
  }

  Record(index) {
    this.speechRecognition.hasPermission()
    .then((hasPermission: boolean) => {
      if(hasPermission){
        this.startRecording(index);
      } else {
        this.requestRecordPermission(index);
      }
    })
  }

  requestRecordPermission(index) {
    this.speechRecognition.requestPermission()
    .then(
      () => this.startRecording(index),
      () => {}
    )
  }

  startRecording(index) {
    const option = {
      language: 'de-DE',
    };
    this.speechRecognition.startListening(option)
    .subscribe(
      (matches: string[]) => 
      {
        //console.log(matches);
        //this.inputChange()
        this.controlValues[index] = matches[0];
        this.cd.detectChanges();
        this.updateWholeValue();
      },
      (onerror) => console.log('error:', onerror)
    )
  }

  inputChange(event, index) {
    this.controlValues[index] = event.detail.value;
    this.updateWholeValue();
  }

  selectionChange(event, index) {
    this.controlValues[index] = event;
    this.updateWholeValue();
  }

  updateWholeValue() {
    this.value = '';
    for(let i = 0 ; i < this.controlValues.length ; ++i) {
      this.value += this.controls[i].label + ': ' + ((this.controlValues[i] === undefined 
                                                      || this.controlValues[i] === null ) ? '' : 
                                                      this.controlValues[i].replace(/^\s+|\s+$/g, '')) + '\n';
    }
    this.onChange.emit(this.value);
  }

  toDropDownList(parameters: string[]) {
    //console.log('list', parameters)
    let list = [];
    for(let i = 0; i < parameters.length ; ++i) {
      list.push({text: parameters[i], value: parameters[i], index: i});
    }
    //console.log(list)
    return list;
  }
}
