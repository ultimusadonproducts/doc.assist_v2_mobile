import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-patient-group',
  templateUrl: './patient-group.component.html',
  styleUrls: ['./patient-group.component.scss'],
})
export class PatientGroupComponent implements OnInit {
  arrowDown = './assets/icon/arrow/down.svg';
  InnerBodyAppear = false;
  
  @Input() group = null;
  @Output() groupChange = new EventEmitter();

  constructor() { }

  ngOnInit() {}

}
