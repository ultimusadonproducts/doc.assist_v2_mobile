import { AfterViewChecked, AfterViewInit, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ViewWillEnter, ViewWillLeave } from '@ionic/angular';
import { Screens } from 'src/app/Enums/screens.enum';
import { VoiceCommandHandler } from 'src/app/Services/VoiceCommandHandler/voice-command-handler.service';
import { VoiceRecordingHandler } from 'src/app/Services/VoiceRecordingHandler/voice-recording-handler.service';

@Component({
  selector: 'app-floating-action-button',
  templateUrl: './floating-action-button.component.html',
  styleUrls: ['./floating-action-button.component.scss'],
})
export class FloatingActionButtonComponent implements OnInit, OnDestroy {
  @Input() addBlankSpace: boolean = true;
  @Input() screen: Screens = null;
  @Input() context: string = null; 

  record
  subscription;
  constructor(private voiceRecordingHandler: VoiceRecordingHandler,
              private voiceCommandHandler: VoiceCommandHandler) { 
    this.subscription = this.voiceRecordingHandler.onRecordComplete.subscribe((message) => {
      this.voiceCommandHandler.classify(message, (this.screen === Screens.Home), this.context);
    })
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  recordButtonTrigger() {
    this.voiceRecordingHandler.Record();
  }
}
