import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component, ElementRef, Input, OnInit, QueryList, ViewChildren } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AppModule } from 'src/app/app.module';
import { AddPatientModuleService } from 'src/app/Events/AddPatient/add-patient-module.service';
import { ConnectPatientsService } from 'src/app/Events/ConnectPatients/connect-patients.service';

@Component({
  selector: 'app-add-patient-care-card',
  templateUrl: './add-patient-care-card.component.html',
  styleUrls: ['./add-patient-care-card.component.scss'],
})
export class AddPatientCareCardComponent implements OnInit, AfterViewInit {

  @ViewChildren('textBox', {read: ElementRef}) textBoxIcon: QueryList<ElementRef>;
  
  @Input() group;
  @Input() ModuleNumber;

  arrowDown = './assets/icon/arrow/down.svg'
  checkBoxOn = './assets/icon/checkbox/on.svg'
  checkBoxOff = './assets/icon/checkbox/off.svg'
  
  checkBoxesOnCount = 0;
  showInnerBody = false;

  checkBoxes = [];

  constructor(private modelController: ModalController,
              private http: HttpClient,
              private connectPatinet: ConnectPatientsService,
              private addPatientModule: AddPatientModuleService) { }

  ngOnInit() {
    this.connectPatinet.getObservable().subscribe(data => {
      if(data.connect == true && data.ModuleNumber == this.ModuleNumber)
        this.callConnectToUser();
    })
  }

  ngAfterViewInit() {
    this.checkBoxes = this.textBoxIcon.toArray();
  }

  dismiss() {
    this.modelController.dismiss();
  }

  toggleCard() {
    this.showInnerBody = !this.showInnerBody;
  }

  /*selectwholeRoom() {
    this.checkBoxesOnCount = (this.checkBoxesOnCount < this.Care.Patients.length) ? 
                                this.Care.Patients.length : 0;

    for(let i = 0 ; i < this.checkBoxes.length ; ++i){
      if(this.checkBoxesOnCount == 0 && this.checkBoxes[i].nativeElement.src != this.checkBoxOff) {
        this.checkBoxes[i].nativeElement.src = this.checkBoxOff;
        this.notifyAddPatientModule({cardSelected: false});
      } else if (this.checkBoxesOnCount > 0 && this.checkBoxes[i].nativeElement.src != this.checkBoxOn) {
        this.checkBoxes[i].nativeElement.src = this.checkBoxOn;
        this.notifyAddPatientModule({cardSelected: true});
      }
    }

    /*this.checkBoxes.forEach(checkBox => {
      if()
      checkBox.nativeElement.src = (this.checkBoxesOnCount == 0) ? this.checkBoxOff : this.checkBoxOn;
    })*
  }*/

  checkBoxClicked(index: number) {
    if(this.checkBoxes[index].nativeElement.src == this.checkBoxOff) {
      this.checkBoxes[index].nativeElement.src = this.checkBoxOn;
      this.checkBoxesOnCount++;
      this.notifyAddPatientModule({cardSelected: true});
    } else {
      this.checkBoxes[index].nativeElement.src = this.checkBoxOff
      this.checkBoxesOnCount--;
      this.notifyAddPatientModule({cardSelected: false});
    }

    //if(this.checkBoxesOnCount == )
  }

  notifyAddPatientModule(body) {
    this.addPatientModule.publishSomeData(body);
  }

  async callConnectToUser() { 
    for (let i = 0; i < this.checkBoxes.length ; ++i) {

      if (this.checkBoxes[i].nativeElement.src == this.checkBoxOn){
        await this.http.post(AppModule.baseURL + '/UserPatientConnection/ConnectUserToPatient', 
          {
            "userID": AppModule.currentUser.ID,
            "patientID": this.group.Content[i].Patient.ID
          })
          .subscribe(
            async connected => {
              this.notifyAddPatientModule({connected:true});
            },
            error => {
              this.notifyAddPatientModule({connected:false});
            }
          );
      }
    }
  }
}
