import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-short-patient-card',
  templateUrl: './short-patient-card.component.html',
  styleUrls: ['./short-patient-card.component.scss'],
})
export class ShortPatientCardComponent implements OnInit {
  @Input() name: string = '';
  @Input() extraInfo: string = '';
  constructor() { }

  ngOnInit() {}

}
