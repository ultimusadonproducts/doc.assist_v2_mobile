import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
  ViewContainerRef,
  ViewEncapsulation,
} from '@angular/core';
import { Subject } from 'rxjs';

import * as touchEvent from '../core/util/touch-event';
import * as velocity from '../core/util/velocity';
import { PickerOptions } from './picker-options.provider';

@Component({
  selector: 'app-picker',
  template: '<div></div>',
  encapsulation: ViewEncapsulation.None,
})
export class PickerComponent<T = any, R = any>
  implements OnInit, AfterViewInit, OnDestroy {
  transitionName = 'am-slide-up-enter am-slide-up-enter-active';
  maskTransitionName = 'am-fade-enter am-fade-enter-active';
  startY = 0;
  differY = 0;
  currentY = 0;
  len = 0;
  dom: any = null;
  index = 0;
  maxY = 0;
  lineHeight = 55;
  dataForRender: any[] = [];
  selectedTarget: any[] = [];
  isMouseDown = false;
  Velocity = velocity.getVelocity();
  currentPicker: any;
  tempDifferY = 0;
  mouseDownItems = [];

  private unsubscribe$: Subject<void> = new Subject<void>();

  // tslint:disable-next-line: no-output-on-prefix
  @Output() onPickerChange: EventEmitter<any> = new EventEmitter();

  @ViewChild('picker', { read: ViewContainerRef, static: true })
  private picker: ViewContainerRef;

  @Input() disabled: boolean = false;

  @HostListener('mousedown', ['$event'])
  @HostListener('touchstart', ['$event'])
  panstart(event) {
    if(this.disabled) return;
    if (event.target.classList.contains('unit')) {
      return;
    }

    if (
      !event.target.classList.contains('am-picker-col-mask') ||
      this.options.disabled
    ) {
      return;
    }

    this.isMouseDown = true;

    this.mouseDownItems[event.target.id] = true;

    event.preventDefault();
    this.currentY = -this.getIndex(event.target.id);

    this.dom = touchEvent.getEventTarget(
      event
    ).target.parentElement.children[2];
    this.len = this.dom.children.length;
    this.maxY = -(this.len - 1);

    if (this.dom.style.transform === 'translateY(0px)') {
      this.currentY = 0;
      this.maxY = -(this.len - 1);
    }

    this.startY = touchEvent.getEventTarget(event).clientY;
  }

  @HostListener('mousemove', ['$event'])
  @HostListener('touchmove', ['$event'])
  panmove(event) {
    if (event.target.classList.contains('unit')) {
      return;
    }

    if (
      !event.target.classList.contains('am-picker-col-mask') ||
      !this.isMouseDown ||
      this.options.disabled
    ) {
      return;
    }

    event.preventDefault();
    this.currentY = -this.getIndex(event.target.id);

    const ev = touchEvent.getEventTarget(event);
    this.differY = ev.clientY - this.startY;
    this.Velocity.record(this.differY);

    this.dom.style.transition = 'transform 300ms';
    this.dom.style.transform = `translateY(${
      this.currentY * this.lineHeight + this.differY
    }px)`;
    /****** */
    // shift time
    // shift time
    //let time = 0.3;
    //const velocityTemp = this.Velocity.getVelocity(this.differY) * 4;
    /*if (velocity) {
      this.differY = velocityTemp * 40 + this.differY;
      // slow speed 0.3
      time = Math.abs(velocityTemp) * 0.1;
    }
    this.dom.style.transition = 'transform ' + (time < 0.3 ? 0.3 : time) + 's';
    console.log('touch currentY='+this.currentY+ " ClientY = "+ev.clientY+" this.differY="+this.differY);

    if (this.differY <= -this.lineHeight) {
      this.currentY += Math.floor(this.differY / this.lineHeight);
      if (this.currentY <= this.maxY) {
        this.currentY = this.maxY;
      }
    } else if (this.differY >= this.lineHeight ) {
      this.currentY += Math.floor(this.differY / this.lineHeight);
      if (this.currentY >= 0) {
        this.currentY = 0;
      }
    }

    this.dom.style.transform = `translateY(${
      this.currentY * this.lineHeight
    }px)`;
    this.index = Math.floor(Math.abs(this.currentY / 1));
    // console.log('touch currentY='+this.currentY+ " ClientY = "+ev.clientY+" this.differY="+this.differY);
    // console.log('this index = '+this.index);
    //this.setCurrentSelected(parseInt(event.target.id, 0), this.index);

    if (this.options.value !== this.combineResult()) {
      this.onPickerChange.emit(this.stringifyCombineResult());
      this.onChange(this.stringifyCombineResult());
    }
    */
    //console.log('touch move on = '+event.target.id+" this.index"+this.index);
    //console.log('touch startY='+this.startY+ " ClientY = "+ev.clientY+" this.differY="+this.differY);
    
    /*console.log(this.differY,this.lineHeight)
    if (this.differY < 0 && (this.differY  % 35) == 0) {
      const index = this.getIndex(event.target.id);
      console.log('this index is '+index);
      this.setCurrentSelected(parseInt(event.target.id, 0), index+1);
      //this.selectedTarget[event.target.id]--; 
      console.log("this.selectedTarget[event.target.id] = "+this.selectedTarget[event.target.id]);
      this.tempDifferY = this.differY;
    }else if (this.differY > 0 && (-this.differY  % 35 ==0 )){
      console.log('nagative');
      const index = this.getIndex(event.target.id);
      console.log('this index is '+index);
      this.setCurrentSelected(parseInt(event.target.id, 0), index-1);
      //this.selectedTarget[event.target.id]++;
      this.tempDifferY = this.differY;
    }*/
  }

  @HostListener('mouseup', ['$event'])
  @HostListener('mouseleave', ['$event'])
  @HostListener('touchend', ['$event'])
  panend(event) {
    if (event.target.classList.contains('unit')) {
      return;
    }

    if (
      !event.target.classList.contains('am-picker-col-mask') ||
      !this.isMouseDown ||
      this.options.disabled
    ) {
      return;
    }

    this.isMouseDown = false;
    this.mouseDownItems[event.target.id] = false;
    event.preventDefault();
    this.currentY = -this.getIndex(event.target.id);

    const ev = touchEvent.getEventTarget(event);
    this.differY = ev.clientY - this.startY;

    // shift time
    let time = 0.3;
    const velocityTemp = this.Velocity.getVelocity(this.differY) * 4;
    if (velocity) {
      this.differY = velocityTemp * 40 + this.differY;
      // slow speed 0.3
      time = Math.abs(velocityTemp) * 0.1;
    }
    this.dom.style.transition = 'transform ' + (time < 0.3 ? 0.3 : time) + 's';
    if (this.differY <= -this.lineHeight / 2) {
      this.currentY += Math.floor(this.differY / this.lineHeight);
      if (this.currentY <= this.maxY) {
        this.currentY = this.maxY;
      }
    } else if (this.differY >= this.lineHeight / 2) {
      this.currentY += Math.floor(this.differY / this.lineHeight);
      if (this.currentY >= 0) {
        this.currentY = 0;
      }
    }

    this.dom.style.transform = `translateY(${
      this.currentY * this.lineHeight
    }px)`;
    this.index = Math.floor(Math.abs(this.currentY / 1));

    this.setCurrentSelected(parseInt(event.target.id, 0), this.index);

    if (this.options.value !== this.combineResult()) {
      this.onPickerChange.emit(this.stringifyCombineResult());
      this.onChange(this.stringifyCombineResult());
    }
  }

  constructor(public elementRef: ElementRef, public options: PickerOptions) {}

  onChange = (_: any) => {};

  getIndex(target){
    const list = this.dataForRender[target];

    return list.indexOf(this.selectedTarget[target]);
  }
  /*reloadPicker() {
    if (!this.picker || this.picker === undefined) {
      return;
    }
    this.currentPicker = this.picker.element.nativeElement;
    if (this.currentPicker && this.currentPicker.children.length > 0) {
      const self = this;
      setTimeout(() => {
        self.selectedTarget.forEach((item, i) => {
          self.currentPicker.children[i].children[2].style.transition =
            'transform .3s';
          const index = parseInt(item.currentY, 0);
          self.currentPicker.children[
            i
          ].children[2].style.transform = `translateY(${
            index * self.lineHeight
          }px)`;
        });
      }, 0);
    }
  }*/

  defaultselected(){

    this.options.wheels.forEach((item,i) => {
      const element = this.dataForRender[i];
      if(item.value!==undefined){
        const indexn = element.findIndex(e =>
        e == item.value);
      this.setCurrentSelected(i,indexn);
      this.currentPicker.children[i].children[2].style.transition = 'transform .3s';
      this.currentPicker.children[i].children[2].style.transform = `translateY(-${
                                                                    indexn * this.lineHeight
                                                                    }px)`;
      }else{
      this.setCurrentSelected(i,0);
      }
    
    this.onPickerChange.emit(this.stringifyCombineResult());
    
    this.onChange(this.stringifyCombineResult());
    
    });
    
    }
    
    reloadPicker() {
      console.log('reloadPicker');
      if (!this.picker || this.picker === undefined)
      {
        return;
      }
      this.currentPicker = this.picker.element.nativeElement;
      if (this.currentPicker && this.currentPicker.children.length > 0) {
        const self = this;
        setTimeout(() => {
        this.defaultselected();
        }, 0);
      }
    
    }

  generateArrayData(options: PickerOptions) {
    const tempArr = [];

    // tslint:disable-next-line: prefer-for-of
    for (let index = 0; index < options.wheels.length; index++) {
      const wheel = options.wheels[index];
      tempArr.push(this.generateArray(wheel.min, wheel.max));
    }

    return tempArr;
  }

  generateArray(min: number, max: number) {
    return Array.from({ length: max - min + 1 }, (_, i) => i + min).map((o) =>
      o.toString()
    );
  }

  combineResult() {
    return this.selectedTarget;
  }

  stringifyCombineResult() {
    let str = '';
    for (let index = 0; index < this.options.wheels.length; index++) {
      const wheel = this.options.wheels[index];

      if (wheel.beforeDot) {
        str = str + `.${this.selectedTarget[index]}`;
      } else {
        str = str + `${this.selectedTarget[index]}`;
      }
    }

    return str;
  }

  setCurrentSelected(target, index) {
    const list = this.dataForRender[target];
    const value = list[index];

    this.selectedTarget[target] = value;
  }

  getInstance(): PickerComponent {
    return this;
  }

  getElement(): HTMLElement {
    return this.elementRef && this.elementRef.nativeElement;
  }

  close(): void {}

  destroy(): void {
    this.close();
  }

  // track scrolling for each list
  initMouseDowns() {
    this.mouseDownItems.fill(false, 0, this.dataForRender.length);
  }

  ngOnInit() {}

  ngAfterViewInit() {
    this.reloadPicker();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
