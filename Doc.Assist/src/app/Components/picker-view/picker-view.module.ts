import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { PickerOptions } from './picker-options.provider';
import { PickerViewComponent } from './picker-view.component';
import { PickerComponent } from './picker.component';
import { SmoothScrollerDirective } from './smooth-scroller.directive';

@NgModule({
  imports: [FormsModule, CommonModule],
  exports: [PickerViewComponent],
  declarations: [PickerViewComponent, PickerComponent, SmoothScrollerDirective],
  providers: [PickerOptions],
})
export class PickerViewModule {}
