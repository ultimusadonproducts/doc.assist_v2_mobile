import { Directive, ElementRef } from '@angular/core';
import BScroll from '@better-scroll/core';

@Directive({
  selector: '[appSmoothScroller]',
})
export class SmoothScrollerDirective {
  bs;
  constructor(private el: ElementRef) {}

  ngAfterViewInit() {
    this.init();
  }

  init() {
    this.bs = new BScroll(this.el.nativeElement, {
      scrollX: false,
    });
    this.bs.on('scrollStart', () => {
      console.log('scrollStart-');
    });
    this.bs.on('scroll', ({ y }) => {
      console.log('scrolling-');
    });
    this.bs.on('scrollEnd', () => {
      console.log('scrollingEnd');
    });
  }
}
