import { Injectable } from '@angular/core';

export type PickerWheelOptions = {
  min: number;
  max: number;
  beforeDot?: boolean;
  width?: number;
  value?: number;
};

export interface PickerOptionsInterface {
  wheels: PickerWheelOptions[];
  unit?: boolean;
  disabled?: boolean;
  value?: Array<string>;
  unitStr?: String;
}

@Injectable()
export class PickerOptions implements PickerOptionsInterface {
  wheels: PickerWheelOptions[];
  unit?: boolean;
  disabled?: boolean;
  value?: Array<string>;
  unitStr?: String;

  updateNgModel?: (value: any[]) => void;
}

export type PickerCallBack = (result?: any) => PromiseLike<any> | void;
