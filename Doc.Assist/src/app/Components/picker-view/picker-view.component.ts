import {
  AfterViewInit,
  Component,
  forwardRef,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  ViewEncapsulation,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { PickerOptions } from './picker-options.provider';
import { PickerComponent } from './picker.component';

@Component({
  selector: 'app-picker-view',
  templateUrl: './picker-view.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PickerViewComponent),
      multi: true,
    },
  ],
})
export class PickerViewComponent
  extends PickerComponent
  implements OnInit, AfterViewInit, ControlValueAccessor, OnChanges {
  @Input() options: PickerOptions = {
    unit: true,
    unitStr: "C",
    wheels: [
      {
        min: 10,
        max: 17,
      },
      {
        min: 10,
        max: 20,
      },
      {
        min: 10,
        max: 20,
        beforeDot: true,
      },
    ],
  };

  pickerViewInit() {
    this.init();
  }

  init() {
    this.selectedTarget = [];
    this.dataForRender = this.generateArrayData(this.options);

    this.fillSelectedTarget();
    this.initMouseDowns();

    setTimeout(() => {
      this.reloadPicker();
    });
  }

  fillSelectedTarget() {
    // tslint:disable-next-line: prefer-for-of
    for (let index = 0; index < this.dataForRender.length; index++) {
      const element = this.dataForRender[index];
      if (element.length > 0) {
        this.selectedTarget.push(element[0]);
      }
    }
  }

  writeValue(value: any[]): void {
    if (value) {
      this.init();
    }
  }

  registerOnChange(fn: (_: any[]) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any[]): void {}

  ngOnInit() {
    this.pickerViewInit();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.cols) {
      this.dataForRender = [];
    }
    if (changes.data || changes.cols) {
      this.pickerViewInit();
    }
  }

  ngAfterViewInit() {
    this.currentPicker = this.elementRef.nativeElement;
    this.reloadPicker();
  }
}
