import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ConfigService } from 'src/app/Services/ConfigService/config.service';

@Component({
  selector: 'app-calender-picker',
  templateUrl: './calender-picker.component.html',
  styleUrls: ['./calender-picker.component.scss'],
})
export class CalenderPickerComponent implements OnInit {
  value: Date = null;

  @Input() showOpenButton: boolean = false;
  @Output() onDateChange = new EventEmitter();

  constructor(public config: ConfigService) { }

  ngOnInit() {
    if(this.config.isNullableAttributes())
      this.config.getServerConfigTime().subscribe(config => {
        this.value = new Date(this.config.serverTimeNow);
      });
    else 
      this.value = new Date(this.config.serverTimeNow);
  }

  dateChange($event) {
    this.value = $event.target.value;
    this.onDateChange.emit(this.config.toMinutes(this.value.valueOf() - this.config.serverTimeNow.valueOf()));
  }

}
