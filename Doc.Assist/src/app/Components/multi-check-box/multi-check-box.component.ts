import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-multi-check-box',
  templateUrl: './multi-check-box.component.html',
  styleUrls: ['./multi-check-box.component.scss'],
})
export class MultiCheckBoxComponent implements OnInit {
  @Input() header: string = '';
  @Input() list: string[] = [];
  @Input() disabled: boolean = false;
  @Input() value: boolean = false;
  @Output() onChange = new EventEmitter();
  @Output() valueChange: EventEmitter<boolean> = new EventEmitter();

  selectedValues: string = '';
  status: boolean[] = [];
  
  checkBoxOn = './assets/icon/checkbox/on.svg';
  checkBoxOff = './assets/icon/checkbox/off.svg';

  constructor() { }

  ngOnInit() {
  }

  checkBoxClick(index) {
    let firstfound = false;
    this.selectedValues = '';

    for(let i = 0 ; i < this.status.length ; ++i) {
      if(this.status[i] === true) {
        this.selectedValues += ((firstfound) ? ', ' : '') + this.list[i];
        firstfound = true;
      }
    }

    this.onChange.emit(this.selectedValues);
  }

  changeItem(index) {
    this.selectedValues = this.list[index];
    this.onChange.emit(this.selectedValues);
  }

}
