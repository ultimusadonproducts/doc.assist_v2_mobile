import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { DatetimeService } from 'src/app/Services/DatetimeService/datetime.service';

@Component({
  selector: 'app-task-card',
  templateUrl: './task-card.component.html',
  styleUrls: ['./task-card.component.scss'],
})
export class TaskCardComponent implements OnInit {

  @Input() grouping: string;
  @Input() activity: any;
  @Input() showExtralDetails: boolean;
  @Input() roomName: string;
  @Input() patientName: string;

  constructor(private router: Router, private storage: Storage,
              public datetimeFormatService: DatetimeService) { }

  ngOnInit() {
    //console.log(this.activity);
  }

  goToDocumentTask() {
    this.storage.set('selectedTask', this.activity).then(() => {
      this.router.navigate(['/document-task']);
    });
  }

}
