import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { ActivityMenuComponent } from './activity-menu/activity-menu.component';
import { AddPatientCareCardComponent } from './add-patient-care-card/add-patient-care-card.component';
import { AufgabenActivitiesComponent } from './aufgaben-activities/aufgaben-activities.component';
import { CardItemComponent } from './card-item/card-item.component';
import { CheckBoxComponent } from './check-box/check-box.component';
import { DropDownComponent } from './drop-down/drop-down.component';
import { LateAufgabenCardComponent } from './late-aufgaben-card/late-aufgaben-card.component';
import { RadioGroupComponent } from './radio-group/radio-group.component';
import { TaskCardComponent } from './task-card/task-card.component';
import { TitleBarComponent } from './title-bar/title-bar.component';
import { UserTasksComponent } from './user-tasks/user-tasks.component';
import { MiniFormComponent } from './mini-form/mini-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MultiCheckBoxComponent } from './multi-check-box/multi-check-box.component';
import { TranslateModule } from '@ngx-translate/core';
import { AddBedarfComponent } from '../Module/add-bedarf/add-bedarf.component';
import { PatientGroupComponent } from './patient-group/patient-group.component';
import { AddPatientRoomCardComponent } from './add-patient-room-card/add-patient-room-card.component';
import { ModifiedDropDownComponent } from './modified-drop-down/modified-drop-down.component';
import { CalenderPickerComponent } from './calender-picker/calender-picker.component';

import { MatExpansionModule } from '@angular/material/expansion';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatNativeDateModule } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { NgxMatDatetimePickerModule, NgxMatNativeDateModule, NgxMatTimepickerModule } from '@angular-material-components/datetime-picker';
import { FloatingActionButtonComponent } from './floating-action-button/floating-action-button.component';
import { ShortPatientCardComponent } from './short-patient-card/short-patient-card.component';
import { DropDownListFilterPipe } from '../pipes/drop-down-list-filter.pipe';


@NgModule({
  declarations: [ActivityMenuComponent,
                AddPatientCareCardComponent,
                AddPatientRoomCardComponent,
                AufgabenActivitiesComponent,
                CardItemComponent,
                CheckBoxComponent,
                DropDownComponent,
                LateAufgabenCardComponent,
                RadioGroupComponent,
                TaskCardComponent,
                TitleBarComponent,
                UserTasksComponent,
                MiniFormComponent,
                MultiCheckBoxComponent,
                AddBedarfComponent,
                PatientGroupComponent,
                ModifiedDropDownComponent,
                CalenderPickerComponent,
                FloatingActionButtonComponent,
                ShortPatientCardComponent,
                DropDownListFilterPipe],
  exports: [ActivityMenuComponent,
              AddPatientCareCardComponent,
              AddPatientRoomCardComponent,
              AufgabenActivitiesComponent,
              CardItemComponent,
              CheckBoxComponent,
              DropDownComponent,
              LateAufgabenCardComponent,
              RadioGroupComponent,
              TaskCardComponent,
              TitleBarComponent,
              UserTasksComponent,
              MiniFormComponent,
              MultiCheckBoxComponent,
              AddBedarfComponent,
              PatientGroupComponent,
              ModifiedDropDownComponent,
              CalenderPickerComponent,
              FloatingActionButtonComponent,
              ShortPatientCardComponent],
  imports: [IonicModule.forRoot(), 
            CommonModule, 
            FormsModule, 
            TranslateModule,
            MatExpansionModule,
            MatDatepickerModule,
            MatNativeDateModule,
            MatFormFieldModule,
            MatInputModule,
            ReactiveFormsModule,
            MatButtonModule,
            NgxMatDatetimePickerModule,
            NgxMatTimepickerModule,
            NgxMatNativeDateModule]
})

export class CustomComponents {

}
