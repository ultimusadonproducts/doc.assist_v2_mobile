import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AppModule } from 'src/app/app.module';
import { AddBedarfComponent } from 'src/app/Module/add-bedarf/add-bedarf.component';
import { AddEventPage } from 'src/app/Module/add-event/add-event.page';
import { AdhocPage } from 'src/app/Module/adhoc/adhoc.page';
import { ArztlicheAnordnungPage } from 'src/app/Module/arztliche-anordnung/arztliche-anordnung.page';

@Component({
  selector: 'app-activity-menu',
  templateUrl: './activity-menu.component.html',
  styleUrls: ['./activity-menu.component.scss'],
})
export class ActivityMenuComponent implements OnInit {
  showEvents: boolean = false;
  showBedarf: boolean = false;
  showAdhoc: boolean = false;
  showKorrektur: boolean = false;
  showArztliche: boolean = false;

  constructor(private modalController: ModalController) { 
    
  }

  ngOnInit() {

    if(this.checkFeatureAction('AM_Ereignis', 'View')) {
      this.showEvents = true;
    }
    if(this.checkFeatureAction('AM_Bedarfsmedikation', 'View')) {
      this.showBedarf = true;
    }
    if(this.checkFeatureAction('AM_AdhocAufgabe', 'View')) {
      this.showAdhoc = true;
    } 
    if(this.checkFeatureAction('AM_Korrektur', 'View')) {
      this.showKorrektur = true;
    } 
    if(this.checkFeatureAction('AM_ärztlicheAnordnung', 'View')) {
      this.showArztliche = true;
    }
  }

  checkFeatureAction(feature, action) {
    return AppModule.checkFeatureAction(feature, action);
  }

  async createAddEventModel() {
    const modal = await this.modalController.create({
      component: AddEventPage,
      swipeToClose: true,
      mode: 'ios',
    });
    return await modal.present();
  }

  async createAddBedarfModel() {
    const modal = await this.modalController.create({
      component: AddBedarfComponent,
      swipeToClose: true,
      mode: 'ios',
    });
    return await modal.present();
  }

  async createAdhocModel() {
    const modal = await this.modalController.create({
      component: AdhocPage,
      swipeToClose: true,
      mode: 'ios',
    });
    return await modal.present();
  }

  async createArztlicheAnordnungModel() {
    const modal = await this.modalController.create({
      component: ArztlicheAnordnungPage,
      swipeToClose: true,
      mode: 'ios',
    });
    return await modal.present();
  }
}
