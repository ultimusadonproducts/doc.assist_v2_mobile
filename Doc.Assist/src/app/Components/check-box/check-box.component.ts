import { ThrowStmt } from '@angular/compiler';
import { AfterContentChecked, AfterContentInit, AfterViewInit, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Console } from 'console';

@Component({
  selector: 'app-check-box',
  templateUrl: './check-box.component.html',
  styleUrls: ['./check-box.component.scss'],
})
export class CheckBoxComponent implements OnInit, AfterContentChecked, AfterContentInit {
  @Input() header: string = null;
  @Input() label: string = null;
  @Input() disabled: boolean = false;
  @Input() value: boolean = false;
  @Output() onChange = new EventEmitter();
  @Output() valueChange: EventEmitter<boolean> = new EventEmitter();
  
  checkBoxOn = './assets/icon/checkbox/on.svg';
  checkBoxOff = './assets/icon/checkbox/off.svg';

  constructor() { }

  ngOnInit() {
    
  }

  ngAfterContentInit(): void {
    this.valueChange.emit(this.value);
    this.onChange.emit(this.value);
  }

  ngAfterContentChecked(): void {
    if(this.label.includes("(default)")) {
      this.label = this.label.replace("(default)", "");
      this.onClick(true);
    }
  }

  onClick(fDefault = false) {
    if(this.disabled) return;
    
    this.value = true;
    if(!fDefault) this.valueChange.emit(this.value);
    this.onChange.emit(this.value);
  }

  getIconSrc() {
    if(this.value ) {
      return ('./assets/icon/checkbox/on' + (this.disabled ? '-disabled.svg' : '.svg'));
    } else {
      return ('./assets/icon/checkbox/off' + (this.disabled ? '-disabled.svg' : '.svg'));
    }
  }
}
