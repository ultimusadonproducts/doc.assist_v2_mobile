import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-radio-group',
  templateUrl: './radio-group.component.html',
  styleUrls: ['./radio-group.component.scss'],
})
export class RadioGroupComponent implements OnInit {
  @Input() header: string = '';
  @Input() list: string[] = [];
  @Input() selectedValue: string = '';
  @Input() disabled: boolean = false;
  @Output() onChange = new EventEmitter();

  constructor() { }

  ngOnInit() {
    for(let i = 0 ; i < this.list.length ; ++i) {
      if(this.list[i].includes("(default)")) {
        this.list[i] = this.list[i].replace("(default)", "");
        this.selectedValue = this.list[i];
      }
    }
  }


  changeItem(index) {
    this.selectedValue = this.list[index];
    this.onChange.emit(this.selectedValue);
  }

}
