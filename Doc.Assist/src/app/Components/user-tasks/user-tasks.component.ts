import { DatePipe } from '@angular/common';
import { AfterViewInit, Component, Injectable, Input, OnInit, SimpleChanges } from '@angular/core';
import { DatetimeService } from 'src/app/Services/DatetimeService/datetime.service';

@Component({
  selector: 'app-user-tasks',
  templateUrl: './user-tasks.component.html',
  styleUrls: ['./user-tasks.component.scss'],
})

export class UserTasksComponent implements OnInit,AfterViewInit {
  @Input() grouping: string;
  @Input() task:any;
  @Input() searchQuery: string = '';

  selectedRoom = [];
  showItems = [];
  showAllitems: boolean = true;

  downArrow = './assets/icon/arrow/down.svg';
  constructor(public datetimeFormatService: DatetimeService) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes.searchQuery.currentValue !== changes.searchQuery.previousValue) {
      this.ismatchingSearch();
    }
  }
  
  ngAfterViewInit() {
  }

  selectRoom(i) {
    this.selectedRoom[i] = (this.selectedRoom[i] === true ) ? false : true;
  }

  ismatchingSearch() {
    const activities =  (this.grouping == 'ungrouped') ? this.task.Content:
                        (this.grouping == 'room') ? this.task.zimmerTaskList:
                        this.task.patientTaskList;
    this.showAllitems = false;
    this.showItems = [];
    
    for(let i = 0 ; i < activities.length ; ++i) {
      if(this.grouping == 'ungrouped') {
        const action = this.isMatchingUngrouped(activities[i]);
        this.showItems[i] = action;

        if(action === true)
          this.showAllitems = true;

      } else if (this.grouping == 'room') {
        const action = this.isMatchingRoom(activities[i]);
        this.showItems[i] = action;

        if(action[0] === true)
          this.showAllitems = true;
      } else {
        const action = this.isMatchingPatient(activities[i],
                                              activities[i].RoomName,
                                              activities[i].PatientName);
        this.showItems[i] = action;

        if(action[0] === true)
          this.showAllitems = true;
      }
    }
  }

  isMatchingRoom(activity) {
    if(this.searchQuery === '' || this.searchQuery === null || this.searchQuery === undefined) {
      return new Array(activity.Content.length + 1).fill(true);
    }

    let roomAufgaben = [false];
    for(let i = 0 ; i < activity.Content.length ; ++i) {
      const act = activity.Content[i];
      const datePipe: DatePipe = new DatePipe('en-US');
      const action = act.RoomName.toLowerCase().includes(this.searchQuery) || 
            datePipe.transform(act.Task.TIME_PLANNED, this.datetimeFormatService.timeFormat).toString().toLowerCase().includes(this.searchQuery) ||
            act.PatientName.toLowerCase().includes(this.searchQuery) ||
            this.getActivityName(act.Task).toLowerCase().includes(this.searchQuery);
            
      roomAufgaben.push(action);

      if(action === true) {
        roomAufgaben[0] = true;
      }
    }
    return roomAufgaben;
  }

  isMatchingUngrouped(activity) {
    if(this.searchQuery === '' || this.searchQuery === null || this.searchQuery === undefined) {
      return true;
    }
    const datePipe: DatePipe = new DatePipe('en-US');
    const action = activity.RoomName.toLowerCase().includes(this.searchQuery) || 
          datePipe.transform(activity.Task.TIME_PLANNED, this.datetimeFormatService.timeFormat).toString().toLowerCase().includes(this.searchQuery) ||
          activity.PatientName.toLowerCase().includes(this.searchQuery) ||
          this.getActivityName(activity.Task).toLowerCase().includes(this.searchQuery);
    
    return action;
  }

  isMatchingPatient(activity, roomName, patientName) {
    if(this.searchQuery === '' || this.searchQuery === null || this.searchQuery === undefined) {
      return new Array(activity.Content.length + 1).fill(true);
    }
    
    let roomAufgaben = [false];
    for(let i = 0 ; i < activity.Content.length ; ++i) {
      const act = activity.Content[i];
      const datePipe: DatePipe = new DatePipe('en-US');
      const action = activity.RoomName.toLowerCase().includes(this.searchQuery) || 
            datePipe.transform(act.TIME_PLANNED, this.datetimeFormatService.timeFormat).toString().toLowerCase().includes(this.searchQuery) ||
            activity.PatientName.toLowerCase().includes(this.searchQuery) ||
            this.getActivityName(act).toLowerCase().includes(this.searchQuery);
            
      roomAufgaben.push(action);

      if(action === true) {
        roomAufgaben[0] = true;
      }
    }
    return roomAufgaben;
  }

  getActivityName(activity) {
    return activity.NameOption01 === null ?  activity.ActivityName :
                                            activity.NameOption01.length > 2? activity.NameOption01 : 
                                            activity.ActivityName
  }
}
