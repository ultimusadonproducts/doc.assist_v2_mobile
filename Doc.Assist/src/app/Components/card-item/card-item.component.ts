import { AfterViewInit, Component, ElementRef, Input, NgModule, OnInit, Output, SimpleChange, SimpleChanges, ViewChild, ViewChildren } from '@angular/core';
import { AlertController, IonCard } from '@ionic/angular';
import { createGesture, Gesture } from '@ionic/core';
import { EventEmitter } from '@angular/core';
import { read } from 'fs';
import { CardSelectedService } from 'src/app/Events/CardSelected/card-selected.service';
import { HttpClient } from '@angular/common/http';
import { AppModule } from 'src/app/app.module';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { LoadingService } from 'src/app/Services/LoadingService/loading.service';

@Component({
  selector: 'app-card-item',
  templateUrl: './card-item.component.html',
  styleUrls: ['./card-item.component.scss'],
})
export class CardItemComponent implements OnInit, AfterViewInit {
  @ViewChild(IonCard, {read: ElementRef}) card: ElementRef;

  @Input() swapableCard: boolean;
  @Input() index: string;
  @Input() patient: any;
  @Input() Cares: any;
  @Input() roomName: any;
  @Input() selectedIndex: any;
  @Input() navToProfile: boolean;

  @Output() delete: EventEmitter<string> = new EventEmitter();
  @Output() selectCard: EventEmitter<string> = new EventEmitter();

  cardOpened = false;
  deleteIcon = './assets/icon/delete.svg'
  showDeleteButton = false;
  
  constructor(private cardselected: CardSelectedService,
              private alertController: AlertController,
              private http: HttpClient,
              private router: Router,
              private storage: Storage,
              private loadingService: LoadingService) { }

  ngOnInit() {
    if(this.swapableCard) {
      this.cardselected.getObservable().subscribe(data => {
        this.selectedIndex = data.index;

        if(this.selectedIndex != this.index && this.cardOpened) {
          this.cardOpened = false;
          this.retransform(this.card, 0, 0.5);
        }
      })
    }
  }
  ngAfterViewInit() {
    if(this.swapableCard)
      this.applySwapGesture();
    this.showDeleteButton = true;
  }

  selectingCard() {
    this.cardselected.publishSomeData({
      index: this.index
    })
  }

  async deleteMe() {
    if(this.cardOpened)
    {
      await this.presentAlert();
      //await this.delete.emit(this.index);
    }
  }

  applySwapGesture() {    
    const style = window.getComputedStyle(document.body);
    const deleteButtonWidth = Number.parseFloat(style.getPropertyValue('--delete-card-width').replace('px', '')) + 8;

    let lastDeltaX = 0;
      const gesture = createGesture({
        el: this.card.nativeElement,
        gestureName: 'swap-gesture',
        gesturePriority: 300,
        onStart: ev => {
          this.card.nativeElement.style.transitionDuration  = '';
          this.selectingCard();
        },
        onMove: ev => { lastDeltaX = this.swapCard(ev, this.card, deleteButtonWidth, lastDeltaX) },
        
        onEnd: ev => {
          lastDeltaX = this.swapEnd(this.card, deleteButtonWidth, ev.deltaX);
        }
      });

      gesture.enable(true);
  }

  swapEnd(card, deleteButtonWidth, deltaX) {
    const style = window.getComputedStyle(card.nativeElement)
    let matrix = new WebKitCSSMatrix(style.transform);
    let currentPostion = matrix.m41;
    
    if((currentPostion >= (deleteButtonWidth/4) && deltaX  >= 0)
      || (currentPostion > (3 * deleteButtonWidth/4) && deltaX  < 0)){
      this.retransform(card, deleteButtonWidth, 0.5)
      this.cardOpened = true;
    } else if ((currentPostion <= (3 * deleteButtonWidth/4) && deltaX  < 0)
              || (currentPostion < (deleteButtonWidth/4) && deltaX  >= 0)) {
      this.retransform(card, 0, 0.5)
      this.cardOpened = false;
    }

    return 0;
  }

  swapCard(ev, card, deleteButtonWidth, lastDeltaX) {
    const style = window.getComputedStyle(card.nativeElement)
    let matrix = new WebKitCSSMatrix(style.transform);
    let currentPostion = matrix.m41;

    this.retransform(card, this.getNewPosition(ev.deltaX, currentPostion, deleteButtonWidth, lastDeltaX) , 0)

    return ev.deltaX;
  }

  getNewPosition(deltaX, currentPostion, deleteButtonWidth, lastDeltaX) {

    let newPosition = (currentPostion + (deltaX - lastDeltaX))
    return (newPosition > deleteButtonWidth ? deleteButtonWidth : 
      newPosition < 0 ? 0 : newPosition);
  }

  retransform(card, position, duration) {
    card.nativeElement.style.transform = 'translateX(' + position + 'px)';
    card.nativeElement.style.transitionDuration  = duration + 's'
  }

  async callDeleteService() {
    this.loadingService.presentLoading();

    await this.http.post(AppModule.baseURL + '/UserPatientConnection/DisconnectUserToPatient', 
    {
      "userID": AppModule.currentUser.ID,
      "patientID": this.patient.ID
    }).subscribe(
      async deleted => 
      {
        await this.delete.emit(this.index);
        this.loadingService.dismissLoading();
      },
      error => {this.loadingService.dismissLoading();});
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Patient löschen!',
      message: 'Sie wollen ' + this.patient.FirstName + ' ' + this.patient.LastName + ' löschen?',
      buttons: [
        {
          text: 'Abbrechen',
          role: 'cancel',
        }, {
          text: 'Löschen',
          handler: () => {
            this.callDeleteService();
          }
        }
      ],
      mode: "ios"
    });

    await alert.present();
  }

  async openPatientProfile() {
    if(this.navToProfile == true) {
      await this.storage.set('selectedPatient', this.patient).then(
        () => {
          this.router.navigate(['/patient-profile']);
        });
    }
  }

}
