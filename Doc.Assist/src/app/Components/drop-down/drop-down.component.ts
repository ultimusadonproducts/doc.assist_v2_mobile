import { Component, Input, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-drop-down',
  templateUrl: './drop-down.component.html',
  styleUrls: ['./drop-down.component.scss'],
})
export class DropDownComponent implements OnInit {

  inputLimit: number = 20;
  searchTerm: string = null;

  @Input() title: string;
  @Input() list: any;
  @Input() disabled: boolean = false;
  @Input() placeholder: string = 'Auswählen…';
  @Input() selectedValue;
  @Input() inline: boolean = false;
  @Input() canSearch: boolean = false;
  @Output() selectedValueChange = new EventEmitter();
  @Output() itemChange = new EventEmitter();

  dropDownOn = false;
  displayMenu = 'none';

  constructor() {}

  ngOnInit() {
    for(let i = 0 ; i < this.list.length ; ++i) {
      this.list[i].index = i;
    }
  }

  toggleDropDown() {
    if(this.disabled) return;
    switch(this.displayMenu){
      case 'none':
        this.displayMenu = 'block';
        this.dropDownOn = true;
        break;
        case 'block':
          this.displayMenu = 'none';
          this.dropDownOn = false;
        break;
    }
  }

  itemSelected(i) {
    this.toggleDropDown();
    this.placeholder = this.list[i].text;
    this.itemChange.emit(this.list[i].value);
    this.selectedValueChange.emit(this.list[i].value);
    /*if(this.returnClickEvent)
    else*/
  }

  getPlaceholder() {
    return this.placeholder === null || this.placeholder === undefined ? 'Select Item' :
            this.placeholder.length >= this.inputLimit ? this.placeholder.substring(0, this.inputLimit - 1) + ' ...' :
                                                        this.placeholder;
  }

}
