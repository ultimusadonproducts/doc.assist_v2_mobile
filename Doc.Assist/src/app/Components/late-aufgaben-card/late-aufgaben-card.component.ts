import { DatePipe } from '@angular/common';
import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-late-aufgaben-card',
  templateUrl: './late-aufgaben-card.component.html',
  styleUrls: ['./late-aufgaben-card.component.scss'],
})
export class LateAufgabenCardComponent implements OnInit {
  overdueIcon = './assets/icon/overdue.svg';
  showTask: boolean = true;

  @Input() taskName: string;
  @Input() roomName: string;
  @Input() patientName: string;
  @Input() lateAmount: string;
  @Input() activity: any;
  @Input() searchQuery: string = '';
  
  constructor(private storage: Storage, private router: Router) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes.searchQuery.currentValue !== changes.searchQuery.previousValue) {
      this.isMatchingSearch();
    }
  }

  goToDocumentTask() {
    this.storage.set('selectedTask', this.activity).then(() => {
      this.router.navigate(['/document-task']);
    });
  }

  isMatchingSearch() {
    if(this.searchQuery === '' || this.searchQuery === null || this.searchQuery === undefined) {
      this.showTask = true;
    }
    const datePipe: DatePipe = new DatePipe('en-US');
    this.showTask = this.roomName.toLowerCase().includes(this.searchQuery) || 
          datePipe.transform(this.activity.TIME_PLANNED, 'HH:mm').toString().toLowerCase().includes(this.searchQuery) ||
          this.patientName.toLowerCase().includes(this.searchQuery) ||
          this.getActivityName(this.activity).toLowerCase().includes(this.searchQuery);    
  }

  getActivityName(activity) {
    return activity.NameOption01 === null ?  activity.ActivityName :
                                            activity.NameOption01.length > 2? activity.NameOption01 : 
                                            activity.ActivityName
  }

}
