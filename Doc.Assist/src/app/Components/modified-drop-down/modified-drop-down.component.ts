import { Component, Input, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { DatetimeService } from 'src/app/Services/DatetimeService/datetime.service';

@Component({
  selector: 'app-modified-drop-down',
  templateUrl: './modified-drop-down.component.html',
  styleUrls: ['./modified-drop-down.component.scss'],
})
export class ModifiedDropDownComponent implements OnInit {
  inputLimit: number = 20;

  @Input() title: string;
  @Input() list: any;
  @Input() disabled: boolean = false;
  @Input() placeholder: string = 'Auswählen…';
  @Input() selectedValueIndex;
  @Input() inline: boolean = false;
  @Output() selectedValueIndexChange = new EventEmitter();
  @Output() itemChange = new EventEmitter();

  constructor(public datetimeFormatService: DatetimeService) {}

  ngOnInit() {
    
  }

  itemSelected(i) {
    this.selectedValueIndex = i;
    //this.placeholder = this.list[i].text;
    this.itemChange.emit(this.list[i].value);
    this.selectedValueIndexChange.emit(this.list[i].value);
    /*if(this.returnClickEvent)
    else*/
  }

  /*getPlaceholder() {
    const datePipe: DatePipe = new DatePipe('en-US');

    return this.placeholder === null || this.placeholder === undefined ? 'Select Item' :
            this.placeholder.length >= this.inputLimit ? this.placeholder.substring(0, this.inputLimit - 1) + ' ...' + ' , ' + 
                                                          datePipe.transform(this.list[this.selectedValueIndex].date, this.datetimeFormatService.datetimeFormat).toString() :
                                                        this.placeholder + ' , ' + 
                                                          datePipe.transform(this.list[this.selectedValueIndex].date, this.datetimeFormatService.datetimeFormat).toString();
  }

  get*/
}
