import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { AppModule } from 'src/app/app.module';
import { SpeechRecognition } from '@ionic-native/speech-recognition/ngx';
import { HomeService } from 'src/app/Events/Home/home.service';
import { EventsEnum } from 'src/app/Enums/event.enum';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-add-event',
  templateUrl: './add-event.page.html',
  styleUrls: ['./add-event.page.scss'],
})
export class AddEventPage implements OnInit {
  close = './assets/icon/close.svg';
  voiceRecorderInactive = './assets/icon/VoiceRecorder/Voice_inactive.png';
  voiceRecorderActive = './assets/icon/VoiceRecorder/Voice_active.png';

  remarks: string = '';

  eventsList: any = [];
  eventsDropDownList: {value: number; text: string; index: number}[] = [];

  patientsList: any = [];
  patientsDropDownList = [];

  selectedEvent: number = -1;
  selectedPatient: number = -1;

  isOtherEvent: boolean = false;
  otherEventValue: string = '';

  constructor(private modelController: ModalController,
              private http: HttpClient,
              private toastCtrl: ToastController,
              private speechRecognition: SpeechRecognition,
              private cd: ChangeDetectorRef,
              private homeService: HomeService,
              private translate: TranslateService) { }

  ngOnInit() {
    this.getEvents();
    this.getPatients();
  }

  dismiss() {
    this.modelController.dismiss();
  }

  getEvents() {
    this.http.post(AppModule.baseURL + '/SideList/GetEventsLst', null)
    .subscribe(events => {
      this.eventsList = events;
      for(let i = 0 ; i < this.eventsList.length; ++i) {
        this.eventsDropDownList.push({
          value: i,
          text: this.eventsList[i].Header,
          index: i
        })
      }
      this.eventsDropDownList.push({value: EventsEnum.otherEvent, text: this.translate.instant('ADD_EVENT.OTHER'), index: EventsEnum.otherEvent});
    })
  }

  getPatients() {
    this.http.post(AppModule.baseURL + '/SideList/GetUserAssignedPatientsLst', 
    {
      UserID: AppModule.currentUser.ID,
      SelectedStations: AppModule.currentUser.Stations
    })
    .subscribe(patients => {
      this.patientsList = patients;
      for(let i = 0 ; i < this.patientsList.length; ++i) {
        this.patientsDropDownList.push({
          value: i,
          text: this.patientsList[i].Name,
          index: i
        })
      }
    })
  }

  addPatientEvent() {
    console.log(this.selectedEvent);

    this.http.post(AppModule.baseURL + '/SideList/AddNewPatientEvent', {
      CategoryID: this.isOtherEvent ? 0 : this.eventsList[this.selectedEvent].CategoryID,
      EventID: this.isOtherEvent ? EventsEnum.otherEvent :this.eventsList[this.selectedEvent].ID,
      Header: this.isOtherEvent ? this.otherEventValue : this.eventsList[this.selectedEvent].Header,
      PatientID: this.patientsList[this.selectedPatient].ID,
      Remarks: this.remarks,
      UserID: AppModule.currentUser.ID,
      isOtherEvent: this.isOtherEvent
    }).subscribe(data => {
      this.homeService.publishSomeData({
        closePopover: true,
      })
      if(data == true) {
        this.modelController.dismiss().then(() => {
          this.showToast(true);
        })
      } else {
        this.modelController.dismiss().then(() => {
          this.showToast(false);
        })
        //AppModule
      }
    })
  }

  async showToast(success: boolean) {
    const toast = await this.toastCtrl.create({
      message: '<ion-icon style="width: 20px; height: 20px;" src="./assets/icon/' 
                + ((success == true) ? 'success' :  'fail')+ '.svg"></ion-icon>'
                + ((success == true) ? 'Gespeichert' :  'Fehler'),
      duration: 2000,
      mode: 'ios',
      cssClass: 'toast-css-class-' + ((success == true) ? 'success' :  'fail')
    });
    toast.present();
  }

  Record() {
    this.speechRecognition.hasPermission()
    .then((hasPermission: boolean) => {
      if(hasPermission){
        this.startRecording();
      } else {
        this.requestRecordPermission();
      }
    })
  }

  requestRecordPermission() {
    this.speechRecognition.requestPermission()
    .then(
      () => this.startRecording(),
      () => {}
    )
  }

  startRecording() {
    const option = {
      language: 'de-DE',
    };
    this.speechRecognition.startListening(option)
    .subscribe(
      (matches: string[]) => 
      {
        //console.log(matches);
        this.remarks = matches[0];
        this.cd.detectChanges();
      },
      (onerror) => console.log('error:', onerror)
    )
  }

  eventChange(eventIndex) {
    if(eventIndex === EventsEnum.otherEvent) {
      this.isOtherEvent = true;
    } else {
      this.isOtherEvent = false;
    }
  }

}
