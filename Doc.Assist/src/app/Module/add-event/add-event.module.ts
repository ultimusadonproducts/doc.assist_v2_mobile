import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddEventPageRoutingModule } from './add-event-routing.module';

import { AddEventPage } from './add-event.page';
import { DropDownComponent } from 'src/app/Components/drop-down/drop-down.component';
import { CustomComponents } from 'src/app/Components/components.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddEventPageRoutingModule,
    CustomComponents,
    TranslateModule
  ],
  declarations: [AddEventPage]
})
export class AddEventPageModule {}
