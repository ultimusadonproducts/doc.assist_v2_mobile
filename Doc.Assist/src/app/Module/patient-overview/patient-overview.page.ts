import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { PatientService } from 'src/app/Services/PatientService/patient.service';

@Component({
  selector: 'app-patient-overview',
  templateUrl: './patient-overview.page.html',
  styleUrls: ['./patient-overview.page.scss'],
})
export class PatientOverviewPage implements OnInit {
  @Input() mainPatients = null;
  @Input() otherPatients = null;
  @Input() windowHeight = null;

  showOtherPatinet: boolean = false;
  close = './assets/icon/close.svg';
  constructor(private modelController: ModalController,
              private patientService: PatientService) { }

  ngOnInit() {
  }

  dismiss() {
    this.modelController.dismiss();
  }

  navToPatientProfile(id) {
    this.dismiss();
    this.patientService.navToPatientProfileByPatientId(id);
  }

}
