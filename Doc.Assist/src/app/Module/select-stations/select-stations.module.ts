import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelectStationsPageRoutingModule } from './select-stations-routing.module';

import { SelectStationsPage } from './select-stations.page';
import { CustomComponents } from 'src/app/Components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectStationsPageRoutingModule,
    CustomComponents
  ],
  declarations: [SelectStationsPage]
})
export class SelectStationsPageModule {}
