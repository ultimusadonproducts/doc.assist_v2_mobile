import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-select-stations',
  templateUrl: './select-stations.page.html',
  styleUrls: ['./select-stations.page.scss'],
})
export class SelectStationsPage implements OnInit {

  @Input() stations: any = [];
  selectedStations: any = [];
  constructor() { }

  ngOnInit() {
    for(let i = 0 ; i < this.stations.length ; ++i) {
      this.selectedStations.push(i);
    }
  }

}
