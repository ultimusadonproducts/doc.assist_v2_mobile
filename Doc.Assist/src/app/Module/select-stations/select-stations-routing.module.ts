import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectStationsPage } from './select-stations.page';

const routes: Routes = [
  {
    path: '',
    component: SelectStationsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelectStationsPageRoutingModule {}
