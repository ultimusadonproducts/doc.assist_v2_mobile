import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddPatientPageRoutingModule } from './add-patient-routing.module';

import { AddPatientPage } from './add-patient.page';
import { AddPatientRoomCardComponent } from 'src/app/Components/add-patient-room-card/add-patient-room-card.component';
import { AddPatientCareCardComponent } from 'src/app/Components/add-patient-care-card/add-patient-care-card.component';
import { CustomComponents } from 'src/app/Components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddPatientPageRoutingModule,
    CustomComponents
  ],
  declarations: [AddPatientPage]
})
export class AddPatientPageModule {}
