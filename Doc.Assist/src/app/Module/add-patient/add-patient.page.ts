import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { App } from '@capacitor/core';
import { AlertController, ModalController, NavController } from '@ionic/angular';
import { AppModule } from 'src/app/app.module';
import { AddPatientModuleService } from 'src/app/Events/AddPatient/add-patient-module.service';
import { AufgabenTabService } from 'src/app/Events/AufgabenTab/aufgaben-tab.service';
import { ConnectPatientsService } from 'src/app/Events/ConnectPatients/connect-patients.service';
import { PatientTabService } from 'src/app/Events/PatientTab/patient-tab.service';
import { LoadingService } from 'src/app/Services/LoadingService/loading.service';

@Component({
  selector: 'app-add-patient',
  templateUrl: './add-patient.page.html',
  styleUrls: ['./add-patient.page.scss'],
})
export class AddPatientPage implements OnInit {
  trashIcon = './assets/icon/delete.svg'
  close = './assets/icon/close.svg';
  
  
  selectedUpperPatientTab = 1;

  selectedCardsCount = 0;
  connectedCardsCount = 0;
  problemDetected = false;

  patientsGroupedByRoom = [];
  patientsGroupedByUser = [];

  constructor(private modelController: ModalController,
              private http: HttpClient,
              private connectPatinet: ConnectPatientsService,
              private loadingController: LoadingService,
              private nav: NavController,
              private patientTab: PatientTabService,
              private addPatientModule: AddPatientModuleService,
              private alertController: AlertController,
              private aufgabenTabService: AufgabenTabService) { }

  async ngOnInit() {
    AppModule.moduleNumber++;

    await this.getPatientsGroupedByRoom()
    await this.getPatientsGroupedbyUser();

    await this.addPatientModule.getObservable().subscribe(async data =>
      {
        if(data.cardSelected == true) {
          this.selectedCardsCount++;
          return;
        }
        else if(data.cardSelected == false) {
          this.selectedCardsCount--;
          return;
        }

        // For Connecting the Patients to the Current User
        if(data.connected == false && !this.problemDetected) {
          this.problemDetected = true;
          
          this.loadingController.dismissLoading();

          await this.presentAlert();
          //await this.modelController.dismiss();
          return;
        } else if(data.connected == true && ++this.connectedCardsCount == this.selectedCardsCount) {
          await this.refreshTasks();
          return;
        }

        if(data.patientRefreshed == true){
          this.loadingController.dismissLoading();
            
          await this.modelController.dismiss();
          return;
        }
          
      });
  }

  tabClicked(tabNumber: number) {
    this.selectedUpperPatientTab = tabNumber;
  }
  dismiss() {
    this.modelController.dismiss();
  }

  async getPatientsGroupedByRoom() {
    await this.http.post(AppModule.baseURL + '/NewPatient/GetOtherPatientsGroupedByRoom', 
    {
      UserID : AppModule.currentUser.ID,
      Scope : AppModule.ActionOnFeature['Scope'][0]
    }).subscribe(async (data: any[]) => 
      {
        this.patientsGroupedByRoom = data;
      }
    );  
  }

  async getPatientsGroupedbyUser() {
    await this.http.post(AppModule.baseURL + '/NewPatient/GetOtherPatientsGroupedByCarePerson', 
    {
      UserID : AppModule.currentUser.ID,
      Scope : AppModule.ActionOnFeature['Scope'][0]
    })
    .subscribe(async (data: any[]) => 
      {
        this.patientsGroupedByUser = data;
      }
    );  
  }

  getCurrentUserID() {
    return AppModule.currentUser.ID;
  }

  async connectPatients(){
    await this.presentLoading();

    this.connectPatinet.publishSomeData({
      connect: true,
      ModuleNumber: this.getModuleNumber()
    });
  }

  async presentLoading() {
    this.loadingController.presentLoading();
  }

  getModuleNumber() {
    return AppModule.moduleNumber;
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Ein Problem ist aufgetreten!',
      message: 'Ein Problem ist aufgetreten. Bitte kontaktieren Sie Ihren Administrator.',
      buttons: [{
          text: 'OK',
          handler: () => {
            this.modelController.dismiss();
          }
        }
      ],
      mode: "ios"
    });

    await alert.present();
  }

  async refreshTasks() {
    await this.patientTab.publishSomeData({
      refresh: true
    });
    await this.aufgabenTabService.publishSomeData({
      refresh: true
    });
  }

}
