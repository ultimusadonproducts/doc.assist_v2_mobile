import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { SpeechRecognition } from '@ionic-native/speech-recognition/ngx';
import { ModalController, ToastController } from '@ionic/angular';
import { AppModule } from 'src/app/app.module';
import { HomeService } from 'src/app/Events/Home/home.service';

@Component({
  selector: 'app-add-bedarf',
  templateUrl: './add-bedarf.component.html',
  styleUrls: ['./add-bedarf.component.scss'],
})
export class AddBedarfComponent implements OnInit {

  close = './assets/icon/close.svg';
  voiceRecorderInactive = './assets/icon/VoiceRecorder/Voice_inactive.png';
  voiceRecorderActive = './assets/icon/VoiceRecorder/Voice_active.png';

  remarks: string = '';

  bedarfList: any = [];
  bedarfDropDownList = [];

  patientsList: any = [];
  patientsDropDownList = [];

  selectedBedarf: number = -1;
  selectedPatient: number = -1;

  constructor(private http: HttpClient,
              private modalController: ModalController,
              private speechRecognition: SpeechRecognition,
              private cd: ChangeDetectorRef,
              private toastCtrl: ToastController,
              private homeService: HomeService) { }

  ngOnInit() {
    //this.getBedarf();
    this.getPatients();
  }

  dismiss() {
    return this.modalController.dismiss();
  }

  getBedarf(patientID) {
    this.http.post(AppModule.baseURL + '/SideList/GetPerDemandActivities', patientID)
    .subscribe(bedarf => {
      this.bedarfList = bedarf;
      for(let i = 0 ; i < this.bedarfList.length; ++i) {
        this.bedarfDropDownList.push({
          value: i,
          text: this.bedarfList[i].Name,
          index: i
        })
      }
    })
  }

  changeSelectedPatient(id) {
    this.selectedPatient = id;
    const patientID = this.patientsList[this.selectedPatient].ID;
    this.getBedarf(patientID);
  }

  getPatients() {
    this.http.post(AppModule.baseURL + '/SideList/GetUserAssignedPatientsLst', 
    {
      UserID: AppModule.currentUser.ID,
      SelectedStations: AppModule.currentUser.Stations
    }).subscribe(patients => {
      this.patientsList = patients;
      for(let i = 0 ; i < this.patientsList.length; ++i) {
        this.patientsDropDownList.push({
          value: i,
          text: this.patientsList[i].Name,
          index: i
        })
      }
    })
  }

  addDocumentedActivity() {
    this.http.post(AppModule.baseURL + '/SideList/AddNewDocumentedActivity', {
      ActivityID: this.bedarfList[this.selectedBedarf].ID,
      PatientID: this.patientsList[this.selectedPatient].ID,
      Remarks: this.remarks,
      EventID: 0,
      Value: '',
      UserID: AppModule.currentUser.ID
    }).subscribe(data => {
      this.homeService.publishSomeData({
        closePopover: true,
      })
      if(data == true) {
        this.dismiss().then(() => {
          this.showToast(true);
        })
      } else {
        this.dismiss().then(() => {
          this.showToast(false);
        })
        //AppModule
      }
    })
  }

  async showToast(success: boolean) {
    const toast = await this.toastCtrl.create({
      message: '<ion-icon style="width: 20px; height: 20px;" src="./assets/icon/' 
                + ((success == true) ? 'success' :  'fail')+ '.svg"></ion-icon>'
                + ((success == true) ? 'Gespeichert' :  'Fehler'),
      duration: 2000,
      mode: 'ios',
      cssClass: 'toast-css-class-' + ((success == true) ? 'success' :  'fail')
    });
    toast.present();
  }

  Record() {
    this.speechRecognition.hasPermission()
    .then((hasPermission: boolean) => {
      if(hasPermission){
        this.startRecording();
      } else {
        this.requestRecordPermission();
      }
    })
  }

  requestRecordPermission() {
    this.speechRecognition.requestPermission()
    .then(
      () => this.startRecording(),
      () => {}
    )
  }

  startRecording() {
    const option = {
      language: 'de-DE',
    };
    this.speechRecognition.startListening(option)
    .subscribe(
      (matches: string[]) => 
      {
        //console.log(matches);
        this.remarks = matches[0];
        this.cd.detectChanges();
      },
      (onerror) => console.log('error:', onerror)
    )
  }

}
