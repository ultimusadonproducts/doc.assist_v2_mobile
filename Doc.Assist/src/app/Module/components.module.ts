import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { AddBedarfComponent } from './add-bedarf/add-bedarf.component';
import { CustomComponents } from '../Components/components.module';


@NgModule({
  declarations: [],
  exports: [],
  imports: [IonicModule.forRoot(), FormsModule, TranslateModule]
})

export class ModulesComponents {

}
