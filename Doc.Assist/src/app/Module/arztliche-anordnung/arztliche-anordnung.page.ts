import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AppModule } from 'src/app/app.module';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { HomeService } from 'src/app/Events/Home/home.service';

@Component({
  selector: 'app-arztliche-anordnung',
  templateUrl: './arztliche-anordnung.page.html',
  styleUrls: ['./arztliche-anordnung.page.scss'],
})
export class ArztlicheAnordnungPage implements OnInit {

  close = './assets/icon/close.svg';
  voiceRecorderInactive = './assets/icon/VoiceRecorder/Voice_inactive.png';
  voiceRecorderActive = './assets/icon/VoiceRecorder/Voice_active.png';

  patientsList: any = [];
  patientsDropDownList = [];

  selectedPatient: number = -1;

  constructor(private http: HttpClient,
              private modalController: ModalController,
              private storage: Storage,
              private router: Router,
              private homeService: HomeService) { }

  ngOnInit() {
    this.getPatients();
  }

  dismiss() {
    return this.modalController.dismiss();
  }

  getPatients() {
    this.http.post(AppModule.baseURL + '/SideList/GetUserWholeAssignedPatientsLst', 
    {
      UserID: AppModule.currentUser.ID,
      SelectedStations: AppModule.currentUser.Stations
    })
    .subscribe(patients => {
      this.patientsList = patients;
      for(let i = 0 ; i < this.patientsList.length; ++i) {
        this.patientsDropDownList.push({
          value: i,
          text: this.patientsList[i].FirstName + ' ' + this.patientsList[i].LastName,
          index: i
        })
      }
    })
  }

  async goToPatientPage() {
    await this.storage.set('selectedPatient', this.patientsList[this.selectedPatient]).then(
      () => {
        this.homeService.publishSomeData({
          closePopover: true,
        })
        this.router.navigate(['/patient-profile']).then(() => {
          this.dismiss();
        })
      });
  }

}
