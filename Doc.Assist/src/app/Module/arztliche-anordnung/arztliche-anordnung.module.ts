import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ArztlicheAnordnungPageRoutingModule } from './arztliche-anordnung-routing.module';

import { ArztlicheAnordnungPage } from './arztliche-anordnung.page';
import { DropDownComponent } from 'src/app/Components/drop-down/drop-down.component';
import { CustomComponents } from 'src/app/Components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ArztlicheAnordnungPageRoutingModule,
    CustomComponents
  ],
  declarations: [ArztlicheAnordnungPage]
})
export class ArztlicheAnordnungPageModule {}
