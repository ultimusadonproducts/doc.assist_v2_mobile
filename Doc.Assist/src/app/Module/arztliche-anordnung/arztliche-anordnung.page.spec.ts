import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ArztlicheAnordnungPage } from './arztliche-anordnung.page';

describe('ArztlicheAnordnungPage', () => {
  let component: ArztlicheAnordnungPage;
  let fixture: ComponentFixture<ArztlicheAnordnungPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ArztlicheAnordnungPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ArztlicheAnordnungPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
