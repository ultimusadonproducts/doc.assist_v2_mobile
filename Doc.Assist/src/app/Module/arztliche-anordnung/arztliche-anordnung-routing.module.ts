import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ArztlicheAnordnungPage } from './arztliche-anordnung.page';

const routes: Routes = [
  {
    path: '',
    component: ArztlicheAnordnungPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ArztlicheAnordnungPageRoutingModule {}
