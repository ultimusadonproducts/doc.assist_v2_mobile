import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ModalController, Platform, ToastController } from '@ionic/angular';
import { pickerController } from '@ionic/core';
import { AppModule } from 'src/app/app.module';
import { SpeechRecognition } from '@ionic-native/speech-recognition/ngx';
import { HomeService } from 'src/app/Events/Home/home.service';
import { VitalColumns } from 'src/app/Enums/vital-columns.enum';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-adhoc',
  templateUrl: './adhoc.page.html',
  styleUrls: ['./adhoc.page.scss'],
})
export class AdhocPage implements OnInit {
  select = 'select';
  document = 'document';

  status = 'select';

  taskColumns = {
    colCount: 6,
    colName: ['1', '2', '3', '4', '.', '.1'],
    colMin: [0, 0, 0, 0, 0, 0],
    colMax: [9, 9, 9, 9, 9, 9],
    defaultValues: [0, 0, 0, 0, 0, 0],
    colIncreament: [1, 1, 1, 1, 1, 1],
    colDotPostion: 5,
  }

  taskColumns01 = {
    unit: true,
    unitStr: "",
    wheels: [
    ],
  };

  taskColumns02 = {
    unit: true,
    unitStr: "",
    wheels: [
    ],
  };

  value01 = 0;
  value02 = 0;

  patientsList: any = [];
  patientsDropDownList = [];
  selectedPatient: number = -1;

  activityList: any = [];
  activityDropDownList = [];
  selectedactivity: number = -1;
  activity: any = {};

  eventsList: any = [];
  eventsDropDownList = [{text: 'kein Ereignis', value: -1, index: 0, date: null}];
  selectedEventIndex: number = -1;

  remarks = '';
  freeText = '';

  close = './assets/icon/close.svg';
  voiceRecorderInactive = './assets/icon/VoiceRecorder/Voice_inactive.png';
  voiceRecorderActive = './assets/icon/VoiceRecorder/Voice_active.png';

  constructor(private modalController: ModalController,
              private http: HttpClient,
              private toastCtrl: ToastController,
              private speechRecognition: SpeechRecognition,
              private cd: ChangeDetectorRef,
              private homeService: HomeService,
              private platform: Platform,
              private iab: InAppBrowser) { }

  ngOnInit() {
    this.getPatients();
    this.getAdhocActivities();
  }

  dismiss() {
    return this.modalController.dismiss();
  }

  goToDocumentPhase() {
    this.handleWheelColumns();
    this.status = this.document;
  }

  openWheelSelector() {
    const columns = this.generateColumns(this.taskColumns)
    this.pushWheel(columns);
  }

  generateColumns(taskColumns){
    let columns = [];
    for(let i = 0 ; i < taskColumns.colCount ; ++i) {
      columns.push({
        name: 'col' + taskColumns.colName[i],
        options: this.generateColumn(taskColumns.colMin[i], taskColumns.colMax[i], 
                                    taskColumns.colIncreament[i], (taskColumns.colDotPostion == i + 1 ? true : false)),
        selectedIndex: 0
      });
    }

    return columns;
  }

  generateColumn(columnMin, columnMax, columnIcrement, dotPostion) {
    if(dotPostion)
      return [this.generateColumnText('.', '.')];

    let colItems = [];
    for(let i = columnMin ; i <= columnMax ;  i = i + columnIcrement) {
      colItems.push(this.generateColumnText(i.toString(), i));
    }

    return colItems;
  }

  generateColumnText(text, value) {
    return { 
            text: text,
            value: value
          }
  }

  async pushWheel(cols) {
    const picker = await pickerController.create({
      columns: cols,
      buttons: [
        {
          text: 'Abbrechen',
          role: 'cancel'
        },
        {
          text: 'Ändern',
          handler: (cols) => {
            let selectedValue = cols['col4'].value
                                + cols['col.1'].value / 10
                                + cols['col1'].value * 1000
                                + cols['col2'].value * 100
                                + cols['col3'].value * 10;

            this.value01 = selectedValue;


          }
        }
      ],
      mode: 'ios'
    })

    await picker.present().then(() => {
      /*var columns = document.getElementsByTagName('ion-picker-column');
      columns[0].style.minWidth = '200px';*/
      }
    )
  }

  selectPatient(id) {
    this.selectedPatient = this.patientsList[id].ID;
  }

  selectEvent(id) {
    this.selectedEventIndex = (id < 0) ? -1 : this.eventsList[id].ID;
  }

  selectActivity(id) {
    this.selectedactivity = this.activityList[id].ID;
  }

  getPatients() {
    this.http.post(AppModule.baseURL + '/SideList/GetUserAssignedPatientsLst', 
    {
      UserID: AppModule.currentUser.ID,
      SelectedStations: AppModule.currentUser.Stations
    })
    .subscribe(patients => {
      this.patientsList = patients;
      for(let i = 0 ; i < this.patientsList.length; ++i) {
        this.patientsDropDownList.push({
          value: i,
          text: this.patientsList[i].Name,
          index: i
        })
      }
    })
  }

  getEvents(patientID) {
    this.http.post(AppModule.baseURL + '/SideList/GetPatientAssignedEvents', this.patientsList[patientID].ID )
    .subscribe(events => {
      this.eventsList = events;
      this.eventsDropDownList = [];
      for(let i = 0 ; i < this.eventsList.length; ++i) {
        this.eventsDropDownList.push({
          value: i,
          text: this.eventsList[i].Name,
          index: i,
          date: this.eventsList[i].Date
        })
      }
      //console.log(this.eventsDropDownList)
    })
  }

  getAdhocActivities() {
    this.http.post(AppModule.baseURL + '/SideList/GetAdhocActivities', null)
    .subscribe(activities => {
      this.activityList = activities;
      for(let i = 0 ; i < this.activityList.length; ++i) {
        this.activityDropDownList.push({
          value: i,
          text: this.activityList[i].NameOption01.length > 2 ? this.activityList[i].NameOption01 : this.activityList[i].Name,
          index: i,
        })
      }
    })
  }

  addDocumentedActivity() {
    this.http.post(AppModule.baseURL + '/SideList/AddNewDocumentedActivity', {
      ActivityID: this.activityList[this.selectedactivity].ID,
      PatientID: this.patientsList[this.selectedPatient].ID,
      Remarks: this.remarks,
      EventID: (this.selectedEventIndex < 0) ? -1 : this.eventsList[this.selectedEventIndex].ID,
      Value: this.value01,
      UserID: AppModule.currentUser.ID
    }).subscribe(data => {
      if(this.activity.InputValues > 1) {
        this.http.post(AppModule.baseURL + '/SideList/AddNewDocumentedActivity', {
          ActivityID: this.activityList[this.selectedactivity].ID,
          PatientID: this.patientsList[this.selectedPatient].ID,
          Remarks: this.remarks,
          EventID: (this.selectedEventIndex < 0) ? -1 : this.eventsList[this.selectedEventIndex].ID,
          Value: this.value02,
          UserID: AppModule.currentUser.ID
        }).subscribe(data => {
          this.homeService.publishSomeData({
            closePopover: true,
          })
          if(data == true) {
            this.dismiss().then(() => {
              this.showToast(true);
            })
          } else {
            this.dismiss().then(() => {
              this.showToast(false);
            })
          }
        })
      }
      else {
        this.homeService.publishSomeData({
          closePopover: true,
        })
        if(data == true) {
          this.dismiss().then(() => {
            this.showToast(true);
          })
        } else {
          this.dismiss().then(() => {
            this.showToast(false);
          })
        }
      }
    })
    
    //console.log(this.value);
  }

  async showToast(success: boolean) {
    const toast = await this.toastCtrl.create({
      message: '<ion-icon style="width: 20px; height: 20px;" src="./assets/icon/' 
                + ((success == true) ? 'success' :  'fail')+ '.svg"></ion-icon>'
                + ((success == true) ? 'Gespeichert' :  'Fehler'),
      duration: 2000,
      mode: 'ios',
      cssClass: 'toast-css-class-' + ((success == true) ? 'success' :  'fail')
    });
    toast.present();
  }

  Record() {
    this.speechRecognition.hasPermission()
    .then((hasPermission: boolean) => {
      if(hasPermission){
        this.startRecording();
      } else {
        this.requestRecordPermission();
      }
    })
  }

  requestRecordPermission() {
    this.speechRecognition.requestPermission()
    .then(
      () => this.startRecording(),
      () => {}
    )
  }

  startRecording() {
    const option = {
      language: 'de-DE',
    };
    this.speechRecognition.startListening(option)
    .subscribe(
      (matches: string[]) => 
      {
        //console.log(matches);
        this.remarks = matches[0];
        this.cd.detectChanges();
      },
      (onerror) => console.log('error:', onerror)
    )
  }

  changeActivity(index) {
    this.selectedactivity = index;
    this.activity = this.activityList[this.selectedactivity];
  }

  isOk() {
    return (this.getDocType() === 'OK');
  }

  isFormular() {
    return (this.getDocType() === 'Formular');
  }

  isWert() {
    return (this.getDocType() === 'Wert')
  }

  isFreiText() {
    return (this.getDocType() === 'Freitext');
  }
  
  isMiniForm() {
    return (this.getDocType() === 'Mini Formular');
  }

  getDocType() {
    if(this.activity.DocType !== undefined && this.activity.DocType !== null) {
      const docType = this.activity.DocType.replace(/^\s+|\s+$/g, '');
      return docType;
    } return '';
  }

  miniFormChanged(value) {
    this.value01 = value;
  }

  handleWheelColumns() {
    this.taskColumns01.unitStr = this.activity.Unit;
    this.taskColumns02.unitStr = this.activity.Unit;
    if(this.activity.RefVitalTableColumn01 == VitalColumns.Temperatur) {
      this.value01 = 35.0; //
      const defult = this.generateDefaultValues(parseFloat(this.activity.DefaultValue01.toString()), [2, 1], true);
      this.taskColumns01.wheels.push({min: 35, max: 44, value: defult[0], beforeDot: false});
      this.taskColumns01.wheels.push({min: 0, max: 9, value: defult[1], beforeDot: true});
    } else if(this.activity.RefVitalTableColumn01 === VitalColumns.Heartrate) {
      this.value01 = 30;
      const defult = this.generateDefaultValues(parseFloat(this.activity.DefaultValue01.toString()), [3], false);
      this.taskColumns01.wheels.push({min: 30, max: 250, value: defult[0], beforeDot: false});
      this.taskColumns01.unit = false;
    } else if (this.activity.RefVitalTableColumn01 === VitalColumns.Pain_Motion ||
      this.activity.RefVitalTableColumn02 === VitalColumns.Pain_NoMotion) {
      this.value01 = 0;
      this.value02 = 0;
      const defult01 = this.generateDefaultValues(parseFloat(this.activity.DefaultValue01.toString()), [2], false);
      const defult02 = this.generateDefaultValues(parseFloat(this.activity.DefaultValue02.toString()), [2], false);
      this.taskColumns01.wheels.push({min: 0, max: 10, value: defult01[0], beforeDot: false});
      this.taskColumns02.wheels.push({min: 0, max: 10, value: defult02[0], beforeDot: false});
      this.taskColumns01.unit = false;
      this.taskColumns02.unit = false;
    } else if (this.activity.RefVitalTableColumn01 === VitalColumns.Blutdruck_SYS ||
      this.activity.RefVitalTableColumn02 === VitalColumns.Blutdruck_DIA) {
      this.value01 = 50.0;      
      this.value02 = 50.0;
      const defult01 = this.generateDefaultValues(parseFloat(this.activity.DefaultValue01.toString()), [3], false);
      const defult02 = this.generateDefaultValues(parseFloat(this.activity.DefaultValue02.toString()), [3], false);
      this.taskColumns01.wheels.push({min: 50, max: 250, value: defult01[0], beforeDot: false});
      this.taskColumns02.wheels.push({min: 50, max: 250, value: defult02[0], beforeDot: false});
    } else if(this.activity.ID === 122) {
      this.value01 = 10;      
      const defult = this.generateDefaultValues(parseFloat(this.activity.DefaultValue01.toString()), [3], false);
      this.taskColumns01.wheels.push({min: 10, max: 250, value: defult[0], beforeDot: false});
    } else if (this.activity.ID === 130) {
      this.value01 = 0;
      this.value02 = 0;
      const defult = this.generateDefaultValues(parseFloat(this.activity.DefaultValue01.toString()), [1, 1, 1], false);
      this.taskColumns01.wheels.push({min: 0, max: 9, value: defult[0], beforeDot: false});
      this.taskColumns01.wheels.push({min: 0, max: 9, value: defult[1], beforeDot: false});
      this.taskColumns01.wheels.push({min: 0, max: 9, value: defult[2], beforeDot: false});
      this.taskColumns01.unit = false;
    } else if(this.activity.ID === 101) {
      this.value01 = 10;      
      const defult = this.generateDefaultValues(parseFloat(this.activity.DefaultValue01.toString()), [3], false);
      this.taskColumns01.wheels.push({min: 10, max: 300, value: defult[0], beforeDot: false});
    } 
  }

  generateDefaultValues(value: number, columnDecCount: any, frction: boolean = false) {
    const v = value;
    const colsCount = columnDecCount.length;
    let defalt = columnDecCount;

    const fraction = Math.trunc(Math.round(((value%1) * 10)))
    
    value = Math.trunc(value);

    for(let i = colsCount - (frction ? 2 : 1) ; i >= 0 ; --i) {
      const modValue = Math.pow(10, columnDecCount[i]);
      defalt[i] = value % modValue;
      value = Math.trunc(value / modValue);
    }

    if(frction)
      defalt[colsCount - 1] = fraction;

    return defalt;
  }

  openWebForm(link) {
    link += + '&PatientID=' + this.activity.PatientID
            + '&IncidentNumber=' + this.activity.IncidentID;

    if (this.platform.is('ios')) {
      this.iab.create(link, '_blank', 'location=no');
    } else if (this.platform.is('android')) {
      this.iab.create(link, '_blank', 'hideurlbar=yes');
    }
  }
}
