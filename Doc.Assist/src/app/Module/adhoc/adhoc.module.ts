import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdhocPageRoutingModule } from './adhoc-routing.module';

import { AdhocPage } from './adhoc.page';
import { PickerViewComponent, PickerViewModule } from 'src/app/Components/picker-view';
import { CustomComponents } from 'src/app/Components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdhocPageRoutingModule,
    PickerViewModule,
    CustomComponents
  ],
  declarations: [AdhocPage]
})
export class AdhocPageModule {}
