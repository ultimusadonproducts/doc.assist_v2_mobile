import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdhocPage } from './adhoc.page';

const routes: Routes = [
  {
    path: '',
    component: AdhocPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdhocPageRoutingModule {}
