import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AppModule } from 'src/app/app.module';
import { LoadingService } from 'src/app/Services/LoadingService/loading.service';

@Component({
  selector: 'app-task-overview',
  templateUrl: './task-overview.page.html',
  styleUrls: ['./task-overview.page.scss'],
})
export class TaskOverviewPage implements OnInit {
  @Input() mainPatients = null;
  @Input() otherPatients = null;
  @Input() windowHeight = null;
  @Input() value1 = null;
  @Input() value2 = null;
  @Input() title = '';

  showOtherPatinet: boolean = false;
  close = './assets/icon/close.svg';
  constructor(private modelController: ModalController,
              private http: HttpClient,
              private storage: Storage,
              private router: Router,
              private loading: LoadingService) { }

  ngOnInit() {
  }

  dismiss() {
    this.modelController.dismiss();
  }

  navToTaskPage(id) {
    this.loading.presentLoading();
    this.http.get(AppModule.baseURL + `/ActiveTasks/GetActiveTaskById?taskId=${id}`)
    .subscribe((task: any) =>
      {
        if(task) {
          task.DefaultValue01 = this.value1;
          task.DefaultValue02 = this.value2;

          this.goToDocumentTask(task);
          return;
        }
        this.loading.dismissLoading();
        //console.log(task);
        //this.completeRefresher(event);
      }, error => {
        //this.completeRefresher(event);
        this.loading.dismissLoading();
      })
    //this.patientService.navToPatientProfileByPatientId(id);
  }

  goToDocumentTask(task) {
    this.storage.set('selectedTask', task).then(() => {
      this.loading.dismissLoading();
      this.router.navigate(['/document-task']);
      this.dismiss();
    });
  }
}
