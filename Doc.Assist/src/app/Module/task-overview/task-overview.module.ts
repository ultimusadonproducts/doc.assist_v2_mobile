import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TaskOverviewPageRoutingModule } from './task-overview-routing.module';

import { TaskOverviewPage } from './task-overview.page';
import { CustomComponents } from 'src/app/Components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TaskOverviewPageRoutingModule,
    CustomComponents
  ],
  declarations: [TaskOverviewPage]
})
export class TaskOverviewPageModule {}
