import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TaskOverviewPage } from './task-overview.page';

const routes: Routes = [
  {
    path: '',
    component: TaskOverviewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TaskOverviewPageRoutingModule {}
