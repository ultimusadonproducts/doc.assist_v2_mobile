import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  private loading = null;

  constructor(private loadingController: LoadingController,
              private translate: TranslateService) { }

  async presentLoading() {
    if(this.loading) return;

    const loadingMessage = this.translate.instant('COMMON.PLEASE_WAIT');

    this.loading = await this.loadingController.create({
      cssClass: 'loadingContainer',
      message: loadingMessage,
      mode: 'ios',
      spinner: 'circular'
    });
    await this.loading.present();

    //alert('ok');
  }

  async dismissLoading() {
    this.loading.dismiss();

    this.loading = null;
  }

  getLoading() {
    return this.loading;
  }
}
