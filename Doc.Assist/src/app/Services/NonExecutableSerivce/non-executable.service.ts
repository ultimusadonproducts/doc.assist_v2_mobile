import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppModule } from 'src/app/app.module';

@Injectable({
  providedIn: 'root'
})
export class NonExecutableService {
  options: {text: string, value: string, index: number}[] = []

  constructor(private http: HttpClient) { 
    this.getNonExecutableReasons();
  }

  getNonExecutableReasons() {
    this.http.post(AppModule.baseURL + '/NonExecutable/getNonExecutableReasons', {}).subscribe((res: any) => {
      if(!res) return;
      this.options = [];
      for(let i = 0 ; i < res.length; ++i) {
        this.options.push({text: res[i].Reason, value: res[i].Reason, index: i});
      }
    }) 
  }
}
