import { TestBed } from '@angular/core/testing';

import { NonExecutableService } from './non-executable.service';

describe('NonExecutableService', () => {
  let service: NonExecutableService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NonExecutableService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
