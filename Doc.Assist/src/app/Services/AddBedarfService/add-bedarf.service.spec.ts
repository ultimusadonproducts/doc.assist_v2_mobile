import { TestBed } from '@angular/core/testing';

import { AddBedarfService } from './add-bedarf.service';

describe('AddBedarfService', () => {
  let service: AddBedarfService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AddBedarfService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
