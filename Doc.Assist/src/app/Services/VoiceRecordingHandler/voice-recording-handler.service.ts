import { ApplicationRef, EventEmitter, Injectable } from '@angular/core';
import { SpeechRecognition } from '@ionic-native/speech-recognition/ngx';
import { from } from 'rxjs';
import { map, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class VoiceRecordingHandler {
  public onRecordComplete: EventEmitter<string> = new EventEmitter();

  constructor(private speechRecognition: SpeechRecognition,
              private ref: ApplicationRef) { }

  Record() {
    this.speechRecognition.hasPermission()
      .then((hasPermission: boolean) => {
        if(hasPermission){
          this.startRecording();
        } else {
          this.requestRecordPermission();
        }
      })
  }

  requestRecordPermission() {
    this.speechRecognition.requestPermission()
    .then(
      () => this.startRecording(),
      () => {}
    )
  }

  startRecording() {
    const option = {
      language: 'de-DE',
    };
    this.speechRecognition.startListening(option)
    .subscribe(
      (matches: string[]) => 
      {
        this.onRecordComplete.emit(matches[0]);
        //this.ref.tick();
        //this.cd.detectChanges();
      },
      (onerror) => console.log('error:', onerror)
    )
  }
}
