import { ChangeDetectorRef, EventEmitter, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoadingService } from '../LoadingService/loading.service';
import { AlertController, ModalController, NavController, Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { PatientService } from '../PatientService/patient.service';
import { PatientOverviewPage } from 'src/app/Module/patient-overview/patient-overview.page';
import { AppModule } from 'src/app/app.module';
import { TaskOverviewPage } from 'src/app/Module/task-overview/task-overview.page';

export class Command {
  action: string = null;
  value: any = null;
  others: any = null;
  value1: any = null;
  value2: any = null;
  task: any = '';
}

@Injectable({
  providedIn: 'root'
})
export class VoiceCommandHandler {
  checkingTimes: number = 6;
  checkingEveryms: number = 250;
  selectedPatientId = -1;

  showAllPatients = false;

  onSetAction: EventEmitter<{name: string, value: string}> = new EventEmitter();

  constructor(private http: HttpClient,
              private loading: LoadingService,
              private alertCtrl: AlertController,
              private router: Router,
              private patientService: PatientService,
              private modalController: ModalController,
              private platform: Platform) { }

  async classify(message, isHomeScreen: boolean, context: string) {
    await this.loading.presentLoading();
    this.http.post('https://badt-docassist-voice.azurewebsites.net/api/classify', {
        "message": message,
        "context": context,
        "UserID": AppModule.currentUser.ID,
        "RoleID": AppModule.currentUser.Role,
        "patients": this.patientService.getMyPatientsShortList(),
        "API_KEY": "430467213112199115930236015207364867",
        "SelectedStations": AppModule.currentUser.Stations
      }
    ).subscribe(async (res: any) => {
      this.switchActions(res.commands, isHomeScreen);

      await this.loading.dismissLoading();
    }, async (error) => {alert('error'); await this.loading.dismissLoading();});
  }

  private async switchActions(cmds: Command[], isHomeScreen: boolean) {
    //alert(cmds.length)
    for(let i = 0 ; i < cmds.length ;  ++i) {
      switch(cmds[i].action.toLowerCase()) {
        case "start screen":
          await this.handleStartScreenAction(isHomeScreen);
          break;
        case "choose":
          await this.handleChooseAction(cmds[i]);
          break;
        case "click":
          await this.handleClickAction(cmds[i]);
          break;
        case "select":
          await this.handleSelectAction(cmds[i]);
          break;
        case "popup overview":
          await this.handlePopupPatientsAction(cmds[i]);
          break;
        case "popup tasks":
          await this.handlePopupTasksAction(cmds[i]);
          break;
        case "set":
          //await this.handleSelectAction();
        default:
          break;
      }
    }

    //this.popupActions(cmds);
  }

  private handleStartScreenAction(isHomeScreen: boolean) {
    if(isHomeScreen)
      this.handleChooseAction({value:'main-patienten', action: 'choose', others: null, value1: null, value2: null, task: ''})
    else
      this.router.navigate(['/home/main-patienten']);
  }

  private async handleChooseAction(cmd: Command) {
    this.activateClickEvent('tab-button-' + cmd.value.toString().toLowerCase());
  }

  private async handleClickAction(cmd: Command) {
    this.activateClickEvent('button-' + cmd.value.toString().toLowerCase());
  }

  private async handleSelectAction(cmd: Command) {
    this.activateClickEvent(cmd.value.toString().toLowerCase());
  }

  private async handlePopupPatientsAction(cmd: Command) {
    const modal = await this.modalController.create({
      component: PatientOverviewPage,
      mode: 'ios',
      initialBreakpoint: 0.5,
      breakpoints: [0, 0.5],
      swipeToClose: true,
      componentProps: {
        mainPatients: cmd.value,
        otherPatients: cmd.others,
        windowHeight: this.platform.height()
      }
    });
    
    return await modal.present();
  }

  private async handlePopupTasksAction(cmd: Command) {
    const modal = await this.modalController.create({
      component: TaskOverviewPage,
      mode: 'ios',
      initialBreakpoint: 0.5,
      breakpoints: [0, 0.5],
      swipeToClose: true,
      componentProps: {
        mainPatients: cmd.value,
        otherPatients: cmd.others,
        windowHeight: this.platform.height(),
        value1: cmd.value1 === null || cmd.value1 === undefined ? "0" : cmd.value1,
        value2: cmd.value2 === null || cmd.value2 === undefined ? "0" : cmd.value2,
        title: cmd.task
      }
    });
    
    return await modal.present();
  }

  private async activateClickEvent(id: string) {
    for(let i = 0 ; i < this.checkingTimes ; ++i) {
      let element = document.getElementById(id);
      console.log(id)
      if (element && element instanceof HTMLElement) {
        element.click();
        return;
      }
      await this.delay();
    }
  }

  private async popupActions(cmds: Command[]) {
    await this.delay();
    await this.delay();
    const alert = await this.alertCtrl.create({
      header: 'Actions',
      message: this.getCmdsMessage(cmds),
      buttons: ['Ok'],
    });

    await alert.present();
  }

  private getCmdsMessage(cmds: Command[]) {
    let msg = '<ion-label class="d-flex flex-column">';

    for(let i = 0 ; i < cmds.length ; ++i) {
      if(cmds[i].action.toLowerCase() === 'popup patients')
        msg += this.getCmdsMessageForPopupPatients(cmds[i]);
      else
        msg += '<ion-label> ' + (i + 1) + '. ' + cmds[i].action + ' , ' + cmds[i].value + ' </ion-label>'
    }
    msg += '</ion-label>';

    return msg;
  }

  private getCmdsMessageForPopupPatients(cmd: Command) {
    let msg = cmd.action;

    for(let i = 0 ; i < cmd.value.length ; ++i) {
      msg += '<ion-label> P.' + (i + 1) + '- ' + cmd.value[i].id + ' , ' + cmd.value[i].name + ' </ion-label>'
    }
    return msg
  }

  private delay() {
    return new Promise(f => setTimeout(f, this.checkingEveryms));
  }


}
