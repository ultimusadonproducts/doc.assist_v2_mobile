import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppModule } from 'src/app/app.module';
import { take, map, switchMap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  serverTimeNow: Date = null;
  
  overrideOffset = null;
  timeOverrideOffset: Date = null;

  overrideDuration = null;
  timeOverrideDuration: Date = null;

  constructor(private http: HttpClient) { }

  getServerConfigTime() {
    return this.http.get(AppModule.baseURL + '/Config/GetTimeConfig').pipe(
      take(1),
      map((config: any) => {
        this.serverTimeNow = new Date(config.Now);
        this.overrideDuration = config.Duration;
        this.overrideOffset = config.Offset;
        console.log(this.serverTimeNow, config.Now)
        this.timeOverrideOffset = this.addHours(this.serverTimeNow, -this.overrideOffset);
        this.timeOverrideDuration = this.addHours(this.serverTimeNow, this.overrideDuration);
        //console.log(this.timeOverrideOffset, this.timeOverrideDuration)
        return config;
      })
    )
  }

  isNullableAttributes() {
    return this.serverTimeNow === null && this.overrideOffset === null && this.overrideDuration === null;
  }

  private addHours(time: Date, hours) {
    let tempDate = new Date(time);
    tempDate.setTime(tempDate.getTime() + (hours*60*60*1000));
    return tempDate;
  }

  toMinutes(minutes) {
    return (minutes/(60*1000));
  }

}
