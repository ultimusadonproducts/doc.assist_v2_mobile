import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { map, take } from 'rxjs/operators';
import { AppModule } from 'src/app/app.module';

@Injectable({
  providedIn: 'root'
})
export class PatientService {
  myPatients = [];
  allPatients = [];
  freePatients = [];

  constructor(private http: HttpClient,
              private storage: Storage,
              private router: Router) { }

  getMeinePatient(event = null) {
    return this.http.post(AppModule.baseURL + '/NewPatient/GetMyPatients', 
      {
        UserID : AppModule.currentUser.ID,
        Scope : AppModule.ActionOnFeature['Scope'][0],
        SelectedStations: AppModule.currentUser.Stations
      }
    ).pipe(
      take(1),
      map((meinePatient: any) => {
        this.myPatients = meinePatient;
        return meinePatient;
      })
    )
  }

  

  getAllPatients() {
    return this.http.post(AppModule.baseURL + '/NewPatient/GatAllPatients', 
      {
        UserID : AppModule.currentUser.ID,
        Scope : AppModule.ActionOnFeature['Scope'][0]
      }).pipe(
        take(1),
        map((allPatient: any) => {
          this.allPatients = allPatient;
          return this.allPatients;
        })
      )
  }

  getFreePatients() {
    return this.http.post(AppModule.baseURL + '/NewPatient/GetFreePatients', 
      {
        UserID : AppModule.currentUser.ID,
        Scope : AppModule.ActionOnFeature['Scope'][0]
      }).pipe(
        take(1),
        map((freePatients: any) => {
          this.freePatients = freePatients;
          return this.freePatients;
        })
      );
  }

  getMyPatientsShortList() {
    let myPatientsShortList: {name: string, id: number}[] = [];

    for(let i = 0 ; i < this.myPatients.length ; i++) {
      for(let j = 0 ; j < this.myPatients[i].Content.length ; j++) {
        const patient = this.myPatients[i].Content[j].Patient;

        myPatientsShortList.push({name: patient.FirstName + ' ' + patient.LastName, id: patient.ID});
      }
    }

    return myPatientsShortList;
  }

  getMyPatientsShortListExcept(patients: {name: string, id: number}[]) {
    let myPatientsShortList: {name: string, id: number}[] = [];

    for(let i = 0 ; i < this.myPatients.length ; i++) {
      for(let j = 0 ; j < this.myPatients[i].Content.length ; j++) {
        const patient = this.myPatients[i].Content[j].Patient;
        const patientShortItem = {name: patient.FirstName + ' ' + patient.LastName, id: patient.ID};

        let inList: Boolean = false;
        patients.forEach(pat => {
          if(pat.id === patientShortItem.id)
            inList = true;
        })

        if(!inList) {
          myPatientsShortList.push(patientShortItem);
          console.log(patientShortItem);
        }
      }
    }

    return myPatientsShortList;
  }

  searchInMeinePatients(searchQuery) {
    let foundPatients = [];
    if(searchQuery === '' || searchQuery === null || searchQuery === undefined) return foundPatients;

    for(let i = 0 ; i < this.myPatients.length ; ++i) {
      let currentItem = this.myPatients[i];
      let pushRoom = false;

      let tempRoomItem = {RoomName: currentItem.RoomName, Content: []};

      tempRoomItem.Content = this.searchInContent( currentItem.Content, searchQuery);

      if(tempRoomItem.Content.length > 0)
        foundPatients.push(tempRoomItem);

    }
    return foundPatients;
  }

  searchInAllPatients(searchQuery) {
    let foundPatients = [];
    if(searchQuery === '' || searchQuery === null || searchQuery === undefined) return foundPatients;

    foundPatients = this.searchInContent(this.allPatients, searchQuery);
    
    return foundPatients;
  }

  searchInFreePatients(searchQuery) {
    let foundPatients = [];
    if(searchQuery === '' || searchQuery === null || searchQuery === undefined) return foundPatients;

    foundPatients = this.searchInContent(this.allPatients, searchQuery);
    
    return foundPatients;
  }
  
  public async navToPatientProfileByPatient(patient) {
    await this.storage.set('selectedPatient', patient).then(
      () => {
        this.router.navigate(['/patient-profile']);
      });
  }

  public async navToPatientProfileByPatientId(id) {
    const patient = this.getPatientObjById(id);
    await this.storage.set('selectedPatient', patient).then(
      () => {
        this.router.navigate(['/patient-profile']);
      });
  }

  private searchInContent(content, searchQuery) {
    let tempPatientsItem = [];

    for(let j = 0 ; j < content.length ; ++j) {
      let currentContentItem = content[j];
      if(this.matchPatient(currentContentItem.Patient, searchQuery)) {
        tempPatientsItem.push(currentContentItem);
      }
    }

    return tempPatientsItem;
  }


  private matchPatient(patient, searchQuery: string) {
    if((patient.FirstName + ' ' + patient.LastName).toLowerCase().includes(searchQuery.toLowerCase()) || this.matchRoomName(patient.RoomName, searchQuery))
      return true;
    return false;
  }

  private matchRoomName(roomName, searchQuery: string) {
    if(roomName.toLowerCase().includes(searchQuery.toLowerCase()))
      return true;
    return false;
  }

  private getPatientObjById(id) {
    for(let i = 0 ; i < this.myPatients.length ; i++) {
      for(let j = 0 ; j < this.myPatients[i].Content.length ; j++) {
        const patient = this.myPatients[i].Content[j].Patient;

        if(patient.ID.toString() === id.toString()) return patient;
      }
    }
  }
  
}
