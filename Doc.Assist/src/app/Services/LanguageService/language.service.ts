import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { Language } from './language';


const languages: Language[] = [{text: 'English', value: 'en'},
                                {text: 'Deutsch', value: 'de'}]

@Injectable({
  providedIn: 'root'
})
export class LanguageService {
  selected = 'de';
  
  constructor(private translate: TranslateService, private storage: Storage) { this.setInitialAppLanguage(); }

  async setInitialAppLanguage(lang = null) {
    this.translate.setDefaultLang(this.selected);
    if(lang) await this.setLanguage(lang);
  }

  getLanguages() {
    return languages;
  }

  async setLanguage(lang) {
    await this.translate.use(lang);
    this.selected = lang;
    await this.storage.set('LANG_KEY', lang);
  }

  getTranslation(str) {
    return this.translate.get(str);
  }

  getSelectedLanguage() {
    return this.selected;
  }
}
