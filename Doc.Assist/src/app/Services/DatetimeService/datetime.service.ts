import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DatetimeService {
  dateFormat: string = 'dd.MM.yyyy';
  timeFormat: string = 'HH:mm';
  datetimeFormat: string = 'dd.MM.yyyy HH:mm';

  constructor() { 
    this.getDateFormats();
  }

  getDateFormats() {

  }

}
