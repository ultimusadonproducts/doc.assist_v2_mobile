import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { ToastStatus } from 'src/app/Enums/toast-status.enum';



@Injectable({
  providedIn: 'root'
})
export class ToastHandlerService {

  constructor(private toastCtrl: ToastController) { }

  async presentToast(toastStatus: ToastStatus) {
    const toast = await this.toastCtrl.create({
      message: '<ion-icon style="width: 20px; height: 20px;" src="./assets/icon/' 
                + ((toastStatus == ToastStatus.success) ? 'success' :  'fail')+ '.svg"></ion-icon>'

                + ((toastStatus == ToastStatus.success) ? 'Gespeichert' :  'Fehler'),
      duration: 2000,
      mode: 'ios',
      cssClass: 'toast-css-class' + (toastStatus === ToastStatus.success) ? ' toast-success ' : ' toast-fail '
    });
    toast.present();
  }
}

/**'<ion-grid class="ion-no-padding">'
          + '<ion-row>' 
            + '<ion-col class="d-flex flex-column justify-content-center" size="auto">' 
              + '<ion-icon class="normal-icon" src="./assets/icon/' 
              + ((toastStatus == ToastStatus.success) ? 'success' :  'fail') 
              + '.svg"></ion-icon>'
            + '</ion-col>'
            + '<ion-col class="d-flex flex-column justify-content-center ion-no-padding">'
              + '<ion-label>' + ((toastStatus == ToastStatus.success) ? 'Gespeichert' :  'Fehler') + '</ion-label>'
            + '</ion-col>'
          + '</ion-row>'
        + '</ion-grid>', */
