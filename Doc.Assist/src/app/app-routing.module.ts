import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('./Pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./Pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'patient',
    loadChildren: () => import('./Pages/patient/patient.module').then( m => m.PatientPageModule)
  },
  {
    path: 'aufgaben',
    loadChildren: () => import('./Pages/aufgaben/aufgaben.module').then( m => m.AufgabenPageModule)
  },
  {
    path: 'serial-aufgaben',
    loadChildren: () => import('./Pages/serial-aufgaben/serial-aufgaben.module').then( m => m.SerialAufgabenPageModule)
  },
  {
    path: 'einstellung',
    loadChildren: () => import('./Pages/einstellung/einstellung.module').then( m => m.EinstellungPageModule)
  },
  {
    path: 'patient-profile',
    loadChildren: () => import('./Pages/PatientProfile/patient-profile/patient-profile.module').then( m => m.PatientProfilePageModule)
  },
  {
    path: 'add-patient',
    loadChildren: () => import('./Module/add-patient/add-patient.module').then( m => m.AddPatientPageModule)
  },
  {
    path: 'full-patient-profile-info',
    loadChildren: () => import('./Pages/PatientProfile/full-patient-profile-info/full-patient-profile-info.module').then( m => m.FullPatientProfileInfoPageModule)
  },
  {
    path: 'documented-tasks',
    loadChildren: () => import('./Pages/PatientProfile/documented-tasks/documented-tasks.module').then( m => m.DocumentedTasksPageModule)
  },
  {
    path: 'document-task',
    loadChildren: () => import('./Pages/document-task/document-task.module').then( m => m.DocumentTaskPageModule)
  },
  {
    path: 'add-new-task',
    loadChildren: () => import('./Pages/add-new-task/add-new-task.module').then( m => m.AddNewTaskPageModule)
  },
  {
    path: 'add-event',
    loadChildren: () => import('./Module/add-event/add-event.module').then( m => m.AddEventPageModule)
  },
  {
    path: 'arztliche-anordnung',
    loadChildren: () => import('./Module/arztliche-anordnung/arztliche-anordnung.module').then( m => m.ArztlicheAnordnungPageModule)
  },
  {
    path: 'adhoc',
    loadChildren: () => import('./Module/adhoc/adhoc.module').then( m => m.AdhocPageModule)
  },
  {
    path: 'select-stations',
    loadChildren: () => import('./Module/select-stations/select-stations.module').then( m => m.SelectStationsPageModule)
  },
  {
    path: 'patient-overview',
    loadChildren: () => import('./Module/patient-overview/patient-overview.module').then( m => m.PatientOverviewPageModule)
  },
  {
    path: 'task-overview',
    loadChildren: () => import('./Module/task-overview/task-overview.module').then( m => m.TaskOverviewPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules, relativeLinkResolution: 'legacy' })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
