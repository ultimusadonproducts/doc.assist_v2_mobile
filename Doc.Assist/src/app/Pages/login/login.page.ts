import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { App } from '@capacitor/core';
import { AlertController, ModalController } from '@ionic/angular';
import { features } from 'process';
import { SelectStationsPage } from 'src/app/Module/select-stations/select-stations.page';
import { AppModule } from '../../app.module';
import { Storage } from '@ionic/storage';
import { LoadingService } from 'src/app/Services/LoadingService/loading.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  username = '';
  password = '';
  showSelectServer = false;
  serverLink = '';
  domainConfig = AppModule.domainConfig;
  selectedDomain = AppModule.domainConfig.DefaultDomain;

  constructor(private router: Router,
              private alertController: AlertController,
              private http: HttpClient,
              private loadingController: LoadingService,
              private modalController: ModalController,
              private storage: Storage) { }

  ngOnInit() {
    //this.createSelectStationModal();
    //this.presentAlertCheckbox();
  }

  async login() {
    if(this.username == '' || this.password == ''){
      await this.presentAlert('Anmeldung fehlgeschlagen', '', 'Bitte überprüfen Sie Benutzername und Passwort', ['in Ordnung']);
    } else {
      await this.presentLoading();
      await this.storage.set('currentServerLink', AppModule.currentServerLink).then(async () =>
        {
          const loginResponse = await this.callLoginService();
          await this.handleLoginResponse(loginResponse);
        })
    }
  }

  async callLoginService() {
    return await this.http.post(AppModule.baseURL + '/Login/Login',
    {
      Username: this.username,
      Domain: 'UltimusOrganization',//this.selectedDomain,
      Password: this.password
    });
  }

  async handleLoginResponse(userRespone) {
    await userRespone.subscribe(async (user: any) => {
      if(user.UserData == null)
      {
        await this.presentAlert('Anmeldung fehlgeschlagen', '', 'Bitte überprüfen Sie Benutzername und Passwort', ['in Ordnung']);
        await this.loadingController.dismissLoading();
      }
      else{
        AppModule.currentUser = user.UserData;
        AppModule.isDoctor = user.IsDoctor;
        await this.getUserRules();    
      }
    }, error => {
      this.loadingController.dismissLoading();
      this.showSelectServer = true;
      AppModule.handleError(error);
    })
  }

  async getActionOnfeatures(login) {
    await this.http.post(AppModule.baseURL + '/actions/GetAccessRights',
    {
      UserID: AppModule.currentUser.ID,
      RoleID: AppModule.currentUser.Role
    }).subscribe(
      async (actionOnFeatures: any) => {
        if(actionOnFeatures.length == 0) {
          this.presentAlert('Kein Zugriff', '', 'Dem Nutzer wurden keine Funktionen hinzugefügt', ['OK']);
        } else {
          //console.log(actionOnFeatures);
          for(let i = 0 ; i < actionOnFeatures.length ; ++i)
          {
            if(AppModule.ActionOnFeature[actionOnFeatures[i].feature] == undefined || AppModule.ActionOnFeature[actionOnFeatures[i].feature] == null)
              AppModule.ActionOnFeature[actionOnFeatures[i].feature] = [actionOnFeatures[i].action]
            else
              AppModule.ActionOnFeature[actionOnFeatures[i].feature].push(actionOnFeatures[i].action)
          }

          if(login) {
            await this.navigateToHomePage();
          }
        }
      }, error => {
        this.loadingController.dismissLoading();
      }
    )
  }

  async handleActionOnFeatureResponse(respone) {
    await respone
  }

  async getDoctorStations() {
    await this.http.post(AppModule.baseURL + '/NewDoctor/GetDoctorStations', AppModule.currentUser.ID)
    .subscribe(async stats => {
      const stations: any = stats;
      await this.checkStations(stations);
    })
  }

  async getUserRules() {
    await this.http.post(AppModule.baseURL + '/NewDoctor/GetLoggedInUserRoles', AppModule.currentUser.ID)
    .subscribe(async rol => {
      const Roles: any = rol;
      await this.checkRoles(Roles);
    })
  }

  async checkStations(stations) {
    
    if(stations.length > 1) {
      await this.loadingController.dismissLoading();
      await this.presentStationsAlert('Station', true, stations)
    } else {
      this.getActionOnfeatures(true)
      //this.navigateToHomePage();
    }
  }

  async checkRoles(roles) {
    if(roles.length == 0){
      await this.presentAlert('Keine Rollen', '', 'Dem Benutzer sind keine Rollen zugewiesen', ['OK']);
      return;
    }
    else if(roles.length > 1) {
      await this.loadingController.dismissLoading();
      await this.presentStationsAlert('Roles', false, roles)
    } else {
      AppModule.currentUser.Role = roles[0].ID;
      if(AppModule.isDoctor)
        await this.getDoctorStations();
      else {
        await this.getActionOnfeatures(true);
        //await this.navigateToHomePage();
      }
    }
  }

  async presentAlert(header, subHeader, message, buttons) {
    const alert = await this.alertController.create({
      header: header,
      subHeader: subHeader,
      message: message,
      buttons: buttons
    });

    await alert.present();
  }

  async presentLoading() {
    this.loadingController.presentLoading();
  }

  async presentStationsAlert(header, isStationSelection, data) {
    const alert = await this.alertController.create({
      header: header,
      inputs: (isStationSelection ? this.generateStationCheckboxAlertInput(data) :
                                    this.generateRolesCheckboxAlertInput(data)),
      mode: 'ios',
      buttons: [
        {
          text: 'Abbrechen',
          role: 'cancel',
          cssClass: 'secondary',
        }, {
          text: 'Ok',
          handler: async (selectedData) => {
            if(isStationSelection){
              //console.log(selectedData);
              if(selectedData.length == 0){
                this.presentAlert('Keine Station', '','Keine Station ausgewählt', ['Ok']);
                return;
              }
              await this.presentLoading();
              AppModule.currentUser.Stations = selectedData;
              this.getActionOnfeatures(true);
              //await this.navigateToHomePage();
            } else {
              await this.presentLoading();
              AppModule.currentUser.Role = selectedData;
              if(AppModule.isDoctor)
                await this.getDoctorStations();
              else
                await this.getActionOnfeatures(true);

            }
          }
        }
      ]
    });

    await alert.present();
  }

  generateStationCheckboxAlertInput(input) {
    let checkBoxInput = [];
    for(let i = 0; i < input.length ; ++i){
      checkBoxInput.push({
        name: input[i].ShortName,
        type: 'checkbox',
        label: input[i].ShortName,
        value: input[i].ID,
        checked: true
      })
    }
    return checkBoxInput;
  }

  generateRolesCheckboxAlertInput(input) {
    let checkBoxInput = [];
    for(let i = 0; i < input.length ; ++i){
      checkBoxInput.push({
        name: input[i].Name,
        type: 'radio',
        label: input[i].Name,
        value: input[i].ID,
        checked: (i == 0) ? true : false
      })
    }
    return checkBoxInput;
  }

  async navigateToHomePage() {
    if(!AppModule.isDoctor)
      for(let i = 0 ; i < AppModule.currentUser.Stations.length ; ++i)
        AppModule.currentUser.Stations[i] = AppModule.currentUser.Stations[i].ID;

    //await this.getServersLinks();
    this.showSelectServer = false;
    await this.router.navigate(['/home']);
    await this.loadingController.dismissLoading();
  }

  async onServerChange() {
    AppModule.currentServerLink = 'https://' + this.serverLink + '.ultimuscps.com/';
    AppModule.baseURL = AppModule.currentServerLink + AppModule.apiURL
    //this.storage.set('currentServerLink', AppModule.currentServerLink);
  }

}
