import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SerialAufgabenPageRoutingModule } from './serial-aufgaben-routing.module';

import { SerialAufgabenPage } from './serial-aufgaben.page';
import { UserTasksComponent } from 'src/app/Components/user-tasks/user-tasks.component';
import { TaskCardComponent } from 'src/app/Components/task-card/task-card.component';
import { CustomComponents } from 'src/app/Components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SerialAufgabenPageRoutingModule,
    CustomComponents
  ],
  declarations: [SerialAufgabenPage]
})
export class SerialAufgabenPageModule {}
