import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SerialAufgabenPage } from './serial-aufgaben.page';

const routes: Routes = [
  {
    path: '',
    component: SerialAufgabenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SerialAufgabenPageRoutingModule {}
