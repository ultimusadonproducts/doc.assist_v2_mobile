import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppModule } from 'src/app/app.module';
import { Storage } from '@ionic/storage';
import { AufgabenTabService } from 'src/app/Events/AufgabenTab/aufgaben-tab.service';
import { DatePipe } from '@angular/common';
import { SerialTabService } from 'src/app/Events/SerialTab/serial-tab.service';
import { DatetimeService } from 'src/app/Services/DatetimeService/datetime.service';
import { ViewWillEnter, ViewWillLeave } from '@ionic/angular';
import { Screens } from 'src/app/Enums/screens.enum';

@Component({
  selector: 'app-serial-aufgaben',
  templateUrl: './serial-aufgaben.page.html',
  styleUrls: ['./serial-aufgaben.page.scss'],
})
export class SerialAufgabenPage implements OnInit, ViewWillLeave, ViewWillEnter {
  tasks: any = [];
  selectedActivity = [];
  showItems: any[][][];
  searchQuery: string = '';
  downArrow = './assets/icon/arrow/down.svg';
  screensEnum = Screens;

  showRecorder
  constructor(private http: HttpClient,
              private router: Router,
              private storage: Storage,
              private serialTabService: SerialTabService,
              public datetimeFormatService: DatetimeService,
              private ref: ChangeDetectorRef) { 
              }
  
  ionViewWillLeave() {
    this.showRecorder = false;
    this.ref.detectChanges();
  }

  ionViewWillEnter() {
    this.showRecorder = true;
    this.ref.detectChanges();
  }

  async ngOnInit() {
    //this.showItem
    await this.serialTabService.getObservable().subscribe(async (data) => {
      if(data.refresh == true) {
        this.getActiveSerialTasks();
        return;
      }

      if(data.searchQuery !== null && data.searchQuery !== undefined) {
        this.searchQuery = data.searchQuery;
        this.isMatchingSearch();
      }
    })
    this.getActiveSerialTasks();
  }

  getActiveSerialTasks(event = null) {
    this.http.post(AppModule.baseURL + '/ActiveTasks/GetSerialTasks', 
    {
      UserID : AppModule.currentUser.ID,
      Scope : AppModule.ActionOnFeature['Scope'][0],
      SelectedStations: AppModule.currentUser.Stations,
      RoleID: AppModule.currentUser.Role
    }
    ).subscribe(tasks => 
      {
        //this.showData = true;
        this.tasks = tasks;
        this.completeRefresher(event);
        this.isMatchingSearch();
      }, error => {
        this.completeRefresher(event);
      })
  }

  selectActivity(i) {
    this.selectedActivity[i] = (this.selectedActivity[i] === true ) ? false : true;
  }

  goToDocumentTask(task) {
    this.storage.set('selectedTask', task.Task).then(() => {
      this.router.navigate(['/document-task']);
    });
  }

  completeRefresher(event) {
    if(event !== null)
      event.target.complete();
  }

  isMatchingSearch() {
    this.showItems = [];
    for(let i = 0 ; i < this.tasks.length ; ++i) {
      const content = this.tasks[i].Content;
      this.showItems[i] = [];
      for(let j = 0 ; j < content.length ; ++j) {
        //this.showItems[i] = new Array(content.length);
        const activity = content[j];
        const action: boolean[] = this.isMatchingActivity(activity, this.tasks[i].TimeStamp);
        //console.log(i, j, this.showItems[i][j], action);
        this.showItems[i][j] = action;
      }
    }
  }

  isMatchingActivity(activity, time) {
    if(this.searchQuery === '' || this.searchQuery === null || this.searchQuery === undefined) {
      return new Array(activity.Task.length + 1).fill(true);
    }
    
    let serialAufgaben = [false];
    for(let i = 0 ; i < activity.Task.length ; ++i) {
      const act = activity.Task[i];
      const datePipe: DatePipe = new DatePipe('en-US');
      const action = act.RoomName.toLowerCase().includes(this.searchQuery) || 
            datePipe.transform(time, this.datetimeFormatService.timeFormat).toString().toLowerCase().includes(this.searchQuery) ||
            act.Task.ActivityName.toLowerCase().includes(this.searchQuery) ||
            act.PatientName.toLowerCase().includes(this.searchQuery);
            
      serialAufgaben.push(action);

      if(action === true) {
        serialAufgaben[0] = true;
      }
    }
    return serialAufgaben;
  }
}
