import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomePage } from './home.page';

const routes: Routes = [
  {
    path: '',
    component: HomePage,
    children: [
      {
        path: 'main-patienten',
        children: [
          {
            path: '',
            loadChildren: () => import('../patient/patient.module').then(m => m.PatientPageModule)
          }
        ]
      },
      {
        path: 'aufgaben',
        children: [
          {
            path: '',
            loadChildren: () => import('../aufgaben/aufgaben.module').then( m => m.AufgabenPageModule)
          }
        ]
      },
      {
        path: 'serienaufgaben',
        children: [
          {
            path: '',
            loadChildren: () => import('../serial-aufgaben/serial-aufgaben.module').then( m => m.SerialAufgabenPageModule)
          }
        ]
      },
      {
        path: 'einstellung',
        children: [
          {
            path: '',
            loadChildren: () => import('../einstellung/einstellung.module').then( m => m.EinstellungPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/home/main-patienten',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomePageRoutingModule {}
