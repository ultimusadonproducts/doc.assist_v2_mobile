import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { IonButton, ModalController, PopoverController } from '@ionic/angular';
import { features } from 'process';
import { Button } from 'protractor';
import { ActivityMenuComponent } from '../../Components/activity-menu/activity-menu.component';
import { AppModule } from '../../app.module';
import { PatientTabService } from '../../Events/PatientTab/patient-tab.service';
import { AufgabenTabService } from 'src/app/Events/AufgabenTab/aufgaben-tab.service';
import { HomeService } from 'src/app/Events/Home/home.service';
import { SelectStationsPage } from 'src/app/Module/select-stations/select-stations.page';
import { SerialTabService } from 'src/app/Events/SerialTab/serial-tab.service';
import { MainTabs } from 'src/app/Enums/main-tabs.enum';
import { SecondaryTabs } from 'src/app/Enums/secondary-tabs.enum';
import { SearchService } from 'src/app/Services/SearchService/search.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  @ViewChild("activiteBtn", { read: ElementRef }) private activiteBtn: ElementRef;

  mainTabs = MainTabs;
  secondaryTabs = SecondaryTabs;

  activePatient = './assets/icon/Patienten/active.svg'
  inactivePatient = './assets/icon/Patienten/inactive.svg'

  activeAufgaben = './assets/icon/Aufgaben/active.svg'
  inactiveAufgaben = './assets/icon/Aufgaben/inactive.svg'

  activeProfile = './assets/icon/Profile/active.svg'
  inactiveProfile = './assets/icon/Profile/inactive.svg'

  activeSerienaufgabe = './assets/icon/Serienaufgabe/active.svg'
  inactiveSerienaufgabe = './assets/icon/Serienaufgabe/inactive.svg'

  activite = './assets/icon/activite.svg'

  selectedUpperPatientTab = 1;

  CurrentSelectedTab = MainTabs.Patienten;
  ActionOnFeature = AppModule.ActionOnFeature;

  popover: any;
  searchQuery: string = '';

  constructor(public patientEvent: PatientTabService,
              public popoverController: PopoverController,
              private aufgabenEvent: AufgabenTabService,
              private serialEvent: SerialTabService,
              private homeService: HomeService,
              private modalController: ModalController,
              private ref: ChangeDetectorRef,
              public searchService: SearchService) { }

  ngOnInit() {
    this.homeService.getObservable().subscribe(data =>
      {
        if(data.closePopover == true){
          this.popover.dismiss();
        }
      })
  }

  checkFeatureAction(feature, action) {
    return AppModule.checkFeatureAction(feature, action);
  }

  patientTabClicked(tabNumber) {
    this.selectedUpperPatientTab = tabNumber;
    
    this.talkToPatientenTab(tabNumber);
    this.talkToAufgabenTab(tabNumber);
    
  }

  changeCurrentSelectedTab($event) {
    this.searchQuery = '';
    this.searchQueryChanged();

    switch($event.tab) {
      case 'main-patienten':
        this.CurrentSelectedTab = MainTabs.Patienten;
        this.patientTabClicked(this.secondaryTabs.my);
        break;
      case 'aufgaben':
        this.CurrentSelectedTab = MainTabs.MeineAufgaben;
        this.patientTabClicked(this.secondaryTabs.ungrouped);
        break;
      case 'serienaufgaben':
        this.CurrentSelectedTab = MainTabs.SerialAufgaben;
        this.searchQueryChanged();
        break;
      case 'einstellung':
        this.CurrentSelectedTab = MainTabs.Settings;
        break;
    }
    this.ref.detectChanges();
  }

  async presentPopover(ev: any) {
    this.popover = await this.popoverController.create({
      component: ActivityMenuComponent,
      cssClass: 'my-custom-class',
      event: ev,
      mode: 'md',
      translucent: false
    });
    await this.popover.present();
  }

  talkToPatientenTab(tabNumber: number,) {
    this.sendDataToPatientTab({
      tabNumber: tabNumber,
      searchQuery: this.searchQuery.toLowerCase()
    });
  }

  talkToAufgabenTab(tabNumber: number) {
    this.sendDataToAufgabenTab({
      tabNumber: tabNumber,
      searchQuery: this.searchQuery.toLowerCase()
    });
  }

  isDoctorUser() {
    return AppModule.isDoctor;
  }

  searchQueryChanged() {
    switch(this.CurrentSelectedTab) {
      case this.mainTabs.Patienten:
        this.sendDataToPatientTab({searchQuery: this.searchQuery.toLowerCase()});
        break;
      case this.mainTabs.MeineAufgaben:
        this.sendDataToAufgabenTab({searchQuery: this.searchQuery.toLowerCase()});
        break;
      case this.mainTabs.SerialAufgaben:
        this.sendDataToSerialTab({searchQuery: this.searchQuery.toLowerCase()});
      break;
    }
  }

  clearSearchQuery() {
    this.searchQuery = '';
    this.searchService.searchOnFocus = false;
    switch(this.CurrentSelectedTab) {
      case this.mainTabs.Patienten:
        this.sendDataToPatientTab({searchQuery: this.searchQuery.toLowerCase()});
        break;
      case this.mainTabs.MeineAufgaben:
        this.sendDataToAufgabenTab({searchQuery: this.searchQuery.toLowerCase()});
        break;
      case this.mainTabs.SerialAufgaben:
        this.sendDataToSerialTab({searchQuery: this.searchQuery.toLowerCase()});
      break;
    }
  }

  sendDataToAufgabenTab(data = {}) {
    this.aufgabenEvent.publishSomeData(data);
  }

  sendDataToPatientTab(data = {}) {
    this.patientEvent.publishSomeData(data);
  }

  sendDataToSerialTab(data = {}) {
    this.serialEvent.publishSomeData(data);
  }
}
