import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EinstellungPageRoutingModule } from './einstellung-routing.module';

import { EinstellungPage } from './einstellung.page';
import { DropDownComponent } from 'src/app/Components/drop-down/drop-down.component';
import { CustomComponents } from 'src/app/Components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EinstellungPageRoutingModule,
    CustomComponents
  ],
  declarations: [EinstellungPage]
})
export class EinstellungPageModule {}
