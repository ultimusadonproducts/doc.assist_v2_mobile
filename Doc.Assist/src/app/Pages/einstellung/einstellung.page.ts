import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppModule } from 'src/app/app.module';
import { Storage } from '@ionic/storage';
import { waitForAsync } from '@angular/core/testing';
import { AlertController, ViewWillEnter, ViewWillLeave } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Screens } from 'src/app/Enums/screens.enum';

@Component({
  selector: 'app-einstellung',
  templateUrl: './einstellung.page.html',
  styleUrls: ['./einstellung.page.scss'],
})
export class EinstellungPage implements OnInit, ViewWillEnter, ViewWillLeave {
  arrowRight = './assets/icon/arrow/right.svg';
  docAssistLogo = './assets/icon/docAssistLogo/docAssistLogo.png';
  screensEnum = Screens;

  user = AppModule.currentUser;

  showRecorder
  constructor(private router: Router,
              private storage: Storage,
              private alertController: AlertController,
              private http: HttpClient,
              private ref: ChangeDetectorRef) { }

  ngOnInit() {
    this.getServersLinks();
  }

  ionViewWillLeave() {
    this.showRecorder = false;
    this.ref.detectChanges();
  }

  ionViewWillEnter() {
    this.showRecorder = true;
    this.ref.detectChanges();
  }
  /*getAvatar() {
    return .Picture;
  }*/

  logOut() {
    AppModule.currentUser = null;
    AppModule.ActionOnFeature = [];
    this.router.navigateByUrl('/login', { replaceUrl: true }) 
  }

  gerCurrentServerLink() {
    return AppModule.currentServerLink;
  }

  getServersList() {
    let list = [];
    for(let i = 0 ; i < AppModule.serversLinks.length ; ++i) {
      list.push({text: AppModule.serversLinks[i].BaseURL1, value: AppModule.serversLinks[i].BaseURL1, index: i});
    }
    return list;
  }

  async onServerChange(link) {
    const alert = await this.alertController.create({
      header: 'Changing Server!',
      message: 'you are changing the server Link.',
      mode: 'ios',
      buttons: [
        {
          text: 'Abbrechen',
          role: 'cancel',
          cssClass: 'secondary',
        }, {
          text: 'Ok',
          handler: async () => {
            AppModule.currentServerLink = link;
            AppModule.baseURL = AppModule.currentServerLink + AppModule.apiURL
            this.storage.set('currentServerLink', AppModule.currentServerLink);
            this.logOut();
          }
        }
      ]
    });

    await alert.present();
  }

  async getServersLinks() {
    await this.http.post(AppModule.baseURL + '/ServerConfiguration/GetBaseUrls', {})
    .subscribe(async (links: any) => {
      AppModule.serversLinks = links;
    })
  }

}
