import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AufgabenPageRoutingModule } from './aufgaben-routing.module';

import { AufgabenPage } from './aufgaben.page';
import { CustomComponents } from 'src/app/Components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AufgabenPageRoutingModule,
    CustomComponents
  ],
  declarations: [AufgabenPage]
})
export class AufgabenPageModule {}
