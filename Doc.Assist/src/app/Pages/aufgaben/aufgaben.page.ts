import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ViewWillEnter, ViewWillLeave } from '@ionic/angular';
import { AppModule } from 'src/app/app.module';
import { Screens } from 'src/app/Enums/screens.enum';
import { SecondaryTabs } from 'src/app/Enums/secondary-tabs.enum';
import { AufgabenTabService } from 'src/app/Events/AufgabenTab/aufgaben-tab.service';

@Component({
  selector: 'app-aufgaben',
  templateUrl: './aufgaben.page.html',
  styleUrls: ['./aufgaben.page.scss'],
})
export class AufgabenPage implements OnInit, ViewWillEnter, ViewWillLeave {

  secondaryTabs = SecondaryTabs;

  tasksUngrouped:any = [];
  tasksRoom:any = [];
  tasksPatient:any = [];
  tasksLate: any = [];
  searchQuery: string = '';

  currentTab = this.secondaryTabs.ungrouped;

  showRecorder = true;
  screensEnum = Screens;
  
  constructor(private http: HttpClient,
              private aufgabenTabService: AufgabenTabService,
              private ref: ChangeDetectorRef) { 
  }

  ionViewWillLeave() {
    this.showRecorder = false;
    this.ref.detectChanges();
  }

  ionViewWillEnter() {
    this.showRecorder = true;
    this.ref.detectChanges();
  }

  async ngOnInit() {
    await this.aufgabenTabService.getObservable().subscribe(async (data) => {
      if(data.refresh == true) {

        this.getTasksUngrouped();
        this.getTasksRoom();
        this.getTasksPatient();
        this.getLateTasks();
        return;
        
      }

      if(data.searchQuery !== null && data.searchQuery !== undefined) {
        this.searchQuery = data.searchQuery
      }

      if(data.tabNumber !== null && data.tabNumber !== undefined) {
        this.currentTab = data.tabNumber;
        //console.log(this.currentTab)
      }
    })

    this.getTasksUngrouped();
    this.getTasksRoom();
    this.getTasksPatient();
    this.getLateTasks();
  }

  getTasksUngrouped(event = null) {
    this.http.post(AppModule.baseURL + '/ActiveTasks/GetActiveTasksUngrouped', 
    {
      UserID: AppModule.currentUser.ID,
      SelectedStations: AppModule.currentUser.Stations,
      RoleID: AppModule.currentUser.Role
    }
    ).subscribe(tasks =>
      {
        this.tasksUngrouped = tasks;
        this.completeRefresher(event);
      }, error => {
        this.completeRefresher(event);
      })
  }

  getTasksRoom(event = null) {
    this.http.post(AppModule.baseURL + '/ActiveTasks/GetActiveTasksRoom', 
    {
      UserID: AppModule.currentUser.ID,
      SelectedStations: AppModule.currentUser.Stations,
      RoleID: AppModule.currentUser.Role
    }).subscribe(tasks =>
      {
        this.tasksRoom = tasks;
        this.completeRefresher(event);
      }, error => {
        this.completeRefresher(event);
      })
  }
  getTasksPatient(event = null) {
    this.http.post(AppModule.baseURL + '/ActiveTasks/GetActiveTasksPatient', 
    {
      UserID: AppModule.currentUser.ID,
      SelectedStations: AppModule.currentUser.Stations,
      RoleID: AppModule.currentUser.Role
    }).subscribe(tasks =>
      {
        this.tasksPatient = tasks;
        this.completeRefresher(event);
      }, error => {
        this.completeRefresher(event);
      })
  }

  getLateTasks(event = null, getRelatedTasks = null) {
    this.http.post(AppModule.baseURL + '/ActiveTasks/GetLateTasks_Dangerous', 
    {
      UserID: AppModule.currentUser.ID,
      SelectedStations: AppModule.currentUser.Stations,
      RoleID: AppModule.currentUser.Role
    }).subscribe(tasks =>
      {
        this.tasksLate = tasks;
        switch(getRelatedTasks) {
          case 1:
            this.getTasksUngrouped(event);
            break;
          case 2:
            this.getTasksRoom(event);
            break;
          case 3:
            this.getTasksPatient(event);
            break;
        }
      }, error => {
        this.completeRefresher(event);
      })
  }

  completeRefresher(event) {
    if(event !== null)
      event.target.complete();
  }

}