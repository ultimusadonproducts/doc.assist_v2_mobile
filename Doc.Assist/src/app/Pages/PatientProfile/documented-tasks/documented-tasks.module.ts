import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DocumentedTasksPageRoutingModule } from './documented-tasks-routing.module';

import { DocumentedTasksPage } from './documented-tasks.page';
import { CustomComponents } from 'src/app/Components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DocumentedTasksPageRoutingModule,
    CustomComponents
  ],
  declarations: [DocumentedTasksPage]
})
export class DocumentedTasksPageModule {}
