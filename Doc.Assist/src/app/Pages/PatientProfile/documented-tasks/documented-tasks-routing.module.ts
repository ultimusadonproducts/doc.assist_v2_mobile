import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DocumentedTasksPage } from './documented-tasks.page';

const routes: Routes = [
  {
    path: '',
    component: DocumentedTasksPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DocumentedTasksPageRoutingModule {}
