import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Platform } from '@ionic/angular';
import { AppModule } from 'src/app/app.module';
import { ToastStatus } from 'src/app/Enums/toast-status.enum';
import { ToastHandlerService } from 'src/app/Handlers/ToastHandler/toast-handler.service';
import { ConfigService } from 'src/app/Services/ConfigService/config.service';
import { DatetimeService } from 'src/app/Services/DatetimeService/datetime.service';

@Component({
  selector: 'app-documented-tasks',
  templateUrl: './documented-tasks.page.html',
  styleUrls: ['./documented-tasks.page.scss'],
})
export class DocumentedTasksPage implements OnInit {
  selectedUpperPatientTab = 1;
  patientID: number;
  allTasks;
  medicineTasks;

  dateClicked = [];
  constructor(private http: HttpClient,
              private router: ActivatedRoute,
              private platform: Platform,
              private iab: InAppBrowser,
              public datetimeFormatService: DatetimeService,
              public config: ConfigService,
              private toastHandler: ToastHandlerService) { }

  ngOnInit() {
    this.router.params.subscribe(params => 
      {
        this.patientID = params['ID'];
        this.getAllDocumentedTasks();
        this.getMedicineDocumentedTasks();
      })

    this.config.getServerConfigTime().subscribe();
  }


  getAllDocumentedTasks(event = null) {
    this.http.post(AppModule.baseURL + '/ActivitiesDocumented/GetPatientDocumentedActivities', 
    JSON.parse(this.patientID.toString()))
    .subscribe(data => 
      {
        this.allTasks = data;
        //console.log(this.allTasks);
        this.completeRefresher(event);
      }, error => {
        this.completeRefresher(event);
      })
  }

  getMedicineDocumentedTasks(event = null) {
    this.http.post(AppModule.baseURL + '/ActivitiesDocumented/GetPatientDocumentedActivitiesMedical', 
    JSON.parse(this.patientID.toString()))
    .subscribe(data => 
      {
        this.medicineTasks = data;
        this.completeRefresher(event);
      }, error => {
        this.completeRefresher(event);
      })
  }

  taskTabClicked(tabNumber) {
    this.dateClicked = [];
    this.selectedUpperPatientTab = tabNumber;
  }

  isOk(doctype) {
    return (this.getDocType(doctype) === 'OK');
  }

  isFormular(doctype) {
    return (this.getDocType(doctype) === 'Formular');
  }

  isWert(doctype) {
    return (this.getDocType(doctype) === 'Wert')
  }

  isFreiText(doctype) {
    return (this.getDocType(doctype) === 'Freitext');
  }

  isMiniFormular(doctype) {
    return (this.getDocType(doctype) === 'Mini Formular')
  }

  getDocType(doctype) {
    if(doctype !== undefined && doctype !== null) {
      const docType = doctype.replace(/^\s+|\s+$/g, '');
      return docType;
    } return '';
  }

  openForm(link, patientID, incidentID) {
    link += + '&PatientID=' + patientID
            + '&IncidentNumber=' + incidentID;

    if (this.platform.is('ios')) {
      this.iab.create(link, '_blank', 'location=no');
    } else if (this.platform.is('android')) {
      this.iab.create(link, '_blank', 'hideurlbar=yes');
    }
  }

  openRelatedCardsExtension(timeStamp) {
    this.dateClicked[timeStamp] = this.dateClicked[timeStamp] === undefined 
                                  || this.dateClicked[timeStamp] === null 
                                  || this.dateClicked[timeStamp] === false? true : false;
  }

  replaceByLineBreak(text: string) {
    const sr = text.replace(/-\?-/g, '\n');
    return sr;
  }

  completeRefresher(event) {
    if(event !== null)
      event.target.complete();
  }

  overrideTime($event, i, j, taskId) {
    //console.log($event, i, j, taskId);

    this.http.post(AppModule.baseURL + '/ActivitiesDocumented/ChangeTimeOverride', {
      TimeOverrideDateDifferenceInMinutes: $event,
      DocumentedActivityId: taskId
    }).subscribe(
      async (res) => {
        if(res === true) {
          if(this.selectedUpperPatientTab === 1) 
            this.allTasks[i].Activities[j].TimeOverridden = true;
          else if(this.selectedUpperPatientTab === 2)
            this.medicineTasks[i].Activities[j].TimeOverridden = true;

          await this.toastHandler.presentToast(ToastStatus.success);
        } else {
          await this.toastHandler.presentToast(ToastStatus.success);
        }
      }
    )
  }

}
