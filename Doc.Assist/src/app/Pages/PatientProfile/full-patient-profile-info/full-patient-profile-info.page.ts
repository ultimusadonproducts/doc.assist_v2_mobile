import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { DatetimeService } from 'src/app/Services/DatetimeService/datetime.service';

@Component({
  selector: 'app-full-patient-profile-info',
  templateUrl: './full-patient-profile-info.page.html',
  styleUrls: ['./full-patient-profile-info.page.scss'],
})
export class FullPatientProfileInfoPage implements OnInit {
  rightArrow = './assets/icon/arrow/right.svg';
  document = './assets/icon/document.svg';

  patient: any = {};

  constructor(private router: Router,
              private storage: Storage,
              public datetimeFormatService: DatetimeService) { }

  async ngOnInit() {
    await this.storage.get('selectedPatient').then(p => {
      this.patient = p;
    });
  }

  goToDocumentedTasks() {
    this.router.navigate(['/documented-tasks', {
      ID: this.patient.ID
    }]);
  }

}
