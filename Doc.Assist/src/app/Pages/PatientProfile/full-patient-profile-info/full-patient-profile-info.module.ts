import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FullPatientProfileInfoPageRoutingModule } from './full-patient-profile-info-routing.module';

import { FullPatientProfileInfoPage } from './full-patient-profile-info.page';
import { TitleBarComponent } from 'src/app/Components/title-bar/title-bar.component';
import { CustomComponents } from 'src/app/Components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FullPatientProfileInfoPageRoutingModule,
    CustomComponents
  ],
  declarations: [FullPatientProfileInfoPage]
})
export class FullPatientProfileInfoPageModule {}
