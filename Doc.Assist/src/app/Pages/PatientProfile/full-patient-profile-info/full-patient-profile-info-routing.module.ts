import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FullPatientProfileInfoPage } from './full-patient-profile-info.page';

const routes: Routes = [
  {
    path: '',
    component: FullPatientProfileInfoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FullPatientProfileInfoPageRoutingModule {}
