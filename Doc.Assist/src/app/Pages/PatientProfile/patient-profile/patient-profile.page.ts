import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Chart } from 'chart.js';

import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { AppModule } from 'src/app/app.module';
import { VitalWerteValues } from 'src/app/Enums/vital-werte-values.enum';
import { PatientProfileService } from 'src/app/Events/PatientProfile/patient-profile.service';
import { DatetimeService } from 'src/app/Services/DatetimeService/datetime.service';

@Component({
  selector: 'app-patient-profile',
  templateUrl: './patient-profile.page.html',
  styleUrls: ['./patient-profile.page.scss'],
})
export class PatientProfilePage implements OnInit, AfterViewInit {
  @ViewChild('chart') chart;
  lineChart;

  mehrArrow = './assets/icon/arrow/small(mehr_).svg'
  addIcon = './assets/icon/add.svg'
  selectedTab = 0;
  patient: any = {};
  vitalWerte = [[], [], [], [], [], [], [], []]
  medicalTasks: any = [];
  nonMedicalTasks: any = [];
  minDate;
  maxDate;

  constructor(private router:Router, 
              private reciveRouter: ActivatedRoute,
              private storage: Storage,
              private http: HttpClient,
              private patientProfileEvent: PatientProfileService,
              public datetimeFormatService: DatetimeService) { }

  async ngOnInit() {
    await this.patientProfileEvent.getObservable().subscribe(async (data) => {
      if(data.refresh == true) {
        this.getPatientVitalWerte();
        this.getPaitentActiveNonMedicalTasks();
        this.getPaitentActiveMedicalTasks();
        return;
      }
    });
  }

  async ngAfterViewInit() {
    await this.storage.get('selectedPatient').then(p => {
      this.patient = p;
      this.getPatientVitalWerte();
      this.getPaitentActiveNonMedicalTasks();
      this.getPaitentActiveMedicalTasks();
    });
  }

  changeTab(tabId) {
    this.selectedTab = tabId;
  }

  goToFullInfoProfile() {
    this.router.navigate(['/full-patient-profile-info']);
  }

  getPaitentActiveNonMedicalTasks(event = null) {
    this.http.post(AppModule.baseURL + '/ActiveTasks/GetPatientActiveNonMedicalTasksByID', 
    {
      UserID: AppModule.currentUser.ID,
      PatientID: this.patient.ID,
      RoleID: AppModule.currentUser.Role
    })
    .subscribe(task => {
      this.nonMedicalTasks = task;
      this.completeRefresher(event);
    }, error => {
      this.completeRefresher(event);
    });
  }

  getPaitentActiveMedicalTasks(event = null) {
    /*console.log({
      UserID: AppModule.currentUser.ID,
      PatientID: this.patient.ID,
      RoleID: AppModule.currentUser.Role
    })*/
    this.http.post(AppModule.baseURL + '/ActiveTasks/GetPatientActiveMedicalTasksByID', 
    {
      UserID: AppModule.currentUser.ID,
      PatientID: this.patient.ID,
      RoleID: AppModule.currentUser.Role
    }
    )
    .subscribe(task => {
      this.medicalTasks = task;
      this.completeRefresher(event);
    }, error => {
      this.completeRefresher(event);
    });
  }

  getPatientVitalWerte(event = null) {
    this.http.post(AppModule.baseURL + '/ActivitiesDocumented/GetPatientVitalwerte', this.patient.ID) //
    .subscribe( ((v: any) => 
      {        
        const vital: any = v.PatientTasks;
        let i = 0;
        this.vitalWerte = [[], [], [], [], [], [], [], []];
        if(vital.length === 0) {
          this.vitalWerte[VitalWerteValues.Date].push(new Date(v.CurrentDate));
          this.minDate = this.maxDate = v.CurrentDate;
        }
        else {
          this.minDate = this.maxDate = vital[0].Date;
          vital.forEach(vitalDate => {
            if(vitalDate.Date < this.minDate) this.minDate = vitalDate.Date;
            if(vitalDate.Date > this.maxDate) this.maxDate = vitalDate.Date;
            this.vitalWerte[VitalWerteValues.Blutdruck_SYS].push(
              { 
                y: vitalDate.Values[VitalWerteValues.Blutdruck_SYS - 1],
                t: new Date(vitalDate.Date)
              });
            this.vitalWerte[VitalWerteValues.Blutdruck_DIA].push(
              {
                y: vitalDate.Values[VitalWerteValues.Blutdruck_DIA - 1],
                t: new Date(vitalDate.Date)
              });
            this.vitalWerte[VitalWerteValues.Heartrate].push(
              {
                y: vitalDate.Values[VitalWerteValues.Heartrate - 1],
                t: new Date(vitalDate.Date)
              });
            this.vitalWerte[VitalWerteValues.Temperatur].push(
              {
                y: vitalDate.Values[VitalWerteValues.Temperatur - 1],
                t: new Date(vitalDate.Date)
              });
            this.vitalWerte[VitalWerteValues.Pain_NoMotion].push(
              {
                y: vitalDate.Values[VitalWerteValues.Pain_NoMotion - 1],
                t: new Date(vitalDate.Date)
              });
            this.vitalWerte[VitalWerteValues.Pain_Motion].push(
              {
                y: vitalDate.Values[VitalWerteValues.Pain_Motion - 1],
                t: new Date(vitalDate.Date)
              });
            this.vitalWerte[VitalWerteValues.Date].push(new Date(vitalDate.Date));
          })
        }
        this.completeRefresher(event);
        this.createChart(this.chart, [false, false, false, false, false, false]);
      })
      , error => {this.completeRefresher(event);})
  }

  createChart(chart, legendHidden) {
    const NumberOfLegends = 6;
    const line = new Chart(chart.nativeElement, {
      type: 'line',
      data: {
        labels: this.vitalWerte[VitalWerteValues.Date],
        datasets: [{
          label: 'SYS',
          data: this.vitalWerte[VitalWerteValues.Blutdruck_SYS],
          backgroundColor: 'rgba(0,255,0)',
          borderColor: 'rgb(0, 255, 0)',
          borderWidth: 1,
          fill: false,
          lineTension: 0,
          meta: {
            hidden: legendHidden[0]
          }
        },
        {
          label: 'DIA​',
          data: this.vitalWerte[VitalWerteValues.Blutdruck_DIA],
          backgroundColor: 'rgba(0,0,255)',
          borderColor: 'rgb(0, 0, 255)',
          borderWidth: 1,
          fill: false,
          lineTension: 0,
          meta: {
            hidden: legendHidden[1]
          }
        },
        {
          label: 'HF',
          data: this.vitalWerte[VitalWerteValues.Heartrate],
          backgroundColor: 'rgba(255,0,0)',
          borderColor: 'rgb(255, 0, 0)',
          borderWidth: 1,
          fill: false,
          lineTension: 0,
          meta: {
            hidden: legendHidden[2]
          }
        },
        {
          label: 'Temp',
          data: this.vitalWerte[VitalWerteValues.Temperatur],
          backgroundColor: 'rgba(255,0,255)',
          borderColor: 'rgb(255, 0, 255)',
          borderWidth: 1,
          fill: false,
          lineTension: 0,
          meta: {
            hidden: legendHidden[3]
          }
        },
        {
          label: 'SiR​',
          data: this.vitalWerte[VitalWerteValues.Pain_NoMotion],
          backgroundColor: 'rgba(255,255,0)',
          borderColor: 'rgb(255, 255, 0)',
          borderWidth: 1,
          fill: false,
          lineTension: 0,
          meta: {
            hidden: legendHidden[4]
          }
        },
        {
          label: 'SiB​',
          data: this.vitalWerte[VitalWerteValues.Pain_Motion],
          backgroundColor: 'rgba(0, 255, 255)',
          borderColor: 'rgb(0, 255, 255)',
          borderWidth: 1,
          fill: false,
          lineTension: 0,
          meta: {
            hidden: legendHidden[5]
          }
        },
      ]
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        scaleShowLabels : false,
        scales: {
          yAxes: [{
            stacked: true,
            ticks: {
              beginAtZero: true,
            },
          }],
          xAxes: [{
            type: 'time',
            ticks: {
              autoSkip: false,
              maxRotation: 0,
              maxTicksLimit: this.vitalWerte[VitalWerteValues.Date].length, //Number of Columns per chart
              //count: this.vitalWerte[VitalWerteValues.Date].length, //Number of Columns per chart
              //display: ReturnCondition === 'AllVitalData' ? false : true,.
              //stepSize: 0.1,
              callback: function(label, index, labels) {
                if (labels.length !== 0) {
                  return '';//(index === 0 || index === labels.length - 1) ? label.substring(6) + label.substring(0, 5) : '';
                } else {
                  return label;
                }
              },

              //afterBuildTicks: (scale, yTicks) => { return Dates },
            },
            time: {
              unit: 'second',
              unitStepSize: 600,
              //stepSize: 1,
              displayFormats: {
                millisecond: 'HH.mm DD.MM.YYYY ', //HH:mm 
                second: 'HH.mm DD.MM.YYYY ',
                minute: 'HH.mm DD.MM.YYYY ',
                hour: 'HH.mm DD.MM.YYYY ',
                day: 'HH.mm DD.MM.YYYY ',
                week: 'HH.mm DD.MM.YYYY ',
                month: 'HH.mm DD.MM.YYYY ',
                quarter: 'HH.mm DD.MM.YYYY ',
                year: 'HH.mm DD.MM.YYYY ',
             },
             //unit: 'month'
            //unit: ReturnCondition === 'Last24Hours' ? 'minute' : 'll',
            }
          }]
        },
        spanGaps : true,
        legend: {
          display: false,//ReturnCondition === 'Last24Hours' ? true : false,
          labels: {
            boxWidth: 15,
          }
        },
        legendCallback: (chart) => {
          const text = [];
          text.push('<ion-grid class="' + chart.id + '-legend max-form-width"' + '>');
          text.push('<ion-row class="' + chart.id + '-legend d-flex justify-content-around flex-center-center"' + '>');
          
          for (let i = 0 ; i < NumberOfLegends ; ++i) {
            text.push('<ion-col ' + 'class=" d-flex custom-h5' + ((legendHidden[i] === true) ? 'hidden' : '') + '"' + 'size="auto"' + '>'
            + '<div style="height: 15px; width: 15px;'
            + 'background-color:' + chart.data.datasets[i].borderColor + ';"></div>'
            + '<ion-label class="pl-1">' + chart.data.datasets[i].label + '</ion-label> </ion-col>');
          }
          text.push('</ion-row>');
          text.push('</ion-grid>');

          return text.join("");
        },
      },
    });

    for (let i = 0 ; i < NumberOfLegends ; ++i) {
      line.getDatasetMeta(i).hidden = legendHidden[i];
    }

    var myLegendContainer = document.getElementById("legend");
    myLegendContainer.innerHTML = line.generateLegend();

    var legendItems = myLegendContainer.getElementsByTagName('ion-col');
    for (var i = 0; i < legendItems.length; ++i) {
      legendItems[i].addEventListener("click", this.legendClickCallback, false);
    }
  
    return line;
  }

  legendClickCallback(event) {
    event = event || window.event;

    var target = event.target || event.srcElement;
    while (target.nodeName !== 'ION-COL') {
      target = target.parentElement;
    }
    var parent = target.parentElement;
    var chartId = parseInt(parent.classList[0].split("-")[0], 10);
    var chart = Chart.instances[chartId];
    var index = Array.prototype.slice.call(parent.children).indexOf(target);
    var meta = chart.getDatasetMeta(index);

    if (meta.hidden === null || meta.hidden === false) {
      meta.hidden = true;
      target.classList.add('hidden');
    } else {
      target.classList.remove('hidden');
      meta.hidden = null;
    }
    chart.update();
  }
  checkUserActionOnFeature(feature, action) {
    return AppModule.checkFeatureAction(feature, action);
  }

  goToEditTask(task: any, medical, edit) {
    this.storage.set('selectedTask', task).then(() => {
      this.router.navigate(['/add-new-task', {
        medical: medical,
        edit: edit
      }]);
    });
  }

  goToAddNewTask(medical, edit) {
    this.router.navigate(['/add-new-task', {
      medical: medical,
      edit: edit,
      patientID: this.patient.ID
    }]);
  }

  getActivityName(task) {
    return task.NameOption01 == null || task.NameOption01.length < 2? task.ActivityName : task.NameOption01 
  }

  isDoctorUser() {
    return AppModule.isDoctor === true;
  }

  completeRefresher(event) {
    if(event !== null)
      event.target.complete();
  }
}
