import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PatientProfilePageRoutingModule } from './patient-profile-routing.module';

import { PatientProfilePage } from './patient-profile.page';
import { TitleBarComponent } from 'src/app/Components/title-bar/title-bar.component';
import { CustomComponents } from 'src/app/Components/components.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PatientProfilePageRoutingModule,
    CustomComponents
  ],
  declarations: [PatientProfilePage]
})
export class PatientProfilePageModule {}
