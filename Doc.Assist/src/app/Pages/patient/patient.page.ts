import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, NgModule, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { createGesture, Gesture } from '@ionic/core';
import { PatientTabService } from '../../Events/PatientTab/patient-tab.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertController, IonCard, ModalController, ViewWillEnter, ViewWillLeave } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { Router } from '@angular/router';
import { AddPatientPage } from '../../Module/add-patient/add-patient.page';
import { AppModule } from '../../app.module';
import { AddPatientModuleService } from '../../Events/AddPatient/add-patient-module.service';
import { AufgabenTabService } from 'src/app/Events/AufgabenTab/aufgaben-tab.service';
import { SecondaryTabs } from 'src/app/Enums/secondary-tabs.enum';
import { Screens } from 'src/app/Enums/screens.enum';
import { SearchService } from 'src/app/Services/SearchService/search.service';
import { PatientService } from 'src/app/Services/PatientService/patient.service';

@Component({
  selector: 'app-patient',
  templateUrl: './patient.page.html',
  styleUrls: ['./patient.page.scss'],
  
})
export class PatientPage implements OnInit, AfterViewInit, ViewWillLeave, ViewWillEnter {
  @ViewChildren('meineCard', {read: ElementRef}) meineCard: QueryList<ElementRef>;
  
  secondaryTabs = SecondaryTabs

  arrowDown = './assets/icon/arrow/down.svg';
  emptyIcon = './assets/icon/empty.svg';

  meineCardArray: any;
  showInnerBody = [];

  selectedCardIndex = {index: -1}
  
  //meinePatient = [];
  //allePatient = [];
  //freiePatient = [];
  //showItems = [[]];
  
  searchQuery = '';
  currentTab = this.secondaryTabs.my;
  
  showRecorder = true;
  screensEnum = Screens;

  constructor(public patientEvent: PatientTabService,
              private http: HttpClient,
              public alertController: AlertController,
              private router: Router,
              public modalController: ModalController,
              private addPatientModule: AddPatientModuleService,
              private aufgabenTabService: AufgabenTabService,
              private ref: ChangeDetectorRef,
              public searchService: SearchService,
              public patientService: PatientService) { }

  ionViewWillLeave() {
    this.showRecorder = false;
    this.ref.detectChanges();
  }

  ionViewWillEnter() {
    this.showRecorder = true;
    this.ref.detectChanges();
  }
  
  async ngOnInit() {
    await this.patientEvent.getObservable().subscribe(async (data) => {
      if(data.refresh == true) {
        await this.getPageData();
        this.addPatientModule.publishSomeData({patientRefreshed: true})
        return;
      }

      if(data.tabNumber !== null && data.tabNumber !== undefined) {
        this.currentTab = data.tabNumber;
      }

      if(data.searchQuery !== null && data.searchQuery !== undefined) {
        this.searchQuery = data.searchQuery;
        //this.isMatchingSearch();
      }

      //this.applySwapGesture();
    })
    
    await this.getPageData()
    //this.getPatientRoom();
  }

  ngAfterViewInit() {
  }

  async getPageData() {
    await this.getMeinePatient();
    if(!this.isDoctorUser()) {
      await this.getAllePatient();
      await this.getfreiePatient();
    }
  }

  getMeinePatient(event = null) {
    this.patientService.getMeinePatient().subscribe(() => this.completeRefresher(event), () => this.completeRefresher(event));
    /*
    await this.http.post(AppModule.baseURL + '/NewPatient/GetMyPatients', 
      {
        UserID : AppModule.currentUser.ID,
        Scope : AppModule.ActionOnFeature['Scope'][0],
        SelectedStations: AppModule.currentUser.Stations
      }
    )
    .subscribe(async (roomsPatients: any[]) => {
        this.meinePatient = roomsPatients;
        this.isMatchingSearch();
        this.completeRefresher(event);
      }, error => {
        this.completeRefresher(event);
      }
    ); */
  }

  async getAllePatient(event = null) {
    this.patientService.getAllPatients().subscribe(() => this.completeRefresher(event), () => this.completeRefresher(event));
    /*await this.http.post(AppModule.baseURL + '/NewPatient/GatAllPatients', 
      {
        UserID : AppModule.currentUser.ID,
        Scope : AppModule.ActionOnFeature['Scope'][0]
      })
    .subscribe(async (data: any[]) => {
        this.allePatient = data;
        this.isMatchingSearch();
        this.completeRefresher(event);
      }, error => {
        this.completeRefresher(event);
      }
    ); */
  }

  async getfreiePatient(event = null) {
    this.patientService.getFreePatients().subscribe(() => this.completeRefresher(event), () => this.completeRefresher(event));
    /*await this.http.post(AppModule.baseURL + '/NewPatient/GetFreePatients', 
      {
        UserID : AppModule.currentUser.ID,
        Scope : AppModule.ActionOnFeature['Scope'][0]
      })
    .subscribe(async (data: any[]) => {
        this.freiePatient = data;
        this.isMatchingSearch();
        this.completeRefresher(event);
      }, error => {
        this.completeRefresher(event);
      }
    );  */
  }

  async deleteMe(index) {
    await this.getPageData()
    this.aufgabenTabService.publishSomeData({
      refresh: true
    });
  }

  async createAddPatientsModel() {
    const modal = await this.modalController.create({
      component: AddPatientPage,
      swipeToClose: true,
      mode: 'ios',
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

  showRoomPatients(roomId) {
    this.showInnerBody[roomId] = (this.showInnerBody[roomId] === undefined ||
                                      this.showInnerBody[roomId] === null ||
                                      this.showInnerBody[roomId] === false) ? true : false;
  }

  checkAriaExpand(id) {
    return document.getElementById(id).getAttribute('aria-expanded') === 'true' ? true : false;
  }

  navigateToPatinetProfile(index: number) {
  
  }

  canAssignUnAssignPatients() {
    return AppModule.checkFeatureAction('PatientChoice', 'Manual');
  }

  
  isDoctorUser() {
    return AppModule.isDoctor;
  }

  completeRefresher(event) {
    if(event !== null)
      event.target.complete();
  }

  /*isMatchingSearch() {
    const patients =  (this.currentTab == this.secondaryTabs.my) ? this.patientService.myPatient:
                        (this.currentTab == this.secondaryTabs.all) ? this.patientService.allPatient:
                        this.freiePatient;
    this.showItems = [];
    for(let i = 0 ; i < patients.length ; ++i) {
      this.showItems[i] = [];
      if(this.currentTab === this.secondaryTabs.my) {
        const action = this.isMatchingMeine(patients[i].Content, patients[i].RoomName);
        this.showItems[i] = action;
      } else if (this.currentTab === this.secondaryTabs.all) {
        const action = this.isMatchingAlle(patients[i].Patient, patients[i].RoomName);
        this.showItems[i] = action;
      } else {
        const action = this.isMatchingFreie(patients[i].Patient, patients[i].RoomName);
        this.showItems[i] = action;
      }
    }
  }

  isMatchingMeine(content, roomName) {
    if(this.searchQuery === '' || this.searchQuery === null || this.searchQuery === undefined) {
      return new Array(content.length + 1).fill(true);
    }

    let roomPatient = [false];
    for(let i = 0 ; i < content.length ; ++i) {
      const patient = content[i].Patient;
      const action = roomName.toLowerCase().includes(this.searchQuery) || 
                      (patient.FirstName + ' ' + patient.LastName).toLowerCase().includes(this.searchQuery);
            
      roomPatient.push(action);

      if(action === true) {
        roomPatient[0] = true;
      }
    }
    return roomPatient;
  }

  isMatchingAlle(patient, roomName) {
    if(this.searchQuery === '' || this.searchQuery === null || this.searchQuery === undefined) {
      return true;
    }
    const action = roomName.toLowerCase().includes(this.searchQuery) || 
                      (patient.FirstName + ' ' + patient.LastName).toLowerCase().includes(this.searchQuery);
    
    return action;
  }

  isMatchingFreie(patient, roomName) {
    if(this.searchQuery === '' || this.searchQuery === null || this.searchQuery === undefined) {
      return true;
    }
    const action = roomName.toLowerCase().includes(this.searchQuery) || 
                      (patient.FirstName + ' ' + patient.LastName).toLowerCase().includes(this.searchQuery);
    
    return action;
  }*/
}
