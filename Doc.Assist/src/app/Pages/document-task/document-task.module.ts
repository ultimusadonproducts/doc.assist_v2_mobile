import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DocumentTaskPageRoutingModule } from './document-task-routing.module';

import { DocumentTaskPage } from './document-task.page';
import { PickerViewModule } from 'src/app/Components/picker-view';
import { CustomComponents } from 'src/app/Components/components.module';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DocumentTaskPageRoutingModule,
    PickerViewModule,
    CustomComponents,
    TranslateModule,
    
  ],
  declarations: [DocumentTaskPage]
})
export class DocumentTaskPageModule {}
