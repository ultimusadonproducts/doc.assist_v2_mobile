import { HttpClient } from '@angular/common/http';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { App } from '@capacitor/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { WheelSelector } from '@ionic-native/wheel-selector/ngx';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { pickerController } from '@ionic/core';
import { Storage } from '@ionic/storage';
import { AppModule } from 'src/app/app.module';
import { VitalColumns } from 'src/app/Enums/vital-columns.enum';
import { AufgabenTabService } from 'src/app/Events/AufgabenTab/aufgaben-tab.service';
import { SpeechRecognition } from '@ionic-native/speech-recognition/ngx';
import { NonExecutableService } from 'src/app/Services/NonExecutableSerivce/non-executable.service';
import { ToastHandlerService } from 'src/app/Handlers/ToastHandler/toast-handler.service';
import { ToastStatus } from 'src/app/Enums/toast-status.enum';

@Component({
  selector: 'app-document-task',
  templateUrl: './document-task.page.html',
  styleUrls: ['./document-task.page.scss'],
})
export class DocumentTaskPage implements OnInit {
  @ViewChild('picker') calenderPicker;

  activity: any = [];

  medical = false;
  morning = false;
  afternone = false;
  evening = false;

  value01: any = 0;
  value02: any = 0;

  everyXhrs: any = 0;
  numberOfDays: any = 0;
  numberPerDays: any = 0;

  remarks: string = '';
  freeText: string = '';

  morningCheckProp: any = {label: 'Morgen', status: false}
  afternoneCheckProp: any = {label: 'Mittag', status: false}
  eveningCheckProp: any = {label: 'Abend', status: false}


  checkBoxOn = './assets/icon/checkbox/on.svg';
  checkBoxOff = './assets/icon/checkbox/off.svg';

  voiceRecorderInactive = './assets/icon/VoiceRecorder/Voice_inactive.png';
  voiceRecorderActive = './assets/icon/VoiceRecorder/Voice_active.png';

  taskColumns = {
    colCount: 6,
    colName: ['1', '2', '3', '4', '.', '.1'],
    colMin: [0, 0, 0, 0, 0, 0],
    colMax: [9, 9, 9, 9, 9, 9],
    defaultValues: [0, 0, 0, 0, 0, 0],
    colIncreament: [1, 1, 1, 1, 1, 1],
    colDotPostion: 5,
  }

  taskColumns01 = {
    unit: true,
    unitStr: "B",
    wheels: [
    ],
  };

  taskColumns02 = {
    unit: true,
    unitStr: "",
    wheels: [
    ],
  };

  numberPerDayDDList = [{text: 'Optional', value: '-1', index: 0},
                      {text: '1', value: '1', index: 1},
                      {text: '2', value: '2', index: 2},
                      {text: '3', value: '3', index: 3},]
  
  everyXHoursDDList = [ {text: 'Optional', value: '-1', index: 0},
                        {text: '1 Stunde', value: '1', index: 1},
                        {text: '2 Stunden', value: '2', index: 2},
                        {text: '3 Stunden', value: '3', index: 3},
                        {text: '4 Stunden', value: '4', index: 4},
                        {text: '5 Stunden', value: '5', index: 5},
                        {text: '6 Stunden', value: '6', index: 6},
                        {text: '7 Stunden', value: '7', index: 7},
                        {text: '8 Stunden', value: '8', index: 8},
                        {text: '9 Stunden', value: '9', index: 9},
                        {text: '10 Stunden', value: '10', index: 10},
                        {text: '11 Stunden', value: '11', index: 11},
                        {text: '12 Stunden', value: '12', index: 12},
                      ]

  //currentInput01Value;
  //currentInput02Value;

  nonExecutable: boolean = false;
  nonExecutableReason: string = '';
  reasonSelected: boolean = false;
  
  timeOverride = false;
  TimeOverrideDateDifferenceInMinutes = 0.0;
  showCalendder = false;

  nonExecutableReasons: [{text: string, value: string, index: number}] = [{text: 'Patient:in nicht im Zimmer', value: 'Patient:in nicht im Zimmer', index: 0}]

  constructor(private storage: Storage, 
              private selector: WheelSelector,
              private http: HttpClient,
              private aufgabenTabService: AufgabenTabService,
              private navCtrl: NavController,
              private toastCtrl: ToastController,
              private platform: Platform,
              private iab: InAppBrowser,
              private speechRecognition: SpeechRecognition,
              private cd: ChangeDetectorRef,
              public nonExecutableService: NonExecutableService,
              private toastHandler: ToastHandlerService) { }

  async ngOnInit() {
    //this.generateDefaultValues(1510.5, [1, 1, 2, 1], true);
    await this.storage.get('selectedTask').then((act) =>
    {
      this.activity = act;
      console.log(this.activity);
      this.handleWheelColumns();

      this.morning = this.activity.EveryMorning;
      this.afternone = this.activity.EveryNoon;
      this.evening = this.activity.EveryEvening;

      this.everyXhrs = this.activity.EveryXhrs;

      this.numberOfDays = this.activity.NumberOfDays;
      this.numberPerDays = this.activity.NumberPerDay;
    })
    this.nonExecutableService.getNonExecutableReasons();
  }

  handleWheelColumns() {
    this.taskColumns01.unitStr = this.activity.Unit;
    this.taskColumns02.unitStr = this.activity.Unit;
    if(this.isTempActivity()) {
      this.value01 = 35.0; //
      const defult = this.generateDefaultValues(parseFloat(this.activity.DefaultValue01.toString()), [2, 1], true);
      this.taskColumns01.wheels.push({min: 35, max: 44, value: defult[0], beforeDot: false});
      this.taskColumns01.wheels.push({min: 0, max: 9, value: defult[1], beforeDot: true});
    } else if(this.activity.RefVitalTableColumn01 === VitalColumns.Heartrate) {
      this.value01 = 30;
      const defult = this.generateDefaultValues(parseFloat(this.activity.DefaultValue01.toString()), [3], false);
      this.taskColumns01.wheels.push({min: 30, max: 250, value: defult[0], beforeDot: false});
      this.taskColumns01.unit = false;
    } else if (this.activity.RefVitalTableColumn01 === VitalColumns.Pain_Motion ||
      this.activity.RefVitalTableColumn02 === VitalColumns.Pain_NoMotion) {
      this.value01 = 0;
      this.value02 = 0;
      const defult01 = this.generateDefaultValues(parseFloat(this.activity.DefaultValue01.toString()), [2], false);
      const defult02 = this.generateDefaultValues(parseFloat(this.activity.DefaultValue02.toString()), [2], false);
      this.taskColumns01.wheels.push({min: 0, max: 10, value: defult01[0], beforeDot: false});
      this.taskColumns02.wheels.push({min: 0, max: 10, value: defult02[0], beforeDot: false});
      this.taskColumns01.unit = false;
      this.taskColumns02.unit = false;
    } else if (this.activity.RefVitalTableColumn01 === VitalColumns.Blutdruck_SYS ||
      this.activity.RefVitalTableColumn02 === VitalColumns.Blutdruck_DIA) {
      this.value01 = 50.0;      
      this.value02 = 50.0;
      const defult01 = this.generateDefaultValues(parseFloat(this.activity.DefaultValue01.toString()), [3], false);
      const defult02 = this.generateDefaultValues(parseFloat(this.activity.DefaultValue02.toString()), [3], false);
      this.taskColumns01.wheels.push({min: 50, max: 250, value: defult01[0], beforeDot: false});
      this.taskColumns02.wheels.push({min: 50, max: 250, value: defult02[0], beforeDot: false});
    } else if(this.activity.ActivityID === 122) {
      this.value01 = 10;      
      const defult = this.generateDefaultValues(parseFloat(this.activity.DefaultValue01.toString()), [3], false);
      this.taskColumns01.wheels.push({min: 10, max: 250, value: defult[0], beforeDot: false});
    } else if (this.activity.ActivityID === 130) {
      this.value01 = 0;
      this.value02 = 0;
      const defult = this.generateDefaultValues(parseFloat(this.activity.DefaultValue01.toString()), [1, 1, 1], false);
      this.taskColumns01.wheels.push({min: 0, max: 9, value: defult[0], beforeDot: false});
      this.taskColumns01.wheels.push({min: 0, max: 9, value: defult[1], beforeDot: false});
      this.taskColumns01.wheels.push({min: 0, max: 9, value: defult[2], beforeDot: false});
      this.taskColumns01.unit = false;
    } else if(this.activity.ActivityID === 101) {
      this.value01 = 10;      
      const defult = this.generateDefaultValues(parseFloat(this.activity.DefaultValue01.toString()), [3], false);
      this.taskColumns01.wheels.push({min: 10, max: 300, value: defult[0], beforeDot: false});
    } 
  }

  generateDefaultValues(value: number, columnDecCount: any, frction: boolean = false) {
    const v = value;
    const colsCount = columnDecCount.length;
    let defalt = columnDecCount;

    const fraction = Math.trunc(Math.round(((value%1) * 10)))
    
    value = Math.trunc(value);

    for(let i = colsCount - (frction ? 2 : 1) ; i >= 0 ; --i) {
      const modValue = Math.pow(10, columnDecCount[i]);
      defalt[i] = value % modValue;
      value = Math.trunc(value / modValue);
    }

    if(frction)
      defalt[colsCount - 1] = fraction;
    return defalt;
  }

  isTempActivity() {
    return this.activity.RefVitalTableColumn01 == VitalColumns.Temperatur
  }

  miniFormChanged(value) {
    this.value01 = value;
  }

  openCalender() {
    this.calenderPicker.open();
  }

  async documentTask() {
    /*console.log({
      ID: this.activity.ID,
      ActivityID: this.activity.ActivityID,
      PatientID: this.activity.PatientID,
      MethodID: this.activity.MethodID,
      NumberOfDays: this.numberOfDays,
      NumberPerDay: this.numberPerDays,
      Occurance: this.activity.Occurance,
      EveryXhrs: this.everyXhrs,
      EveryMorning: this.morning,
      EveryNoon:  this.afternone,
      EveryEvening: this.evening,
      InputValues: this.activity.InputValues,
      DefaultValue01: this.nonExecutable ? this.nonExecutableReason: this.value01,
      DefaultValue02: this.nonExecutable ? this.nonExecutableReason: this.value02,
      Remarks: this.remarks + ' ' + this.freeText,
      UserID: AppModule.currentUser.ID,
      DocIcon: '',
      EventID: 0,
      NonExecutable: this.nonExecutable,
      TimeOverride: this.timeOverride,
      TimeOverrideDateDifferenceInMinutes: this.TimeOverrideDateDifferenceInMinutes
    })*/
    await this.http.post(AppModule.baseURL + '/DocumentActivities/DocumentTask',
    {
      ID: this.activity.ID,
      ActivityID: this.activity.ActivityID,
      PatientID: this.activity.PatientID,
      MethodID: this.activity.MethodID,
      NumberOfDays: this.numberOfDays,
      NumberPerDay: this.numberPerDays,
      Occurance: this.activity.Occurance,
      EveryXhrs: this.everyXhrs,
      EveryMorning: this.morning,
      EveryNoon:  this.afternone,
      EveryEvening: this.evening,
      InputValues: this.activity.InputValues,
      DefaultValue01: this.nonExecutable ? this.nonExecutableReason: this.value01,
      DefaultValue02: this.nonExecutable ? this.nonExecutableReason: this.value02,
      Remarks: this.remarks + ' ' + this.freeText,
      UserID: AppModule.currentUser.ID,
      DocIcon: '',
      EventID: 0,
      NonExecutable: this.nonExecutable,
      TimeOverride: this.timeOverride,
      TimeOverrideDateDifferenceInMinutes: this.TimeOverrideDateDifferenceInMinutes

    }).subscribe(async suss => {
      this.aufgabenTabService.publishSomeData({
        refresh: true
      })

      await this.navCtrl.pop().then(async () =>
        {
          await this.toastHandler.presentToast(ToastStatus.success);
        }
      );

    }, error => {
      console.log('alert should be shown here.')
    })

    //await this.showToast();
    
  }

  

  openWheelSelector(inputNumber) {
    const columns = this.generateColumns(this.taskColumns)
    this.pushWheel(columns, inputNumber);
  }

  async pushWheel(cols, inputNumber) {
    const picker = await pickerController.create({
      columns: cols,
      buttons: [
        {
          text: 'Abbrechen',
          role: 'cancel'
        },
        {
          text: 'Ändern',
          handler: (cols) => {
            let selectedValue = cols['col4'].value
                                + cols['col.1'].value / 10;

            if(!this.isTempActivity()) {
              selectedValue += cols['col1'].value * 1000
                              + cols['col2'].value * 100
                              + cols['col3'].value * 10
            }

            if(inputNumber == 1)
              this.value01 = selectedValue;
            else
              this.value02 = selectedValue;

          }
        }
      ],
      mode: 'ios'
    })

    await picker.present().then(() => {
      /*var columns = document.getElementsByTagName('ion-picker-column');
      columns[0].style.minWidth = '200px';*/
      }
    )
  }

  generateColumns(taskColumns){
    let columns = [];
    for(let i = 0 ; i < taskColumns.colCount ; ++i) {
      columns.push({
        name: 'col' + taskColumns.colName[i],
        options: this.generateColumn(taskColumns.colMin[i], taskColumns.colMax[i], 
                                    taskColumns.colIncreament[i], (taskColumns.colDotPostion == i + 1 ? true : false)),
        selectedIndex: 0
      });
    }

    return columns;
  }

  generateColumn(columnMin, columnMax, columnIcrement, dotPostion) {
    if(dotPostion)
      return [this.generateColumnText('.', '.')];

    let colItems = [];
    for(let i = columnMin ; i <= columnMax ;  i = i + columnIcrement) {
      colItems.push(this.generateColumnText(i.toString(), i));
    }

    return colItems;
  }

  generateColumnText(text, value) {
    return { 
            text: text,
            value: value
          }
  }

  clickEvening() { 
    this.evening = !this.evening;
  }

  clickAfternoon() { 
    this.afternone = !this.afternone;
  }

  clickMorning() { 
    this.morning = !this.morning;
  }

  isOk() {
    return (this.getDocType() === 'OK');
  }

  isFormular() {
    return (this.getDocType() === 'Formular');
  }

  isWert() {
    return (this.getDocType() === 'Wert')
  }

  isFreiText() {
    return (this.getDocType() === 'Freitext');
  }

  isMiniForm() {
    return (this.getDocType() === 'Mini Formular');
  }

  getDocType() {
    if(this.activity.DocType !== undefined && this.activity.DocType !== null) {
      const docType = this.activity.DocType.replace(/^\s+|\s+$/g, '');
      return docType;
    } return '';
  }

  openWebForm(link) {
    link += + '&PatientID=' + this.activity.PatientID
            + '&IncidentNumber=' + this.activity.IncidentID;

    if (this.platform.is('ios')) {
      this.iab.create(link, '_blank', 'location=no');
    } else if (this.platform.is('android')) {
      this.iab.create(link, '_blank', 'hideurlbar=yes');
    }
  }

  Record() {
    this.speechRecognition.hasPermission()
    .then((hasPermission: boolean) => {
      if(hasPermission){
        this.startRecording();
      } else {
        this.requestRecordPermission();
      }
    })
  }

  requestRecordPermission() {
    this.speechRecognition.requestPermission()
    .then(
      () => this.startRecording(),
      () => {}
    )
  }

  startRecording() {
    const option = {
      language: 'de-DE',
    };
    this.speechRecognition.startListening(option)
    .subscribe(
      (matches: string[]) => 
      {
        //console.log(matches);
        this.remarks = matches[0];
        this.cd.detectChanges();
      },
      (onerror) => console.log('error:', onerror)
    )
  }

  nonExecutableCheck() {
    this.nonExecutableReason = '';
    this.reasonSelected = false;
  }

  onReasonChange(reason) {
    this.nonExecutableReason = reason;
    this.reasonSelected = true;
  }

}
