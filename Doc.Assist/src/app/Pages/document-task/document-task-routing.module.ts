import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DocumentTaskPage } from './document-task.page';

const routes: Routes = [
  {
    path: '',
    component: DocumentTaskPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DocumentTaskPageRoutingModule {}
