import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddNewTaskPageRoutingModule } from './add-new-task-routing.module';

import { AddNewTaskPage } from './add-new-task.page';
import { DropDownComponent } from 'src/app/Components/drop-down/drop-down.component';
import { TitleBarComponent } from 'src/app/Components/title-bar/title-bar.component';
import { CheckBoxComponent } from 'src/app/Components/check-box/check-box.component';
import { CustomComponents } from 'src/app/Components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddNewTaskPageRoutingModule,
    CustomComponents
  ],
  declarations: [AddNewTaskPage]
})
export class AddNewTaskPageModule {}
