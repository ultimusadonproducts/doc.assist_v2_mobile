import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AppModule } from 'src/app/app.module';
import { AufgabenTabService } from 'src/app/Events/AufgabenTab/aufgaben-tab.service';
import { PatientProfileService } from 'src/app/Events/PatientProfile/patient-profile.service';
import { SpeechRecognition } from '@ionic-native/speech-recognition/ngx';

@Component({
  selector: 'app-add-new-task',
  templateUrl: './add-new-task.page.html',
  styleUrls: ['./add-new-task.page.scss'],
})
export class AddNewTaskPage implements OnInit {
  activity: any = {};
  
  editMode: string = 'false';
  medical: string = 'false';
  document: string = 'document';
  select: string = 'select';

  state: string = this.select;

  medicalTasks:any = [];
  medicalTasksDDList:any = [];
  selectedMedicalTask:any = -1;

  nonMedicalTasks:any = [];
  nonMedicalTasksDDList:any = [];
  selectedNonMedicalTask:any = -1;

  morning = false;
  afternone = false;
  evening = false;
  bedarf = false;

  everyXhrs: any = 0;
  numberOfDays: any = -2;
  numberPerDays: any = 0;
  dosage: number = 0;

  methodID: number = 0;
  patientID: string = '0';

  remarks: string = '';
  activityName: string = '';

  dauerList = [{text: 'unendlich', value: '-2', index: 0},
                {text: 'Ermessen Pflegekraft', value: '-1', index: 1},
                {text: '1 Tage', value: '1', index: 2},
                {text: '2 Tage', value: '2', index: 3},
                {text: '3 Tage', value: '3', index: 4},
                {text: '4 Tage', value: '4', index: 5},
                {text: '5 Tage', value: '5', index: 6},
                {text: '6 Tage', value: '6', index: 7},
                {text: '7 Tage', value: '7', index: 8},
                {text: '8 Tage', value: '8', index: 9},
                {text: '9 Tage', value: '9', index: 10},
                {text: '10 Tage', value: '10', index: 11},
              ]

  numberPerDayDDList = [{text: 'Optional', value: '-1', index: 0},
                        {text: '1', value: '1', index: 1},
                        {text: '2', value: '2', index: 2},
                        {text: '3', value: '3', index: 3},]
  
  everyXHoursDDList = [ {text: 'Optional', value: '-1', index: 0},
                        {text: '1 Stunde', value: '1', index: 1},
                        {text: '2 Stunden', value: '2', index: 2},
                        {text: '3 Stunden', value: '3', index: 3},
                        {text: '4 Stunden', value: '4', index: 4},
                        {text: '5 Stunden', value: '5', index: 5},
                        {text: '6 Stunden', value: '6', index: 6},
                        {text: '7 Stunden', value: '7', index: 7},
                        {text: '8 Stunden', value: '8', index: 8},
                        {text: '9 Stunden', value: '9', index: 9},
                        {text: '10 Stunden', value: '10', index: 10},
                        {text: '11 Stunden', value: '11', index: 11},
                        {text: '12 Stunden', value: '12', index: 12},
                      ]

  morningCheckProp: any = {label: 'Morgen', status: false}
  afternoneCheckProp: any = {label: 'Mittag', status: false}
  eveningCheckProp: any = {label: 'Abend', status: false}

  Methods = [{
    text: 'Täglich',
    value: 1,
    index: 0
  },
  {
    text: 'Regelmäßig',
    value: 2,
    index: 1
  },
  {
    text: 'Intervall',
    value: 3,
    index: 2
  }]

  checkBoxOn = './assets/icon/checkbox/on.svg';
  checkBoxOnDisabled = './assets/icon/checkbox/on-disabled.svg';
  checkBoxOff = './assets/icon/checkbox/off.svg';
  checkBoxOffDisabled = './assets/icon/checkbox/off-disabled.svg';

  voiceRecorderInactive = './assets/icon/VoiceRecorder/Voice_inactive.png';
  voiceRecorderActive = './assets/icon/VoiceRecorder/Voice_active.png';

  constructor(private storage: Storage,
              private router: ActivatedRoute,
              private http: HttpClient,
              private patientProfile: PatientProfileService,
              private nav: NavController,
              private toastCtrl: ToastController,
              private aufgabenTabService: AufgabenTabService,
              private speechRecognition: SpeechRecognition,
              private cd: ChangeDetectorRef) { }

  async ngOnInit() {
    await this.router.params.subscribe(async params => 
      {
        this.medical = await params['medical'];
        this.editMode = await params['edit'];
        this.patientID = await params['patientID'];

        if(this.editMode == 'true'){
          this.state = this.document;
          await this.storage.get('selectedTask').then((act) =>
          { 
            this.activity = act;
            //console.log(this.activity);
            this.formActivity(this.activity, false);
          })
        } else {
        }

        if(this.medical == 'true' && this.editMode == 'false') {
          this.getMedicalActiveTasks();

        } else if (this.medical == 'false' && this.editMode == 'false') {
          this.getNonMedicalActiveTasks();
        }
      })
  }

  changeMorning() {
    if(this.bedarf) return;
    this.morning = !this.morning;
  }

  changeAfternoon() {
    if(this.bedarf) return;
    this.afternone = !this.afternone;
  }

  changeEvening() {
    if(this.bedarf) return;
    this.evening = !this.evening;
  }

  changeBedarf() {
    this.bedarf = !this.bedarf;
  }

  changeDauer(num) {
    this.numberOfDays = num;
  }

  /*selectNonMedicalTask(index) {
    this.selectedNonMedicalTask = this.nonMedicalTasks[index];
    this.state = this.document;
    this.formActivity(this.selectedNonMedicalTask);
  }

  selectMedicalTask(index) {
    this.selectedMedicalTask = this.medicalTasks[index];
  }*/

  goNext() {
    this.state = this.document;
    this.formActivity(this.medical == 'true' ? this.medicalTasks[this.selectedMedicalTask] : 
                      this.nonMedicalTasks[this.selectedNonMedicalTask]);
  }

  selectMethod(id) {
    this.methodID = id; 
  }

  formActivity(activity, nw:boolean = true){
    this.activityName = activity.NameOption01 == null || activity.NameOption01.length < 2 ? activity.ActivityName : activity.NameOption01;
    this.remarks = activity.Remarks;
    this.bedarf = (activity.PerDemand === null) ? false : activity.PerDemand;
    
    if(nw) {
      this.methodID = 1;
    } else {
      this.methodID = activity.MethodID;
      this.numberPerDays = activity.NumberPerDay;
      this.numberOfDays = activity.NumberOfDays;
      this.everyXhrs = activity.EveryXhrs;
      this.dosage = activity.Dosage == null ? this.dosage : activity.Dosage;
      this.evening = activity.EveryEvening;
      this.afternone = activity.EveryNoon;
      this.morning = activity.EveryMorning;
      this.methodID = activity.MethodID;
    }
  }

  getNonMedicalActiveTasks() {
    this.http.post(AppModule.baseURL + '/NewPatient/GetPatientNonMedicalTasksDDLst', parseInt(this.patientID)).subscribe(data => {
      const activity:any = data;
      for(let i = 0 ; i < activity.length ; ++i){
        this.nonMedicalTasksDDList.push({text: activity[i].Name, value: i, index: i})
        this.nonMedicalTasks.push(activity[i]);
      }
    })
  }

  getMedicalActiveTasks() {
    this.http.post(AppModule.baseURL + '/NewPatient/GetPatientMedicalTasksDDLst', parseInt(this.patientID)).subscribe(data => {
      const activity:any = data;
      for(let i = 0 ; i < activity.length ; ++i){
        this.medicalTasksDDList.push({text: activity[i].Name, value: i, index: i})
        this.medicalTasks.push(activity[i]);
      }
    })
  }

  updateTask() {
    /*console.log({
      ActivityID: this.activity.ActivityID,
      PatientID: this.activity.PatientID,
      MethodID: this.activity.MethodID,
      NumberOfDays: this.numberOfDays,
      NumberPerDay: this.numberPerDays,
      Occurance: this.activity.Occurance,
      EveryXhrs: this.everyXhrs,
      EveryMorning: this.morning,
      EveryNoon: this.afternone,
      EveryEvening: this.evening,
      Dosage: this.dosage,
      Remarks: this.remarks,
      PerDemand: this.bedarf
    })*/

    this.http.post(AppModule.baseURL + '/NewPatient/UpdatePatientTask', {
      ActivityID: this.activity.ID,
      PatientID: this.activity.PatientID,
      MethodID: this.activity.MethodID,
      NumberOfDays: this.numberOfDays,
      NumberPerDay: this.numberPerDays,
      Occurance: this.activity.Occurance,
      EveryXhrs: this.everyXhrs,
      EveryMorning: this.morning,
      EveryNoon: this.afternone,
      EveryEvening: this.evening,
      Dosage: this.dosage,
      Remarks: this.remarks,
      PerDemand: this.bedarf
    }).subscribe(data => {
      this.refreshTasks();
      this.nav.pop().then(() =>
      {
        this.showToast(data == true ? true : false);
      });
    })
  }

  documentTask() {
    const selectedActivity = this.medical == 'true' ? this.medicalTasks[this.selectedMedicalTask] : 
                                                      this.nonMedicalTasks[this.selectedNonMedicalTask];
    /*console.log({
      ActivityID: selectedActivity.ID,
      PatientID: this.patientID,
      MethodID: this.methodID,
      NumberOfDays: this.numberOfDays,
      NumberPerDay: this.numberPerDays,
      Occurance: selectedActivity.OccuranceDefault,
      EveryXhrs: this.everyXhrs,
      EveryMorning: this.morning,
      EveryNoon: this.afternone,
      EveryEvening: this.evening,
      Remarks: this.remarks,
      PerDemand: this.bedarf,
      Dosage: this.dosage
    })*/
    this.http.post(AppModule.baseURL + '/NewPatient/AddPatientTask', {
      ActivityID: selectedActivity.ID,
      PatientID: this.patientID,
      MethodID: this.methodID,
      NumberOfDays: this.numberOfDays,
      NumberPerDay: this.numberPerDays,
      Occurance: selectedActivity.OccuranceDefault,
      EveryXhrs: this.everyXhrs,
      EveryMorning: this.morning,
      EveryNoon: this.afternone,
      EveryEvening: this.evening,
      Remarks: this.remarks,
      PerDemand: this.bedarf,
      Dosage: this.dosage
    }).subscribe(data => {      
      this.refreshTasks();
      this.nav.pop().then(() =>
      {
        this.showToast(data == true ? true : false);
      });
    })
  }

  async showToast(success: boolean) {
    const toast = await this.toastCtrl.create({
      message: '<ion-icon style="width: 20px; height: 20px;" src="./assets/icon/' 
                + ((success == true) ? 'success' :  'fail')+ '.svg"></ion-icon>'
                + ((success == true) ? 'Gespeichert' :  'Fehler'),
      duration: 2000,
      mode: 'ios',
      cssClass: 'toast-css-class-' + ((success == true) ? 'success' :  'fail')
    });
    toast.present();
  }

  refreshTasks() {
    this.patientProfile.publishSomeData({
      refresh: true,
    })
    this.aufgabenTabService.publishSomeData({
      refresh: true
    });
  }

  Record() {
    this.speechRecognition.hasPermission()
    .then((hasPermission: boolean) => {
      if(hasPermission){
        this.startRecording();
      } else {
        this.requestRecordPermission();
      }
    })
  }

  requestRecordPermission() {
    this.speechRecognition.requestPermission()
    .then(
      () => this.startRecording(),
      () => {}
    )
  }

  startRecording() {
    const option = {
      language: 'de-DE',
    };
    this.speechRecognition.startListening(option)
    .subscribe(
      (matches: string[]) => 
      {
        //console.log(matches);
        this.remarks = matches[0];
        this.cd.detectChanges();
      },
      (onerror) => console.log('error:', onerror)
    )
  }

}
