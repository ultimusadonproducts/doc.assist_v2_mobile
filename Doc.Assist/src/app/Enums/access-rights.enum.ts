export enum AccessRights {
    AM_Ereignis = 0,
    AM_Bedarfsmedikation = 1,
    AM_AdhocAufgabe = 2,
    AM_Korrektur = 3,
    AM_ärztlicheAnordnung = 4
}