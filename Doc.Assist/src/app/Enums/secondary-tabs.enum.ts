export enum SecondaryTabs {
    my = 1,
    all = 2,
    free = 3,
    ungrouped = 4,
    room = 5,
    patient = 6
}
