export enum VitalWerteValues {
    keine = 0,
    Blutdruck_SYS = 1,
    Blutdruck_DIA = 2,
    Heartrate = 3,
    Temperatur = 4,
    Pain_NoMotion = 5,
    Pain_Motion = 6,
    Date = 7
}
