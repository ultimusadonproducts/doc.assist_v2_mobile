import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { AlertController, IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HTTP } from '@ionic-native/http/ngx';
import { DomainConfig } from './Models/DomainConfig/domain-config';
import { CommonModule } from "@angular/common";
import { IonicStorageModule } from '@ionic/storage';
import { WheelSelector } from '@ionic-native/wheel-selector/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { SpeechRecognition } from '@ionic-native/speech-recognition/ngx';
import { Storage } from '@ionic/storage';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { PickerOptions } from './Components/picker-view/picker-options.provider';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
/*import {
  NgxMatDatetimePickerModule, 
  NgxMatNativeDateModule, 
  NgxMatTimepickerModule 
} from '@angular-material-components/datetime-picker';
*/

import { NgxMatDatetimePickerModule, 
  NgxMatNativeDateModule, 
  NgxMatTimepickerModule  } from '@angular-material-components/datetime-picker';
import { DropDownListFilterPipe } from './pipes/drop-down-list-filter.pipe';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/lang/', '.json');
}

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, 
    IonicModule.forRoot(), 
    IonicStorageModule.forRoot(), 
    AppRoutingModule,
    HttpClientModule,
    CommonModule,
    /*NgxMatDatetimePickerModule,
    NgxMatTimepickerModule,
    NgxMatNativeDateModule,*/
    NgxMatDatetimePickerModule,
    NgxMatTimepickerModule,
    NgxMatNativeDateModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    BrowserAnimationsModule],
  providers: [
    StatusBar,
    SplashScreen,
    HTTP,
    WheelSelector,
    InAppBrowser,
    SpeechRecognition,
    PickerOptions,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  public static domainConfig: DomainConfig = new DomainConfig(false, 'ultimuscps.com');
  public static ActionOnFeature = [];
  public static currentServerLink = 'https://demo.ultimuscps.com/'
  public static apiURL = 'Doc-Assist/api';
  public static baseURL = AppModule.currentServerLink + AppModule.apiURL;
  public static currentUser;
  public static isDoctor: boolean;
  public static moduleNumber = 0;
  public static serversLinks = [];

  constructor(private storage: Storage, private http: HttpClient) {
    this.storage.get('currentServerLink').then((link) => {
      if(link === null) {
        this.storage.set('currentServerLink', AppModule.currentServerLink);
      } else {
        AppModule.currentServerLink = link;
        AppModule.baseURL = AppModule.currentServerLink + AppModule.apiURL;
      }
    })
    //this.getServersLinks();
  }

  public static checkFeatureAction(feature, action) {    
    if(AppModule.ActionOnFeature[feature] === undefined || AppModule.ActionOnFeature[feature] === null)
      return false;
      
    return AppModule.ActionOnFeature[feature].includes(action);
  }

  public static async handleError(error) {
    if(error.status == 0){ //ERR_CONNECTION_TIMED_OUT
      await AppModule.presentAlert("Server Problem", "", "Server nicht erreichbar. Bitte kontaktieren Sie Ihren Administrator.", ["OK"])
    }
  }

  private static async presentAlert(header, subHeader, message, buttons) {
    const alertController = new AlertController();
    const alert = await alertController.create({
      header: header,
      subHeader: subHeader,
      message: message,
      buttons: buttons
    });

    await alert.present();
  }

}
